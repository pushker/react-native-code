import { Header } from '../../../../common/Header'
import React, {Component} from "react";
import {StyleSheet, Text, View, TouchableOpacity, Button} from "react-native";
import { RNCamera } from 'react-native-camera';

export default class Onboarding extends Component {

    static navigationOptions = {
        header: null
    };

    componentDidMount() {

    }

    takePicture = async function() {
        if (this.camera) {
            const options = { quality: 0.5, base64: true };
            const data = await this.camera.takePictureAsync(options);
            console.log(data.uri);
        }
    };

    goCapture = () => {
        console.log('go to camera page');
        this.props.navigation.navigate('OnboardingSelfieCapture')
    };

    render() {
        const {navigate} = this.props.navigation;
        return (
            <React.Fragment>
                <Header headerText={'Javvy Verification - KYC'} />
                <View>
                    <View >
                        <Text>
                            We’ve all taken at least one selfie in our lives,
                            so this should be pretty easy. We need to see your
                            full face within a well lit room. No hats, hoodies,
                            masks, etc. With a steady hand, hold the front facing
                            camera of your phone a few inches from your face, and
                            press the capture button. Say, “Cheese!”
                        </Text>
                    </View>

                    <View>
                        <Button title="Capture" onPress={this.goCapture}></Button>
                    </View>
                </View>



            </React.Fragment>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: 'black'
    },
    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    capture: {
        flex: 0,
        backgroundColor: '#fff',
        borderRadius: 5,
        padding: 15,
        paddingHorizontal: 20,
        alignSelf: 'center',
        margin: 20
    }
});