import { connect } from 'react-redux';
import UtilityKyc from '../components/Screens/UtilityKyc';
import { utilityKyc } from '../actions/userAction';


const mapStateToProps = (state) => {
    return {
        user: state.user,
        wallet: state.wallet
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        utilityKyc: (data) => {
            dispatch(utilityKyc(data))
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(UtilityKyc);