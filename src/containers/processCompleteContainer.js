import { connect } from 'react-redux';
import ProcessComplete from '../components/Screens/Register/Views/ProcessComplete.js';


import { activateAccount } from '../actions/userAction';

const mapStateToProps = ( state ) => {
    return {
        user: state.user,
        wallet: state.wallet
    }
}

const mapDispatchToProps = ( dispatch ) => {
    return {
        activateAccount: (data) => {
            dispatch(activateAccount(data))
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ProcessComplete);