import { connect } from 'react-redux';
import FrontKyc from '../components/Screens/FrontKyc';
import { saveFrontId } from '../actions/userAction';


const mapStateToProps = (state) => {
    return {
        user: state.user,
        wallet: state.wallet
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        saveFrontId: (data) => {
            dispatch(saveFrontId(data))
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(FrontKyc);