import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    TouchableOpacity,
    Dimensions,
    StatusBar,
    ActivityIndicator,
    Alert
} from 'react-native';

import { validateEmail } from '../../../../validations.js';



export default class ActivateAccount extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            code: '',
        }
        this.handleSubmit = this.handleSubmit.bind(this);
        this.validateActiveAccount = this.validateActiveAccount.bind(this);
    }


    static navigationOptions = {
        gesturesEnabled: false,
        header: null
    }


    validateActiveAccount() {
        const clearActivityIndicator = () => this.setState({ isLoading: false })
        const isActivated = "Account activated successfully."
        const message = "Required  Fields: email, code";
        const validCode = "Please enter valid account activation code";
        console.log(this.props.user.activateAccountData);
        if (this.props.user.activateAccountData.message === isActivated) {
            this.props.navigation.navigate('Login')
            clearActivityIndicator()
        } else if (this.props.user.activateAccountData.message === '' || this.props.user.activateAccountData.message === undefined) {
            Alert.alert(``,'Please check your details');
            clearActivityIndicator();
        } else if (this.props.user.activateAccountData.message === message) {
            Alert.alert(``,'Please enter your email and code');
            clearActivityIndicator();

        } else if (this.props.user.activateAccountData.message === validCode) {
            Alert.alert(``,validCode);
            clearActivityIndicator();
        } else {
            Alert.alert(``,'Something went wrong, please try again.');
            clearActivityIndicator();
        }
    }

    handleSubmit(event){
        event.preventDefault();

        const user = {
            email: this.state.email,
            code: this.state.code
        }
        var emailRegex = new RegExp("^[a-z0-9_.-]+\.?[a-z0-9_-]+@[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?[.][a-z]{2,61}(?:\.[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?)*$");
        if (user.email !== '' && user.code !== '') {
            if (emailRegex.test(user.email) == true) {
                this.props.activateAccount(user)
                this.setState({ isLoading: true })
                setTimeout(() => {
                    this.validateActiveAccount(user)
                }, 3000)
            } else {
                Alert.alert(``,
                    `Please enter valid email address.`,
                    [
                        { text: 'OK' }
                    ])
            }
        } else {
            Alert.alert(``,'Please enter your email and code');
        }


    }
    focusCode = () => {
        if (validateEmail(this.state.email) === 'invalid') {
            Alert.alert(``, 'Please enter valid email');
        } else if (validateEmail(this.state.email) === 'empty') {
            Alert.alert(``, 'Please enter an email address');
        }
        this.codeInputBox.focus();

    }


    render() {

        const { navigate } = this.props.navigation;

        const width = Dimensions.get('window').width
        const height = Dimensions.get('window').height

        const success = 'Account activated successfully.'
        const err = this.props.user.activateAccountData.message !== success ? this.props.user.activateAccountData.message : null

        const newActivationCode = "Account Activation Code sent to your email."
        const newCodeAlert = this.props.user.activationCode.message === newActivationCode ? this.props.user.activationCode.message : null

        return (
            <React.Fragment>
            <StatusBar barStyle={'dark-content'}/>
                <View style={styles.container}>
                    {this.state.isLoading === true ?
                        <View style={{ position: 'absolute', right: 0, left: 0, top: 0, bottom: 0, backgroundColor: '#fff', opacity: .8, height: height, zIndex: 999, justifyContent: 'center', alignItems: 'center' }}><ActivityIndicator size="large" color="#0F44A0" /></View> : null}
                <View style={styles.lgn_container}>

                    <View style={width === 320 ? styles320.lgn_txtContainer : styles.lgn_txtContainer}>
                        <Text style={{fontSize: 16, fontWeight: '200', padding: 10}}>
                            Thank you for signing up with Javvy,
                            your new cryptocurrency wallet.
                        </Text>
                        <Text style={{fontSize: 16, fontWeight: '200', padding: 10}}>
                            Please check your email to confirm your account information
                            and complete the sign-up process.
                        </Text>
                    </View>

                    <View style={{marginTop: 33}}>

                        <TextInput onChangeText={(email) => this.setState({email: email})}
                                keyboardType={'email-address'}
                                autoCapitalize={'none'}
                                style={width === 320 ? styles320.lgn_txtInput : styles.lgn_txtInput}
                                autoFocus
                                placeholder={'Email Address'}
                                underlineColorAndroid={'rgba(0,0,0,0)'}
                                onSubmitEditing={this.focusCode}>
                        </TextInput>

                            <TextInput onChangeText={(code) => this.setState({ code: code })}
                                autoCapitalize={'none'}
                                style={[width === 320 ? styles320.lgn_txtInput : styles.lgn_txtInput, styles.fixup]}
                                keyboardType="phone-pad"
                                placeholder={'Activation Code'}
                                underlineColorAndroid={'rgba(0,0,0,0)'}
                                ref={(ref) => { this.codeInputBox = ref; }}>
                        </TextInput>
                    </View>

                    <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around', width: '100%'}}>
                        <View style={width === 320 ? styles320.btnSignInContainer : styles.btnSignInContainer}>
                            <TouchableOpacity style={width === 320 ? styles320.btnSignIn : styles.btnSignIn} onPress={this.handleSubmit}>
                                <Text style={styles.btnSignInText}>Activate</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={width === 320 ? styles320.btnSignInContainer : styles.btnSignInContainer}>
                            <TouchableOpacity style={width === 320 ? styles320.btnSignIn : styles.btnSignIn} onPress={() => navigate('ResendCode')}>
                                <Text style={styles.btnSignInText}>Resend Code</Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                </View>
            </View>
            </React.Fragment>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: '#fff',
        paddingTop: 15
    },
    lgn_icon: {
        height: 36.62,
        width: 27.62,
    },
    lgn_container: {
        width: 296,
        height: 471,
        alignItems: 'center',
    },
    lgn_txtInput: {
        width: 325.3,
        height: 45.59,
        borderRadius: 50,
        backgroundColor: '#EDEDED',
        opacity: 0.8,
        padding: 10,
        fontSize: 16,
    },
    lgn_txtContainer:{
        width: 325.3,
        height: 102.3,
        marginTop: 47.1,
        justifyContent: 'space-between',
        paddingLeft: 10,
        paddingRight: 10
    },
    fixup:{
        marginTop: 13.1,
        marginBottom: 13.1
    },
    btnSignInContainer: {
        alignItems: 'center',
        marginTop: 20
    },
    btnSignIn: {
        width: 130,
        marginBottom: 10.1,
        shadowColor: '#0F44A0',
        shadowOffset:{width: 2, height: 2},
        shadowOpacity: .8,
        backgroundColor: '#0F44A0',
        height:52.94,
        marginTop: 3,
        borderRadius: 50,
        justifyContent: 'center',
        alignItems:'center'
    },
    btnSignInText: {
        color: 'white',
        fontSize: 18,
        textAlign: 'center'
    },
    signUpContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    signUpBtn: {
        opacity: 0.8,
        padding: 10,
        color: 'black',
        fontWeight: '600'
    }
})


const styles320 = StyleSheet.create({
    lgn_icon: {
        height: 106.62,
        width: 97.62,
    },
    lgn_container: {
        width: 296,
        height: 471,
        alignItems: 'center',
    },
    lgn_txtInput: {
        width: 305.3,
        height: 45.59,
        borderRadius: 50,
        backgroundColor: '#EDEDED',
        opacity: 0.8,
        padding: 10,
        fontSize: 16,
    },
    lgn_txtContainer:{
        width: 305.3,
        height: 102.3,
        marginTop: 47.1,
        justifyContent: 'space-between',
        paddingLeft: 10,
        paddingRight: 10
    },
    fixup:{
        marginTop: 13.1,
        marginBottom: 13.1
    },
    btnSignInContainer: {
        alignItems: 'center',
        marginTop: 20
    },
    btnSignIn: {
        width: 130,
        marginBottom: 10.1,
        shadowColor: '#0F44A0',
        shadowOffset:{width: 2, height: 2},
        shadowOpacity: .8,
        backgroundColor: '#0F44A0',
        height:52.94,
        marginTop: 3,
        borderRadius: 50,
        justifyContent: 'center',
        alignItems:'center'
    },
    btnSignInText: {
        color: 'white',
        fontSize: 18,
        textAlign: 'center'
    },
})

