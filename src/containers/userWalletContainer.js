import { connect } from 'react-redux';
import UserWallet from '../components/Screens/Wallet/UserWallet';
import {
    userWalletBalance, createUserCoinTransaction,
    customLoaderAction, customFlag, convertUsercoin,estimateRateCoin
} from '../actions/walletAction';


const mapStateToProps = (state) => {
    return {
        user: state.user,
        wallet: state.wallet
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        userWalletBalance: (data) => {
            dispatch(userWalletBalance(data))
        },
        createUserCoinTransaction: (data) => {
            dispatch(createUserCoinTransaction(data))
        },
        customLoaderAction: (data) => {
            dispatch(customLoaderAction(data))
        },
        customFlag: (data) => {
            dispatch(customFlag(data))
        },
        convertUsercoin: (data) => {
            dispatch(convertUsercoin(data))
        },
        estimateRateCoin: (data) => {
            dispatch(estimateRateCoin(data))
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(UserWallet);