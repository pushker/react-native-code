import {ACTIVATE_ACCOUNT,
        CHANGE_PASSWORD,
        COUNTRY_LIST,
        CREATE_ACCOUNT,
        CURRENCY_LIST,
        FORGOT_PASSWORD,
        GET_USER_DETAIL,
        LOGIN,
        MERGE_ACCOUNT,
        RESEND_ACTIVATION_CODE,
        UPDATE_ACCOUNT,
        UPDATE_PASSWORD,
        TRANSACTIONS_DETAIL,
        COMPARE_BITCOIN,
        COMPARE_ETHEREUM,
        SELFIE_IMAGE,
        STATE_LIST,
        SAVE_COUNTRY_STATE,
        SAVE_FRONT_ID,
        SAVE_BACK_ID,
        SAVE_FULL_KYC_WITH_UTILITY,
        GET_KYC_DOCUMENTS,
        CHECK_KYC_STATUS
} from './types';


import { token, baseUrlLive, baseUrlTest, compareBTCUrl, compareETHUrl } from '../api';




export const activateAccount = (user) => dispatch => {
    fetch(`${baseUrlLive}user/activate/account?token=${token}`, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(user)
    })
    .then(res => res.json())
    .then(data => dispatch({
        type: ACTIVATE_ACCOUNT,
        payload: data
    }))
    .catch(err => {
        console.log(err)
    })
}


export const createAccount = (user) => dispatch => {
    fetch(`${baseUrlLive}user/create?token=${token}`, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(user)
    })
    .then(res => res.json())
    .then(data => dispatch({
        type: CREATE_ACCOUNT,
        payload: data
    }))
    .catch(err => {
        console.log(err)
    })
};


export const getUserDetail = (user) => dispatch => {
    fetch(`${baseUrlLive}user/detail?token=${token}`, {
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: user.jwt
        },
    })
    .then(res => res.json())
    .then(data => dispatch({
        type: GET_USER_DETAIL,
        payload: data
    }))
    .catch(err => {
        console.log(err)
    })
};


export const login = (user) => dispatch => {
        fetch(`${baseUrlLive}user/auth?token=${token}`, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(user)
        })
    .then(res => res.json())
    .then(data => {
        console.log(data)
        dispatch({
            type: LOGIN,
            payload: data
        })
    })
    .catch(err => {
        console.log(err)
    })
};



export const forgotPassword = (user) => dispatch => {
    fetch(`${baseUrlLive}user/forgot-password?token=${token}`,{
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(user)
    })
    .then(res => res.json())
    .then(data => dispatch ({
        type: FORGOT_PASSWORD,
        payload: data
    }))
    .catch(err => {
        console.log(err)
    })
};


export const updatePassword = (user) => dispatch => {
    fetch(`${baseUrlLive}user/update-password?token=${token}`,{
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(user)
    })
    .then(res => res.json())
    .then(data => dispatch ({
        type: UPDATE_PASSWORD,
        payload: data
    }))
    .catch(err => {
        console.log(err)
    })
};


export const resendActivationCode = (user) => dispatch => {
    fetch(`${baseUrlLive}user/resend/activation-code?token=${token}`, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(user)
    })
    .then(res => res.json())
    .then(data => dispatch({
        type: RESEND_ACTIVATION_CODE,
        payload: data
    }))
    .catch(err => {
        console.log(err)
    })
};


export const transactionsDetail = (user) => dispatch => {
    fetch(`${baseUrlLive}user/transaction/detail?token=${token}`, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: user.jwt
        },
        body: JSON.stringify(user)
    })
        .then(res => res.json())
        .then(data => dispatch({
            type: TRANSACTIONS_DETAIL,
            payload: data
        }))
        .catch(err => {
            console.log(err)
        })
};

export const uploadSelfieImage = (user)  => {
    return {
        type: 'SELFIE_IMAGE',
        payload: user
    }
}


export const compareBitcoin = () => dispatch => {
    fetch(compareBTCUrl)
    .then(res => res.json())
    .then(data => dispatch({
        type: COMPARE_BITCOIN,
        payload: data
    }))
}

export const compareEthereum = () => dispatch => {
    fetch(compareETHUrl)
    .then(res => res.json())
    .then(data => dispatch({
        type: COMPARE_ETHEREUM,
        payload: data
    }))
}

export const countryList = (user) => dispatch => {
    fetch(`${baseUrlLive}countries?token=${token}`, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: user.jwt
        }
    })
        .then(res => res.json())
        .then(data => dispatch({
            type: COUNTRY_LIST,
            payload:data
        }))
        .catch(err => {
            console.log(err)
        })
}

export const stateList = (user) => dispatch => {
    fetch(`${baseUrlLive}states/by/country?token=${token}`, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization:user.jwt
        },
        body: JSON.stringify({
            'country_code': user.county_code
        })
    })
        .then(res => res.json())
        .then(data => dispatch({
            type: STATE_LIST,
            payload: data
        }))
        .catch(err => {
            console.log(err)
        })
}

export const saveCountryState = (countryState) => {
    return {
        type: 'SAVE_COUNTRY_STATE',
        payload: countryState
    }
}

export const saveFrontId = (frontId) => {
    return {
        type: 'SAVE_FRONT_ID',
        payload: frontId
    }
}
export const saveBackId = (backId) => {
    return {
        type: 'SAVE_BACK_ID',
        payload: backId
    }
}

export const utilityKyc = (utilityId) => {
    return {
        type: 'SAVE_FULL_KYC_WITH_UTILITY',
        payload: utilityId
    }
}

export const getKycDocumets = (userData) => dispatch => {
    fetch(`${baseUrlLive}get/kyc-documents?token=${token}`, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: userData.jwt
        },
        body: JSON.stringify({
            'country_code': userData.country_code,
            'state_code':userData.state_code
        })
    })
        .then(res => res.json())
        .then(data => dispatch({
            type: GET_KYC_DOCUMENTS,
            payload: data
        }))
        .catch(err => {
            console.log(err)
        })
}

export const checkKycStatus = (user) => dispatch => {
    fetch(`${baseUrlLive}user/kyc/status?token=${token}`, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: user.jwt
        }
    })
        .then(res => res.json())
        .then(data => dispatch({
            type: CHECK_KYC_STATUS,
            payload: data
        }))
        .catch(err => {
            console.log(err)
        })
}
