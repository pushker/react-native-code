import { connect } from 'react-redux';
import Scanner from '../../components/Screens/Wallet/Scanner.js';

import { sendQRCode } from '../../actions/walletAction';


const mapStateToProps = ( state ) => {
    return {
        user: state.user,
        wallet: state.wallet
    }
}

const mapDispatchToProps = ( dispatch ) => {
    return {
        sendQRCode: (data) => {
            dispatch(sendQRCode(data))
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Scanner);