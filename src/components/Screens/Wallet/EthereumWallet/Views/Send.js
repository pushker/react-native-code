import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    TextInput,
    TouchableOpacity,
    Dimensions
} from 'react-native';

import Icon from 'react-native-vector-icons/Feather';



export default class Send extends Component {
    constructor(props) {
        super(props);
        this.state = {
            coinAmount: '',
            sendAddress: this.props.qrcodeData,
        }
    }



    render() {
        const message = this.props.payload
        const ethereumBalance = this.props.ethereumBalance
        const width = Dimensions.get('window').width
        const {navigate} = this.props.navigate
        return (
            <React.Fragment>
            <View style={styles.hmContainer}>
                <View style={styles.hmHeader}>
                <Text style={{width: 200, position: 'absolute', left: 20, top: 30, color: 'red'}}>{message}</Text>
                <Text style={{width: 300, position: 'absolute', left: 20, top: 80, color: '#0F44A0', fontSize: 16}}>Current Balance: {ethereumBalance}</Text>
                    <View style={{flexDirection: 'row', alignItems: 'center', paddingRight: 20}} >
                        <Image source={require('../../../../../img/Eth_c.png')} style={{height: 30, width: 30, marginRight: 10}} ></Image>
                        <Text style={{fontSize: 18, color: '#0F44A0', fontWeight: '300'}}>
                            Send Ethereum
                        </Text>
                    </View>
                </View>
                <View style={styles.btmContainer}>
                <ScrollView>
                    <View>
                        <Text style={{marginLeft: 20}}>Amount to Send:</Text>
                        <View style={styles.txtInputContainer}>
                            <TextInput keyboardType={'numeric'} placeholder={'0.23'} style={styles.txtInput} onChangeText={(coinAmount) => this.setState({coinAmount: coinAmount})} underlineColorAndroid={'rgba(0,0,0,0)'} />
                        </View>
                    </View>
                    <View>
                        <Text style={{marginLeft: 20}}>Ethereum Source (Encrypted for your security):</Text>
                        <View style={styles.txtInputContainer}>
                            <TextInput style={styles.txtInput} placeholder='My Javvy Ethereum Wallet' editable={false} underlineColorAndroid={'rgba(0,0,0,0)'} />
                        </View>
                    </View>
                    <View>
                        <Text style={{marginLeft: 20}}>Send Ethereum to:</Text>
                        <View style={[styles.txtInputContainer, {flexDirection: 'row', justifyContent: 'space-around'}]}>
                                    <TextInput style={[styles.txtInput, { width: Dimensions.get('window').width - 60 }]} placeholder='Enter or Scan Address' value={this.props.qrcodeData}
                                        onChangeText={(sendAddress) => this.setState({ sendAddress: sendAddress })} underlineColorAndroid={'rgba(0,0,0,0)'} />
                            <TouchableOpacity onPress={() => navigate('Scanner')}><Icon name='camera' size={34} color={'#0F44A0'}></Icon></TouchableOpacity>
                        </View>
                    </View>
                    <View>
                        <Text style={{marginLeft: 20}}>Description/Memo:</Text>
                        <View style={styles.txtInputContainer}>
                            <TextInput style={styles.txtInput} placeholder='Enter Memo' underlineColorAndroid={'rgba(0,0,0,0)'} />
                        </View>
                    </View>
                    <View style={styles.btnSendContainer}>
                        <TouchableOpacity style={styles.btnSend} onPress={() => {this.props.sendEthereum(this.state.coinAmount, this.state.sendAddress)}}>
                            <Text style={styles.btnSendText}>Send</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
                </View>
            </View>
            </React.Fragment>
        )
    }
}



const styles = StyleSheet.create({
    hmContainer: {
        backgroundColor: '#fff',
        height: Dimensions.get('window').height,
        width: Dimensions.get('window').width
    },
    hmHeader: {
        paddingTop: 20,
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        width: '100%'
    },
    btmContainer: {
        paddingTop: 70,
        justifyContent: 'space-around',
        height: '100%'
    },
    txtInputContainer: {
        marginBottom: 15,
        alignItems: 'center'
    },
    txtInput: {
        width: Dimensions.get('window').width - 20,
        height: 45.59,
        borderRadius: 50,
        backgroundColor: '#EDEDED',
        opacity: 0.8,
        padding: 10,
        fontSize: 16,
    },
    btnSendContainer: {
        alignItems: 'center',
        marginTop: 0
    },
    btnSend: {
        width: 230,
        marginBottom: 10.1,
        shadowColor: '#0F44A0',
        shadowOffset:{width: 2, height: 2},
        shadowOpacity: .8,
        backgroundColor: '#0F44A0',
        height: 52.94,
        marginTop: 3,
        borderRadius: 50,
        justifyContent: 'center',
        alignItems:'center'
    },
    btnSendText: {
        color: 'white',
        fontSize: 18
    },
})