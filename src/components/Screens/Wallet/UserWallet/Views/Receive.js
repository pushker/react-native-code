import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    TextInput,
    TouchableOpacity,
    Dimensions,
    Clipboard,
    Alert
} from 'react-native';

import QRCode from 'react-native-qrcode-svg';


export default class Receive extends Component {
    constructor(props) {
        super(props);
        this.state = {
            coinAmount: '',
            sendAddress: '',
            message: ''
        }
        this._setContent = this._setContent.bind(this);
    }


    _setContent() {
        let payload = this.props.payload
        Clipboard.setString(payload)
        Alert.alert("Address Copied")
    }



    render() {
        const message = this.state.message
        const code = this.props.payload
        const width = Dimensions.get('window').width;
        const name = this.props.belowTilte
        const UserCoinBalance = this.props.UserCoinBalance;
        console.log(code, 'code');
        return (
            <React.Fragment>
            <View style={styles.hmContainer}>
                    <View style={styles.hmHeader}>
                        <Text style={{ width: 280, left: 0, top: 10, color: '#0F44A0', fontSize: 16 }} >Current Balance: {UserCoinBalance} </Text>
                <Text style={{width: 200, position: 'absolute', left: 20, top: 30}}></Text>

                </View>
                <View style={styles.btmContainer}>
                <ScrollView>
                    <View style={{alignItems: 'center'}}>
                        <Text style={{color: 'green'}}>{message}</Text>
                                <Text style={{ marginTop: 10, marginBottom: 20, fontSize: 20 }}>{name} Address</Text>
                        <View style={styles.txtInputContainer}>
                        <QRCode value={code !==''?code:'code'} size={280} />

                        </View>
                    </View>

                    <View style={styles.btnSendContainer}>
                        <TouchableOpacity style={styles.btnSend} onPress={this._setContent}>
                            <Text style={styles.btnSendText}>Copy</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
                </View>
            </View>
            </React.Fragment>
        )
    }
}



const styles = StyleSheet.create({
    hmContainer: {
        backgroundColor: '#fff',
        height: Dimensions.get('window').height,
        width: Dimensions.get('window').width
    },
    hmHeader: {
        paddingTop: 20,
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        width: '100%'
    },
    btmContainer: {
        paddingTop: 20,
        justifyContent: 'space-around',
        height: '100%'
    },
    txtInputContainer: {
        marginBottom: 15,
        alignItems: 'center',
        justifyContent: 'center'
    },
    txtInput: {
        width: Dimensions.get('window').width - 20,
        height: 45.59,
        borderRadius: 50,
        backgroundColor: '#EDEDED',
        opacity: 0.8,
        padding: 10,
        fontSize: 16,
    },
    btnSendContainer: {
        alignItems: 'center',
        marginTop: 0
    },
    btnSend: {
        width: 230,
        marginBottom: 10.1,
        shadowColor: '#0F44A0',
        shadowOffset:{width: 2, height: 2},
        shadowOpacity: .8,
        backgroundColor: '#0F44A0',
        height: 52.94,
        marginTop: 3,
        borderRadius: 50,
        justifyContent: 'center',
        alignItems:'center'
    },
    btnSendText: {
        color: 'white',
        fontSize: 18
    },
})