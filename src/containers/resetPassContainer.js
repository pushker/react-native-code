import { connect } from 'react-redux';
import ResetPass from '../components/Screens/ResetPass';

import { forgotPassword, updatePassword } from '../actions/userAction';


const mapStateToProps = ( state ) => {
    return {
        user: state.user
    }
}

const mapDispatchToProps = ( dispatch ) => {
    return {
        forgotPassword: (data) => {
            dispatch(forgotPassword(data))
        },
        updatePassword: (data) => {
            dispatch(updatePassword(data))
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ResetPass);