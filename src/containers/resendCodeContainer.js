import { connect } from 'react-redux';
import { resendActivationCode } from '../actions/userAction';
import ResendCode from '../components/Screens/Register/Views/ResendCode.js';


const mapStateToProps = ( state ) => {
    return {
        user: state.user
    }
}

const mapDispatchToProps = ( dispatch ) => {
    return {
        resendActivationCode: (user) => {
            dispatch(resendActivationCode(user))
        }
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(ResendCode);