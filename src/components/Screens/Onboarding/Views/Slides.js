import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Dimensions,
    Alert,
    TouchableOpacity
} from 'react-native';

import Swiper from '../Swiper';
import Icon from 'react-native-vector-icons/Feather';
import { RNCamera } from 'react-native-camera';


export default class Slides extends Component {

    takePicture = async function () {
        if (this.camera) {
            const options = { quality: 0.5, base64: true };
            const data = await this.camera.takePictureAsync(options);
            console.log(data.uri);
            Alert.alert('Image ' + data.uri + ' successfully uploaded')
        }
    };


    render() {
        const navigate = this.props.navigate;
        return (
            <Swiper navigate={navigate}>

                <View style={styles.slide}>
                    <View>
                        <RNCamera
                            ref={ref => {
                                this.camera = ref;
                            }}
                            style={styles.preview}
                            type={RNCamera.Constants.Type.front}
                            flashMode={RNCamera.Constants.FlashMode.on}
                            permissionDialogTitle={'Permission to use camera'}
                            permissionDialogMessage={'We need your permission to use your camera phone'}
                        >

                        </RNCamera>
                    </View>
                    <ScrollView>
                        <Text style={{ fontSize: 24, color: '#fff', padding: 20 }}>
                            Let's take a selfie
                        </Text>
                        <Text style={{ fontSize: 16, color: '#fff', paddingLeft: 20, paddingRight: 20 }}>
                            We’ve all taken at least one selfie in our lives,
                            so this should be pretty easy. We need to see your
                            full face within a well lit room. No hats, hoodies,
                            masks, etc. With a steady hand, hold the front facing
                            camera of your phone a few inches from your face, and
                            press the capture button. Say, “Cheese!”
                        </Text>
                        <TouchableOpacity
                            onPress={this.takePicture.bind(this)}
                            style={styles.capture} >
                            <Text>Yo</Text>
                        </TouchableOpacity>
                    </ScrollView>
                </View>
                <View style={styles.slide}>
                    <View>
                        <RNCamera
                            ref={ref => {
                                this.camera = ref;
                            }}
                            style={styles.preview}
                            type={RNCamera.Constants.Type.back}
                            onBarCodeRead={console.log('barcode detected')}
                            flashMode={RNCamera.Constants.FlashMode.on}
                            permissionDialogTitle={'Permission to use camera'}
                            permissionDialogMessage={'We need your permission to use your camera phone'}
                        >

                        </RNCamera>
                    </View>

                    <ScrollView>
                        <Text style={{ fontSize: 24, color: '#fff', padding: 20 }}>
                            Front of your ID Please?
                        </Text>
                        <Text style={{ fontSize: 16, color: '#fff', paddingLeft: 20, paddingRight: 20 }}>
                            Now we need a picture of your ID to match it to your face.
                            Fraudulent tomfoolery is not acceptable.
                            For this part, only the front of your ID please.
                            It’s best if you place it on a flat surface with adequate
                            light and all four corners visible. Press the capture button.
                        </Text>
                        <TouchableOpacity
                            onPress={this.takePicture.bind(this)}
                            style={styles.capture}
                        >
                            <Text style={{ fontSize: 18, color: '#2756A9' }}>CAPTURE</Text>
                        </TouchableOpacity>
                    </ScrollView>

                </View>
                <View style={styles.slide}>
                    <View>
                        <RNCamera
                            ref={ref => {
                                this.camera = ref;
                            }}
                            onBarCodeRead={console.log('barcode detected')}
                            style={styles.preview}
                            type={RNCamera.Constants.Type.back}
                            flashMode={RNCamera.Constants.FlashMode.on}
                            permissionDialogTitle={'Permission to use camera'}
                            permissionDialogMessage={'We need your permission to use your camera phone'}
                        >

                        </RNCamera>
                    </View>

                    <ScrollView>
                        <Text style={{ fontSize: 24, color: '#fff', padding: 20 }}>
                            Back of your ID Please?
                        </Text>
                        <Text style={{ fontSize: 16, color: '#fff', paddingLeft: 20, paddingRight: 20 }}>
                            You guessed it, the back of your ID.
                            Flat surface, with decent lighting nearby;
                            all four corners. We want to make sure there’s no
                            shady shenanigans underway when it comes to your identity,
                            and keeping it safe! Once again, press the capture button when set.
                        </Text>
                        <TouchableOpacity
                            onPress={this.takePicture.bind(this)}
                            style={styles.capture}
                        >
                            <Text style={{ fontSize: 18, color: '#2756A9' }}>CAPTURE</Text>
                        </TouchableOpacity>
                    </ScrollView>

                </View>
                <View style={styles.slide}>
                    <View>
                        <RNCamera
                            ref={ref => {
                                this.camera = ref;
                            }}
                            style={styles.preview}
                            type={RNCamera.Constants.Type.front}
                            flashMode={RNCamera.Constants.FlashMode.on}
                            permissionDialogTitle={'Permission to use camera'}
                            permissionDialogMessage={'We need your permission to use your camera phone'}
                        >

                        </RNCamera>
                    </View>


                    <ScrollView>
                        <Text style={{ fontSize: 24, color: '#fff', padding: 20 }}>
                            Utility Bill
                        </Text>
                        <Text style={{ fontSize: 16, color: '#fff', paddingLeft: 20, paddingRight: 20 }}>
                            Last, but not least,
                            we need a utility bill in YOUR name and physical address.
                            Just the front of the document in good lighting is all we need. Viola.
                            You’re now about to open yourself up to the awesome world of crypto. Enjoy, friend!
                        </Text>
                        <TouchableOpacity
                            onPress={this.takePicture.bind(this)}
                            style={styles.capture}
                        >
                            <Text style={{ fontSize: 18, color: '#2756A9' }}>CAPTURE</Text>
                        </TouchableOpacity>
                    </ScrollView>

                </View>

            </Swiper>
        )
    }
}


const iconStyles = {
    size: 100,
    color: '#FFFFFF',
};

const styles = StyleSheet.create({
    // Slide styles
    slide: {
        flex: 1,   // Take up all screen
        // justifyContent: 'center',   // Center vertically
        // alignItems: 'center',       // Center horizontally
        backgroundColor: '#2756A9'
    },
    // Header styles
    header: {
        color: '#FFFFFF',
        fontFamily: 'Avenir',
        fontSize: 30,
        fontWeight: 'bold',
        marginVertical: 15,
    },
    // Text below header
    text: {
        color: '#FFFFFF',
        fontFamily: 'Avenir',
        fontSize: 18,
        marginHorizontal: 40,
        textAlign: 'center',
    },
    preview: {
        height: Dimensions.get('window').height / 2,
        justifyContent: 'center',
        alignItems: 'center'
    },
    capture: {
        backgroundColor: 'white',
        borderRadius: 5,
        padding: 10,
        paddingHorizontal: 20,
        margin: 10,
        position: 'absolute',
        top: Dimensions.get('window').width / 1.84,
        alignSelf: 'flex-start',
    }
});