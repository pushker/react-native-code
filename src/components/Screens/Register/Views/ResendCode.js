import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    TouchableOpacity,
    Dimensions,
    StatusBar,
    Alert,
    ActivityIndicator
} from 'react-native';




export default class ResendCode extends Component {
    constructor(props){
        super(props);
        this.state = {
            email: ''
        }
        this.handleSubmit=this.handleSubmit.bind(this);
        this.validateNewCode=this.validateNewCode.bind(this);
    }


    static navigationOptions = {
        gesturesEnabled: false,
        headerStyle: {
            backgroundColor: '#fff'
        },
        headerTintColor: '#0F44A0'
    }


    validateNewCode() {
        const clearActivityIndicator = () => this.setState({ isLoading: false })
        const newActivationCode = "Account Activation Code sent to your email."
        const errMessage = "Required  Fields: email";
        console.log(this.props.user.activationCode);
        if (this.props.user.activationCode.message === newActivationCode) {
            this.props.navigation.navigate('ActivateAccount')
            clearActivityIndicator()
        } else if (this.props.user.activationCode.message === errMessage) {
            Alert.alert("Please your enter email address");
            clearActivityIndicator()
         }else {
            Alert.alert(this.props.user.activationCode.message)
            clearActivityIndicator()
        }
    }



      handleSubmit(event){
          event.preventDefault();
          var emailRegex = new RegExp("^[a-z0-9_.-]+\.?[a-z0-9_-]+@[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?[.][a-z]{2,61}(?:\.[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?)*$");
          const user = {
              email: this.state.email
          }
          if (user.email !== '' && emailRegex.test(user.email) === true) {
              this.props.resendActivationCode(user)
              this.setState({ isLoading: true })
              setTimeout(() => {
                  this.validateNewCode(user)
              }, 3000)
          } else if (user.email !== '' && emailRegex.test(user.email) === false) {
              Alert.alert(``, 'Please enter valid email');
          } else {
              Alert.alert(``, 'Please enter an email address');
          }


      }



      render() {

          const { navigate } = this.props.navigation;
          const width = Dimensions.get('window').width
          const height = Dimensions.get('window').height
          const success = "Account Activation Code sent to your email."
          const err = this.props.user.activationCode.message !== success ? this.props.user.activationCode.message : null

          return(
            <React.Fragment>
            <StatusBar barStyle={'dark-content'}/>
                  <View style={styles.container}>
                      {this.state.isLoading === true ?
                          <View style={{ position: 'absolute', right: 0, left: 0, top: 0, bottom: 0, backgroundColor: '#fff', opacity: .8, height: height, zIndex: 999, justifyContent: 'center', alignItems: 'center' }}><ActivityIndicator size="large" color="#0F44A0" /></View> : null}
                  <View style={styles.lgn_container}>



                  <View style={width === 320 ? styles320.lgn_txtContainer : styles.lgn_txtContainer}>
                    <Text style={{fontSize: 18, fontWeight: '200', padding: 12}}>If you did not recieve an activation code, please enter your registered email address and we will send you a new one.</Text>
                  </View>

                  {/* <View style={{width: '100%', alignItems:'center', paddingBottom: 10}}><Text style={{color: 'red'}}>{err}</Text></View> */}
                  <View>
                    <TextInput  onChangeText={(email) => this.setState({email: email})}
                                keyboardType={'email-address'}
                                autoCapitalize={'none'}
                                style={width === 320 ? styles320.lgn_txtInput : styles.lgn_txtInput}
                                placeholder={'Email Address'}
                                underlineColorAndroid={'rgba(0,0,0,0)'}>
                    </TextInput>
                  </View>

                  <View style={width === 320 ? styles320.btnSignInContainer : styles.btnSignInContainer}>
                    <TouchableOpacity style={width === 320 ? styles320.btnSignIn : styles.btnSignIn} onPress={this.handleSubmit}>
                        <Text style={styles.btnSignInText}>Send</Text>
                    </TouchableOpacity>
                  </View>

                </View>
              </View>
              </React.Fragment>
          )
      }

  }



  const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: '#fff',

    },
    lgn_icon: {
        height: 136.62,
        width: 140.62,
    },
    lgn_container: {
        width: 296,
        height: 471,
        alignItems: 'center',
    },
    lgn_txtInput: {
        width: 325.3,
        height: 45.59,
        borderRadius: 50,
        backgroundColor: '#EDEDED',
        opacity: 0.8,
        padding: 10,
        fontSize: 16,
    },
    lgn_txtContainer:{
        width: 325.3,
        height: 122.3,
        marginTop: 7.1,
        justifyContent: 'space-between',
        paddingLeft: 10,
        paddingRight: 10
    },
    btnSignInContainer: {
        alignItems: 'center',
        marginTop: 20
      },
      btnSignIn: {
        width: 230,
        marginBottom: 10.1,
        shadowColor: '#0F44A0',
        shadowOffset:{width: 2, height: 2},
        shadowOpacity: .8,
        backgroundColor: '#0F44A0',
        height:52.94,
        marginTop: 3,
        borderRadius: 50,
        justifyContent: 'center',
        alignItems:'center'
      },
      btnSignInText: {
        color: 'white',
        fontSize: 18
      },

  })