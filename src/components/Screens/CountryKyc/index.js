import React, { Component } from 'react';
import { Fragment } from 'react-native';
import {
    StyleSheet, Text, View, StatusBar,
    Dimensions, Alert, Image, TouchableOpacity, AsyncStorage,
    ActivityIndicator, Picker,
    BackHandler
} from 'react-native';
import { Button } from 'react-native-elements';
import { Header } from '../../common/Header';

export default class CountryKyc extends Component {

    constructor(props) {
        super(props);
        this.state = {
            selectedCountryType: '',
            isLoading: false,
            pickerDisable: false,
            states: [],
            selectedStateType: '',
            buttonDisabled:true
        }
        this.onValueChange = this.onValueChange.bind(this);
        this.onValueChangeState = this.onValueChangeState.bind(this);
        this.renderStates = this.renderStates.bind(this);
        this.submitCountry = this.submitCountry.bind(this);

    }

    static navigationOptions = {
        header: null
    };



    onValueChange(e) {
        this.setState({
            selectedCountryType: e
        });
        const user = {
            jwt: 'JWT ' + this.props.user.loginData.user.jwtToken,
            county_code: e
        }
        console.log(e);
        if (e !== null && e !== undefined && e !== '') {
            this.setState({ isLoading: true, buttonDisabled: false })
            this.props.stateList(user);
            setTimeout(() => {
                this.LoaderState()
            }, 2000)
        } else {
            Alert.alert('Please select country');
            this.setState({ states: [], buttonDisabled: true  })
        }

    }
    onValueChangeState(event) {
        this.setState({
            selectedStateType: event
        });
    }

    LoaderState() {
        const clearActivityIndicator = () => this.setState({ isLoading: false })
        if (this.props.user.stateListData && this.props.user.stateListData.states.length > 0) {
            this.setState({
                pickerDisable: true,
                states: this.props.user.stateListData.states,
            })
        } else {
            this.setState({
                pickerDisable: false,
                states: [],
                selectedStateType:''
            })
        }
        clearActivityIndicator()
    }

    renderStates() {
        if (this.state.states && this.state.states.length > 0) {
            return (
                <Picker
                    style={{ width: 170, backgroundColor: 'lightgrey' }} selectedValue={this.state.selectedStateType} onValueChange={this.onValueChangeState}>
                    {this.state.states.map((state, index) => {
                        if (state.length !== 0) {
                            return (
                                <Picker.Item key={state.id} label={state.name} value={state.abbreviation} />
                            )
                        }
                    })}
                </Picker>
            )
        } else {
            return (
                <Picker
                    style={{ width: 170, backgroundColor: 'lightgrey' }} enabled={this.state.pickerDisable}>
                    <Picker.Item key="{country.id}" label="Select State" value="" />
                </Picker>
            )
        }
    }

    submitCountry() {
        console.log(this.state, 'onsubmit');
        if (this.state.selectedCountryType !== '' && this.state.selectedCountryType !== undefined) {
            const data = {
                country: this.state.selectedCountryType,
                state: this.state.selectedStateType
            }
            const userData = {
                jwt: 'JWT ' + this.props.user.loginData.user.jwtToken,
                country_code: this.state.selectedCountryType,
                state_code: this.state.selectedStateType
            }
            this.props.saveCountryState(data);
            this.props.getKycDocumets(userData);
            this.props.navigation.navigate('FrontKyc');
        }
    }



    render() {
        const width = Dimensions.get('window').width;
        const height = Dimensions.get('window').height;
        const { navigate } = this.props.navigation;
        return (
            <React.Fragment>
                <Header headerText={'Javvy Verification - KYC'} />
                {this.state.isLoading === true ?
                    <View style={{ position: 'absolute', right: 0, left: 0, top: 0, bottom: 0, backgroundColor: '#fff', opacity: .8, height: height, zIndex: 999, justifyContent: 'center', alignItems: 'center' }}><ActivityIndicator size="large" color="#0F44A0" /></View> : null}
                <View>
                    <Text style={{ fontSize: 16, color: 'black', paddingTop: 10, marginBottom: 25, marginTop: 15, textAlign: 'center', fontWeight: '900' }}>Please select country and state of resident</Text>
                    <View style={{ width: Dimensions.get('window').width, flexDirection: 'row', justifyContent: 'space-around' }}>
                        <View>
                            <Picker
                                style={{ width: 170, backgroundColor: 'lightgrey' }}
                                selectedValue={this.state.selectedCountryType} onValueChange={this.onValueChange}>
                                <Picker.Item key='01' label={'Select Country'} value="" />
                                {this.props.user.countryListData.data.map((country, index) => {
                                    if (country.length !== 0) {
                                        return (
                                            <Picker.Item key={country.id} label={country.name} value={country.code} />
                                        )
                                    }
                                })}
                            </Picker>
                        </View>
                        <View>
                            {this.renderStates()}

                        </View>
                    </View>
                    <View style={{ marginTop: 25 }}>
                        <Button title='Submit' disabled={this.state.buttonDisabled} onPress={this.submitCountry} buttonStyle={{ backgroundColor: "#2756A9" }} />
                    </View>
                </View>
            </React.Fragment>

        )
    }
}


const styles = StyleSheet.create({
    // Slide styles
    slide: {
        flex: 0,
        backgroundColor: '#2756A9'
    },
    container: {
        flex: 1,
        paddingTop: 10,
        backgroundColor: '#ecf0f1',
    },
});
