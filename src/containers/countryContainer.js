import { connect } from 'react-redux';
import CountryKyc from '../components/Screens/CountryKyc';
import { stateList, saveCountryState, getKycDocumets } from '../actions/userAction';


const mapStateToProps = (state) => {
    return {
        user: state.user,
        wallet: state.wallet
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        stateList: (data) => {
            dispatch(stateList(data))
        },
        saveCountryState: (data) => {
            dispatch(saveCountryState(data))
        },
        getKycDocumets: (data) => {
            dispatch(getKycDocumets(data))
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(CountryKyc);