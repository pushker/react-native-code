import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    TouchableOpacity,
    Dimensions
} from 'react-native';


import Icon from 'react-native-vector-icons/FontAwesome';


export default class Sell extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }



    render() {
        const { name } = this.props
        console.log(name, 'sell');
        const { navigate } = this.props.navigate
        return (
            <View style={styles.hmContainer}>
                <View style={styles.hmHeader}>
                    <View style={{ justifyContent: 'center', alignItems: 'center', backgroundColor: 'orange', width: 100, height: 100, borderRadius: 50 }}><Icon name='exclamation' color="white" size={60}></Icon></View>
                    <Text style={{ fontSize: 18, color: 'grey', fontWeight: '400', marginTop: 40 }}>
                        Oh snap! This feature is soon to be released
                    </Text>
                    <TouchableOpacity onPress={() => navigate('JavvyUpdates')}>
                        <Text style={{ fontSize: 16, color: '#0F44A0', fontWeight: '400', marginTop: 10, textDecorationLine: 'underline' }}>
                            Support Javvy
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    hmContainer: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 35
    },
    hmHeader: {
        padding: 15,
        justifyContent: 'center',
        alignItems: 'center',
    }
})