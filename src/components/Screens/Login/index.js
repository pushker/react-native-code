import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  KeyboardAvoidingView,
  Keyboard,
  Animated,
  Image,
  TextInput,
  TouchableOpacity,
  Dimensions,
  StatusBar,
  ActivityIndicator,
  Alert
} from 'react-native';


const IMAGE_HEIGHT = 141.39
const IMAGE_WIDTH =  104.88

export default class Login extends Component {
    constructor(props) {
        super(props);
      this.state = {
          //shivssharma1990@gmail.com viv100@mailinator.com jash@mailinator.com
            email: '',
            password: '',
            isLoading: false
        }
        this.handleSubmit = this.handleSubmit.bind(this);
        this.validateUser = this.validateUser.bind(this);
        this.focusPassword = this.focusPassword.bind(this);

        this.imageHeight = new Animated.Value(IMAGE_HEIGHT);
        this.imageWidth = new Animated.Value(IMAGE_WIDTH);
    }

    componentWillMount () {
      if (Platform.OS === 'ios') {
        this.keyboardWillShowSub = Keyboard.addListener('keyboardWillShow', this.keyboardWillShow);
        this.keyboardWillHideSub = Keyboard.addListener('keyboardWillHide', this.keyboardWillHide);
      } else {
          this.keyboardWillShowSub = Keyboard.addListener('keyboardDidShow', this.keyboardWillShow);
          this.keyboardWillHideSub = Keyboard.addListener('keyboardDidHide', this.keyboardWillHide);
      }
    }

    componentWillUnmount() {
      this.keyboardWillShowSub.remove();
      this.keyboardWillHideSub.remove();
    }

    keyboardWillShow = (event) => {
      Animated.timing(this.imageHeight, {
        duration: event.duration,
        toValue: (108),
      }).start();

      Animated.timing(this.imageWidth, {
        duration: event.duration,
        toValue: (80),
      }).start();
    };

    keyboardWillHide = (event) => {
      Animated.timing(this.imageHeight, {
        // duration: event.duration,
        toValue: IMAGE_HEIGHT,
      }).start();

      Animated.timing(this.imageWidth, {
        // duration: event.duration,
        toValue: IMAGE_WIDTH,
      }).start();
    };


  static navigationOptions = {
    gesturesEnabled: false,
    header: null
  }




  validateUser() {
    const clearActivityIndicator = () => this.setState({isLoading: false})
    const validUser = "User Exists";
    const accountNotActive = "Your account is not activated yet."
    const success = "User Exists";
    const err = this.props.user.loginData.message !== success ? 'You shall not pass! Please check your username and/or password.' : null;

    if (this.props.user.loginData.message === validUser) {

      console.log(this.props.user.loginData)

      this.props.navigation.navigate('Dashboard')
      clearActivityIndicator()

    } else if (this.props.user.loginData.message === accountNotActive) {
      this.props.navigation.navigate('ActivateAccount')
      clearActivityIndicator()

    } else {
      Alert.alert(``,
        err,
        [
          { text: 'OK', onPress: () => console.log('OK Pressed') }
        ])
      clearActivityIndicator()

    }
  }


  handleSubmit(event) {
    event.preventDefault();
    const user = {
      email: this.state.email,
      password: this.state.password
    };
    console.log(user, 'check login');
    var emailRegex = new RegExp("^[a-z0-9_.-]+\.?[a-z0-9_-]+@[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?[.][a-z]{2,61}(?:\.[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?)*$");

    if (user.email === '' || user.email === undefined || user.password === '' || user.password === undefined) {
      const err = 'You shall not pass! Please check your username and/or password.';
      Alert.alert(``,
        err,
        [
          { text: 'OK' }
        ])
    } else {
      if (emailRegex.test(user.email) === false) {
        Alert.alert(``,
          `Please enter valid email address.`,
          [
            { text: 'OK'}
          ])
      } else {
        this.props.login(user)
        this.setState({ isLoading: true })

        setTimeout(() => {
          this.validateUser(user)
        }, 1000)
      }

    }


  }
  focusPassword() {
    this.PassInput.focus();
  }



  render() {

    const { navigate } = this.props.navigation;
    const width = Dimensions.get('window').width;
    const height = Dimensions.get('window').height;

    return (
      <React.Fragment>

        <StatusBar barStyle="light-content" />
          <KeyboardAvoidingView style={styles.container} behavior="padding">
            { this.state.isLoading === true ?
            <View style={{position: 'absolute', right: 0, left: 0, top: 0, bottom: 0, backgroundColor: '#fff', opacity: .8, height: height, zIndex: 999, justifyContent: 'center', alignItems: 'center'}}><ActivityIndicator size="large" color="#0F44A0" /></View> : null }
            <View style={styles.lgn_container}>
            <View style={{width: '100%', justifyContent: 'center', alignItems: 'center', paddingTop: 40, paddingBottom: 10, backgroundColor: '#0F44A0'}}>
              <Animated.Image style={[width === 320 ? styles320.lgn_icon : styles.lgn_icon, {height: this.imageHeight, width: this.imageWidth}]} source={require('../../../img/lg_icon.png')} />
              <Text style={{textAlign: 'center', paddingTop: 5, fontSize: 10, color: 'white'}}>v1.0.0</Text>
            </View>
            <View>
              <View style={{width: '100%', alignItems:'center', paddingBottom: 10}}>
              </View>

              <View>
                <TextInput onChangeText={(email) => this.setState({email: email})}
                          keyboardType={'email-address'}
                          autoCapitalize={'none'}
                          style={width === 320 ? styles320.lgn_txtInput : styles.lgn_txtInput}
                          placeholder={'Email Address'}
                          underlineColorAndroid={'rgba(0,0,0,0)'}
                          autoCorrect={false}
                        onSubmitEditing={this.focusPassword}>
                </TextInput>
                <TextInput onChangeText={(password) => this.setState({ password: password })}
                  secureTextEntry={true}
                  style={[width === 320 ? styles320.lgn_txtInput : styles.lgn_txtInput, styles.fixup]}
                  placeholder={'Password'}
                  ref={(ref) => { this.PassInput = ref; }}
                  underlineColorAndroid={'rgba(0,0,0,0)'}
                  onSubmitEditing={this.handleSubmit}>
                </TextInput>
              </View>

              <View>
                <View style={width === 320 ? styles320.rstPassContainer : styles.rstPassContainer}>
                  <Text style={styles.rstPassText}>Trouble signing in?</Text>
                  <TouchableOpacity onPress={() => navigate('ResetPass')}>
                    <Text style={styles.rstPassBtn}>Click Here</Text>
                  </TouchableOpacity>
                </View>
              </View>

              <View style={width === 320 ? styles320.btnSignInContainer : styles.btnSignInContainer}>
                <TouchableOpacity style={width === 320 ? styles320.btnSignIn : styles.btnSignIn} onPress={this.handleSubmit}>
                  <Text style={styles.btnSignInText}>Sign In</Text>
                </TouchableOpacity>
              </View>


              <View style={styles.signUpContainer}>
                <Text style={styles.signUpText}>Don't have an account?</Text>
                <TouchableOpacity onPress={() => navigate('CreateAccount')}>
                  <Text style={styles.signUpBtn}>Sign Up</Text>
                </TouchableOpacity>
              </View>

            </View>

            </View>
          </KeyboardAvoidingView>




      </React.Fragment>

    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  lgn_icon: {
    // height: 141.39,
    // width: 104.88,
  },
  lgn_container: {
    width: '100%',
    height: 450,
    alignItems: 'center',
  },
  lgn_txtInput: {
    width: 325.3,
    height: 45.59,
    borderRadius: 50,
    backgroundColor: '#EDEDED',
    opacity: 0.8,
    padding: 10,
    fontSize: 16,
    marginTop: 20
  },
  lgn_txtContainer:{
    width: 325.3,
    height: 102.3,
    marginTop: 47.1,
    justifyContent: 'space-between',
  },
  fixup:{
    marginTop: 13.1,
    marginBottom: 13.1
  },
  rstPassContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 28.1
  },
  rstPassText: {
    opacity: 0.8,
    padding: 5,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center'
  },
  rstPassBtn: {
    color: '#0F44A0',
    fontWeight: '600'
  },
  btnSignInContainer: {
    alignItems: 'center',
    marginTop: 0
  },
  btnSignIn: {
    width: 230,
    marginBottom: 10.1,
    shadowColor: '#0F44A0',
    shadowOffset:{width: 2, height: 2},
    shadowOpacity: .8,
    backgroundColor: '#0F44A0',
    height: 52.94,
    // marginTop: 23,
    borderRadius: 50,
    justifyContent: 'center',
    alignItems:'center'
  },
  btnSignInText: {
    color: 'white',
    fontSize: 18
  },
  signUpContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  signUpText: {
    opacity: 0.8,
    padding: 10
  },
  signUpBtn: {
    color: '#0F44A0',
    fontWeight: '600'
  }
});


const styles320 = {
  lgn_icon: {
    // height: 106.62,
    width: 97.62,
  },
  lgn_txtInput: {
    width: 305.3,
    height: 45.59,
    borderRadius: 50,
    backgroundColor: '#EDEDED',
    opacity: 0.8,
    padding: 10,
    fontSize: 16,
  },
  lgn_txtContainer:{
    width: 305.3,
    height: 102.3,
    marginTop: 47.1,
    justifyContent: 'space-between',
  },
  rstPassContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 27
  },
  btnSignInContainer: {
    alignItems: 'center'
  },
  btnSignIn: {
    width: 230,
    marginBottom: 10.1,
    shadowColor: '#0F44A0',
    shadowOffset:{width: 2, height: 2},
    shadowOpacity: .8,
    backgroundColor: '#0F44A0',
    height: 52.94,
    borderRadius: 50,
    justifyContent: 'center',
    alignItems:'center'
  },
}
