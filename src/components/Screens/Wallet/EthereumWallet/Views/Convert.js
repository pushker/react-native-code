import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    TextInput,
    TouchableOpacity,
    Dimensions
} from 'react-native';



export default class Convert extends Component {
    constructor(props) {
        super(props);
        this.state = {
            coinAmount: '',
        }
    }



    render() {
        const message = this.props.payload
        const width = Dimensions.get('window').width
        return (
            <React.Fragment>
            <View style={styles.hmContainer}>
                <View style={styles.hmHeader}>
                <Text style={{width: 200, position: 'absolute', left: 20, top: 30}}>{message}</Text>
                    <View style={{flexDirection: 'row', alignItems: 'center', paddingRight: 20}} >
                        <Image source={require('../../../../../img/Eth_c.png')} style={{height: 30, width: 30, marginRight: 10}} ></Image>
                        <Text style={{fontSize: 18, color: '#0F44A0', fontWeight: '300'}}>
                            Convert Ethereum
                        </Text>
                    </View>
                </View>
                <View style={styles.btmContainer}>
                <ScrollView>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                        <Text style={{marginLeft: 5, marginRight: 10, paddingBottom: 10}}>From:</Text>
                        <View style={styles.txtInputContainer}>
                            <TextInput keyboardType={'default'} value={'ETH'} placeholder={'ETH'} editable={false} style={styles.txtInput} underlineColorAndroid={'rgba(0,0,0,0)'} />
                        </View>
                    
                        <Text style={{marginLeft: 5, marginRight: 10, paddingBottom: 10}}>To:</Text>
                        <View style={styles.txtInputContainer}>
                            <TextInput style={styles.txtInput} placeholder={'BTC'} value={'BTC'} editable={false} underlineColorAndroid={'rgba(0,0,0,0)'} />
                        </View>
                    </View>
                    <View style={{alignItems: 'center', flexDirection: 'row'}}>
                        <Text style={{marginLeft: 5, marginRight: 10, paddingBottom: 10}}>Amount:</Text>
                        <View style={styles.txtInputContainer}>
                            <TextInput style={styles.txtInput2} placeholder='0.23' onChangeText={(coinAmount) => this.setState({coinAmount: coinAmount})} underlineColorAndroid={'rgba(0,0,0,0)'} />
                        </View>
                    </View>
                    <View style={styles.btnSendContainer}>
                        <TouchableOpacity style={styles.btnSend} onPress={() => {this.props.convertEthereum(this.state.coinAmount)}}>
                            <Text style={styles.btnSendText}>Convert</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
                </View>
            </View>
            </React.Fragment>
        )
    }
}



const styles = StyleSheet.create({
    hmContainer: {
        backgroundColor: '#fff',
        height: Dimensions.get('window').height,
        width: Dimensions.get('window').width
    },
    hmHeader: {
        paddingTop: 20,
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        width: '100%'
    },
    btmContainer: {
        paddingTop: 20,
        justifyContent: 'space-around',
        height: '100%'
    },
    txtInputContainer: {
        marginBottom: 15,
        alignItems: 'flex-start'
    },
    txtInput: {
        width: Dimensions.get('window').width / 3,
        height: 45.59,
        borderRadius: 50,
        backgroundColor: '#EDEDED',
        opacity: 0.8,
        padding: 10,
        fontSize: 16,
    },
    txtInput2: {
        width: Dimensions.get('window').width / 1.5,
        height: 45.59,
        borderRadius: 50,
        backgroundColor: '#EDEDED',
        opacity: 0.8,
        padding: 10,
        fontSize: 16,
    },
    btnSendContainer: {
        alignItems: 'center', 
        marginTop: 0
    },
    btnSend: {
        width: 240, 
        marginBottom: 10.1, 
        shadowColor: '#0F44A0', 
        shadowOffset:{width: 2, height: 2}, 
        shadowOpacity: .8, 
        backgroundColor: '#0F44A0', 
        height: 52.94, 
        marginTop: 13, 
        borderRadius: 50, 
        justifyContent: 'center', 
        alignItems:'center'
    },
    btnSendText: {
        color: 'white', 
        fontSize: 18
    },
})