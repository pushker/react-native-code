import { connect } from 'react-redux';
import { login } from '../actions/userAction';
import Login from '../components/Screens/Login';


const mapStateToProps = ( state ) => {
    return {
        user: state.user
    }
};

const mapDispatchToProps = ( dispatch ) => {
    return {
        login: (user) => {
            dispatch(login(user))
        }
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(Login);