import {
    CREATE_ADDRESS,
    USER_WALLET_DETAIL,
    CREATE_BITCOIN_TRANSACTION,
    CREATE_ETHEREUM_TRANSACTION,
    BITCOIN_DETAIL,
    SHAPESHIFT_EXCHANGE,
    GET_SHAPESHIFT_RATE,
    BITCOIN_BALANCE,
    ETHEREUM_BALANCE,
    ETHEREUM_DETAIL,
    SEND_QRCODE,
    ALL_COINS,
    FAV_COINS,
    SAVE_USER_WALLET_CODE_NAME,
    USER_COIN_DETAIL,
    USER_WALLET_BALANCE,
    CREATE_USERCOIN_TRANSACTION,
    CUSTOM_LOADER,
    CUSTOM_FLAG
} from './types';

import { token, baseUrlLive, baseUrlTest } from '../api'


export const createAddress = (wallet) => dispatch => {
    fetch(`${baseUrlLive}user/create_address?token=${token}`, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: wallet.jwt
        },
        body: JSON.stringify(wallet)
    })
        .then(res => res.json())
        .then(data => dispatch({
            type: CREATE_ADDRESS,
            payload: data
        }))
        .catch(err => {
            console.log(err)
        })
}


export const userWalletDetail = (wallet) => dispatch => {
    fetch(`${baseUrlLive}user/wallet/list?token=${token}`, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: wallet.jwt
        }
    })
        .then(res => res.json())
        .then(data => dispatch({
            type: USER_WALLET_DETAIL,
            payload: data
        }))
        .catch(err => {
            console.log(err)
        })
};


export const createBitcoinTransaction = (wallet) => dispatch => {
    fetch(`${baseUrlLive}user/bitcoin/transaction?token=${token}`, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: wallet.jwt
        },
        body: JSON.stringify(wallet)
    })
        .then(res => res.json())
        .then(data => dispatch({
            type: CREATE_BITCOIN_TRANSACTION,
            payload: data
        }))
        .catch(err => {
            console.log(err)
        })
}


//DELETE AFTER COMPLETE USER WALLET FUNCTIONALITY
export const bitcoinDetail = (wallet) => dispatch => {
    fetch(`${baseUrlLive}user/bitcoin/detail?token=${token}`, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: wallet.jwt
        }
    })
        .then(res => res.json())
        .then(data => dispatch({
            type: BITCOIN_DETAIL,
            payload: data
        }))
        .catch(err => {
            console.log(err)
        })
};


// export const convertBitcoin = (wallet) => dispatch => {
//     fetch(`${baseUrlLive}user/shapeshift/exchange?token=${token}`, {
//         method: 'POST',
//         headers: {
//             Accept: 'application/json',
//             'Content-Type': 'application/json',
//             Authorization: wallet.jwt
//         },
//         body: JSON.stringify(wallet)
//     })
//         .then(res => res.json())
//         .then(data => dispatch({
//             type: SHAPESHIFT_EXCHANGE,
//             payload: data
//         }))
//         .catch(err => {
//             console.log(err)
//         })
// };


export const createEthereumTransaction = (wallet) => dispatch => {
    fetch(`${baseUrlLive}user/ethereum/transaction?token=${token}`, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: wallet.jwt
        },
        body: JSON.stringify(wallet)
    })
        .then(res => res.json())
        .then(data => dispatch({
            type: CREATE_ETHEREUM_TRANSACTION,
            payload: data
        }))
        .catch(err => {
            console.log(err)
        })
}

export const ethereumDetail = (wallet) => dispatch => {
    fetch(`${baseUrlLive}user/ethereum/detail?token=${token}`, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: wallet.jwt
        }
    })
        .then(res => res.json())
        .then(data => dispatch({
            type: ETHEREUM_DETAIL,
            payload: data
        }))
        .catch(err => {
            console.log(err)
        })
};


export const bitcoinBalance = (wallet) => dispatch => {
    fetch(`${baseUrlLive}user/bitcoin/balance?token=${token}`, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: wallet.jwt
        },
        body: JSON.stringify(wallet)
    })
        .then(res => res.json())
        .then(data => dispatch({
            type: BITCOIN_BALANCE,
            payload: data
        }))
        .catch(err => {
            console.log(err)
        })
};


export const ethereumBalance = (wallet) => dispatch => {
    fetch(`${baseUrlLive}user/ethereum/balance?token=${token}`, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: wallet.jwt
        },
        body: JSON.stringify(wallet)
    })
        .then(res => res.json())
        .then(data => dispatch({
            type: ETHEREUM_BALANCE,
            payload: data
        }))
        .catch(err => {
            console.log(err)
        })
};


export const sendQRCode = (wallet) => dispatch => {
    dispatch({
        type: SEND_QRCODE,
        payload: wallet
    })
}


export const getCoins = (wallet) => dispatch => {

    fetch(`${baseUrlLive}user/coins?token=${token}`, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: wallet.jwt
        }
    })
        .then(res => res.json())
        .then(data => {

            dispatch({
                type: ALL_COINS,
                payload: data
            })
        })
        .catch(err => {
            console.log(err)
        })
}


export const getFavCoins = (wallet) => dispatch => {

    fetch(`${baseUrlLive}user/favourite/coins?token=${token}`, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: wallet.jwt
        }
    })
        .then(res => res.json())
        .then(data => {

            let temp_data = data

            temp_data.coins.sort(function (a, b) {
                return a.order - b.order
            })

            dispatch({
                type: FAV_COINS,
                payload: temp_data
            })
        })
        .catch(err => console.log(err))
}


/*
    [{"code":"BTC","coin_order":2},{"code":"ETH","coin_order":1},{"code":"JVY","coin_order":3},{"code":"OMG","coin_order":4}]
*/
export const postFavCoins = (wallet) => dispatch => {
    fetch(`${baseUrlLive}user/favourite/coins?token=${token}`, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: wallet.jwt
        },
        body: JSON.stringify(wallet.coin)
    })
        .then(res => res.json())
        .then(data => {
            console.log(data)
        })
}

export const saveUserWalletCodeName = (wallet) => {
    return {
        type: SAVE_USER_WALLET_CODE_NAME,
        payload: wallet
    }
}

//USER_COIN_DETAIL COMMON FUNCTION TO GET  USER COIN DETAIL VALUES
export const usercoinDetail = (wallet) => dispatch => {
    fetch(`${baseUrlLive}user/wallet/detail?token=${token}`, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: wallet.jwt
        },
        body: JSON.stringify({
            'wallet_code': wallet.wallet_code
        })
    })
        .then(res => res.json())
        .then((data) => {
            //console.log(data,'response data')
            dispatch({
                type: USER_COIN_DETAIL,
                payload: data
            })
        })
        .catch(err => {
            console.log(err)
        })
}


export const userWalletBalance = (wallet) => dispatch => {
    fetch(`${baseUrlLive}user/wallet/balance?token=${token}`, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: wallet.jwt
        },
        body: JSON.stringify({
            'wallet_code': wallet.wallet_code
        })
    })
        .then(res => res.json())
        .then((data) => {
            console.log(data, 'balance', wallet.wallet_code);
            dispatch({
                type: USER_WALLET_BALANCE,
                payload: data
            })
        })
        .catch(err => {
            console.log(err)
        })
};

/*export const createUserCoinTransaction = (wallet) => dispatch => {
    fetch(`${baseUrlLive}user/wallet/transaction?token=${token}`, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: wallet.jwt
        },
        body: JSON.stringify({
            'wallet_code': wallet.wallet_code,
            'value': wallet.value,
            'to': wallet.to
        })
    })
        .then(res => res.json())
        .then(data => dispatch({
            type: CREATE_USERCOIN_TRANSACTION,
            payload: data
        }))
        .catch(err => {
            console.log(err)
        })
}*/
export const createUserCoinTransaction = (wallet) => {
    return {
        type: CREATE_USERCOIN_TRANSACTION,
        payload:wallet
    }
}

export const customLoaderAction = (loader) => {
    return {
        type: CUSTOM_LOADER,
        payload:loader
    }
}
export const customFlag = (flag) => {
    return {
        type: CUSTOM_FLAG,
        payload: flag
    }
}
export const convertUsercoin = (data) => {
    return {
        type: SHAPESHIFT_EXCHANGE,
        payload: data
    }
}

export const estimateRateCoin = (data) => {
    return {
        type: GET_SHAPESHIFT_RATE,
        payload:data
    }
}

