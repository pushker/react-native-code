import React, {Component} from 'react';

import {
    AppRegistry,
    StyleSheet,
    Text,
    TouchableOpacity,
    Linking,
    Alert
} from 'react-native';

import QRCodeScanner from 'react-native-qrcode-scanner';

export default class Scanner extends Component {
    constructor(props){
        super(props);
        this.state = {
            QRCode: ''
        }
        this.handleQRCode = this.handleQRCode.bind(this);
    }

    static navigationOptions = {
        headerStyle: {
            backgroundColor: '#0F44A0'
        },
        headerTintColor: '#fff'
    }

    handleQRCode(){
        this.props.sendQRCode(this.state.QRCode);
        this.props.navigation.state.params.onGoBack();
        this.props.navigation.goBack();
    }
    onSuccess(e) {
        console.log(e.data,'safsaf');
        this.setState({QRCode: e.data})
        Alert.alert(`Wallet Address Scanned Successfully`,
                    `Press OK to continue and press the back button to return to your wallet.`,
                    [
                        {text: 'OK', onPress: () => this.handleQRCode()}
                    ])
    }

    render() {
        return (
            <QRCodeScanner
                onRead={this.onSuccess.bind(this)}
            />
        )
    }
}