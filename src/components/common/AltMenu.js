import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    TouchableOpacity
} from 'react-native';

export default class AltMenu extends Component {

    render() {
        const { navigate } = this.props.navigate
        const closeModal  = () => console.log('closing')
        return (
            <React.Fragment>
            <View style={styles.tabContainer}>
                <TouchableOpacity onPress={() => navigate('AccountInfo')} onPressOut={closeModal}>
                    <Text style={styles.tabText}>Account Info</Text>
                </TouchableOpacity>
            </View>
            <View style={styles.tabContainer}>
                <TouchableOpacity onPress={() => navigate('Support')} onPressOut={closeModal}>
                    <Text style={styles.tabText}>Support</Text>
                </TouchableOpacity>
            </View>
            <View style={styles.tabContainer}>
                <TouchableOpacity onPress={() => navigate('Website')} onPressOut={closeModal}>
                    <Text style={styles.tabText}>Website</Text>
                </TouchableOpacity>
            </View>
            <View style={styles.tabContainer}>
                <TouchableOpacity onPress={() => navigate('Legal')} onPressOut={closeModal}>
                    <Text style={styles.tabText}>Terms and Services</Text>
                </TouchableOpacity>
            </View>
            <View style={styles.bottomTabContainer}>
                <TouchableOpacity onPress={() => navigate('Login')} onPressOut={closeModal} >
                    <Text style={[styles.tabText, {color: 'red'}]}>Sign Out</Text>
                </TouchableOpacity>
            </View>
            </React.Fragment>
        )
    }
}

const styles = StyleSheet.create({
    tabContainer: {
        justifyContent: 'center', 
        alignItems: 'center', 
        height: '20%', 
        borderBottomWidth: 1, 
        borderBottomColor: '#2756A9'
    },
    bottomTabContainer: {
        justifyContent: 'center', 
        alignItems: 'center', 
        height: '20%', 
        borderBottomColor: '#2756A9'
    },
    tabText: {
        fontSize: 18, 
        color: '#2756A9', 
        fontWeight: '300',
        textAlign: 'center'
    }
})
