import React from 'react';
import { AppRegistry } from 'react-native';
import App from './src/App';
console.disableYellowBox = true;


import { Provider } from 'react-redux';
import store from './store';

const Main = () => {
    return (
        <React.Fragment>
            <Provider store={store}>
                <App/>
            </Provider>
        </React.Fragment>
    )
}

AppRegistry.registerComponent('javvy', () => Main);