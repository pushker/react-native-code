import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    TextInput,
    TouchableOpacity,
    Dimensions,
    Picker,
    Alert,
    ActivityIndicator
} from 'react-native';


export default class Convert extends Component {
    constructor(props) {
        super(props);
        this.state = {
            coinAmount: '',
            selectedDefaultCoin: this.props.wallet_code,
            temp: this.props.wallet_code,
            toCoin: '',
            isloading:false
        }
        this.onValueChangeFromCoin = this.onValueChangeFromCoin.bind(this);
        this.onValueChangeToCoin = this.onValueChangeToCoin.bind(this);
    }

    check_convert_status = () => {
        if (this.props.check_convert_status === true) {
            this.setState({
                coinAmount: ''
            });
        }
    }

    onValueChangeFromCoin(event) {
        this.setState({
            selectedDefaultCoin: event
        });
        this.props.capture(false);
        this.props.estimateCoin(this.state.toCoin, event);
    }

    renderVAlueCoinFrom() {
        if (this.props.wallet_flag) {
            return this.props.wallet_code
        }
        else {
            return this.state.selectedDefaultCoin
        }

    }

    onValueChangeToCoin(e) {
        this.setState({
            toCoin: e
        })
        this.props.estimateCoin(e, this.state.selectedDefaultCoin);
    }

    componentWillUnmount() {
        this.props.deleteThem();
    }

    render() {
        const message = this.props.payload
        const width = Dimensions.get('window').width
        const height = Dimensions.get('window').height
        const name = this.props.belowTilte
        const icon_image = this.props.customImage;
        const { wallets } = this.props.walletList;
        const estimate_data = this.props.estimateVAlues;
        const estimate_value = this.props.estimatevalue;
        let code = this.renderVAlueCoinFrom();
        return (
            <React.Fragment>
                <View style={styles.hmContainer}>
                    {this.state.isloading === true ?
                        <View style={{ position: 'absolute', right: 0, left: 0, top: 0, bottom: 0, backgroundColor: '#fff', opacity: .8, height: height, zIndex: 999, justifyContent: 'center', alignItems: 'center' }}><ActivityIndicator size="large" color="#0F44A0" /></View> : null}
                    <View style={styles.hmHeader}>
                        <Text style={{ width: 200, position: 'absolute', left: 20, top: 30 }}>{message}</Text>
                    </View>
                    <View style={styles.btmContainer}>
                        <ScrollView>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Text style={{ marginLeft: 5, marginRight: 10, paddingBottom: 10 }}>From:</Text>
                                <View style={styles.txtInputContainer}>
                                    <Picker
                                        style={{
                                            width: Dimensions.get('window').width / 3,
                                            height: 40.59,
                                            borderRadius: 50,
                                            opacity: 0.8,
                                            padding: 10,
                                        }}
                                        selectedValue={code}
                                        onValueChange={this.onValueChangeFromCoin} underlineColorAndroid={'rgba(0,0,0,0)'} >

                                        {wallets.filter((item) => item.code !== this.state.toCoin).map((currency, index) => {
                                            if (currency.length !== 0) {
                                                return (
                                                    <Picker.Item key={currency.address} label={currency.name} value={currency.code} />
                                                )
                                            }
                                        })
                                        }
                                    </Picker>
                                </View>

                                <Text style={{ marginLeft: 5, marginRight: 10, paddingBottom: 10 }}>To:</Text>
                                <View style={styles.txtInputContainer}>
                                    <Picker
                                        style={{
                                            width: 170, width: Dimensions.get('window').width / 3,
                                            height: 45.59,
                                            borderRadius: 50,
                                            opacity: 0.8,
                                            padding: 10,
                                        }} underlineColorAndroid={'rgba(0,0,0,0)'}
                                        selectedValue={this.state.toCoin}
                                        onValueChange={this.onValueChangeToCoin}>
                                        <Picker.Item key='01' label={'Select Curreny'} value="" />
                                        {wallets.filter((item) => item.code !== code).map((currency, index) => {
                                            if (currency.length !== 0) {
                                                return (
                                                    <Picker.Item key={currency.address} label={currency.name} value={currency.code} />
                                                )
                                            }
                                        })
                                        }
                                    </Picker>
                                </View>
                            </View>
                            <View style={{ alignItems: 'center', flexDirection: 'row' }}>
                                <Text style={{ marginLeft: 5, marginRight: 10, paddingBottom: 10 }}>Amount:</Text>
                                <View style={styles.txtInputContainer}>
                                    <TextInput style={styles.txtInput2} placeholder='0.23' onChangeText={(coinAmount) => this.setState({ coinAmount: coinAmount })} underlineColorAndroid={'rgba(0,0,0,0)'} keyboardType='numeric' value={this.state.coinAmount} />
                                </View>
                            </View>
                            {/* conversion  statistics*/}
                            <View style={styles.boxOutr}>
                                <Text style={{ fontSize: 18, color: '#333333', fontWeight: '300', textTransform: "uppercase", textAlign: 'center', marginBottom: 10, marginTop:3}}>
                                    {"Conversion  Statistics"}
                                </Text>

                                <Text style={styles.conversionBoxText}>
                                    Miner Fee : {estimate_data.data ? estimate_data.data.minerFee:''}
                                </Text>
                                <Text style={styles.conversionBoxText}>
                                    Limit : {estimate_data.data ? estimate_data.data.limit : ''}
                                </Text>

                                <Text style={styles.conversionBoxText}>
                                    Minimum Limit: {estimate_data.data ? estimate_data.data.minimum : ''}
                                </Text>
                                <Text style={styles.conversionBoxText}>
                                    Max Limit:{estimate_data.data ? estimate_data.data.maxLimit : ''}
                                </Text>
                                <Text style={styles.conversionBoxText}>
                                    Estimate Value:{estimate_value ? estimate_value.toFixed(8):''}
                                </Text>
                            </View>

                            <View style={{ margin: 0}}>

                                <View style={{ width: Dimensions.get('window').width, flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center' }}>
                                    <View>
                                        <View style={{ marginTop: 10, paddingBottom: 5 }}>
                                            <TouchableOpacity style={styles.btnSend} onPress={() => { this.props.custom_rate_calculate(this.state.coinAmount, this.state.toCoin, code) }}>
                                                <Text style={styles.btnSendText}>Estimate</Text>
                                            </TouchableOpacity>
                                        </View>

                                    </View>

                                    <View>
                                        <View style={{ marginTop: 10, paddingBottom: 5 }}>
                                            <TouchableOpacity style={styles.btnSend}
                                                onPress={() => { this.props.convertUsercoin(this.state.coinAmount, this.state.toCoin, code) }}

                                            >
                                                <Text style={styles.btnSendText}>Convert</Text>
                                            </TouchableOpacity>
                                        </View>

                                    </View>
                                </View>
                            </View>


                        </ScrollView>
                    </View>

                </View>
            </React.Fragment>
        )
    }
}



const styles = StyleSheet.create({
    hmContainer: {
        backgroundColor: '#fff',
        height: Dimensions.get('window').height,
        width: Dimensions.get('window').width
    },
    hmHeader: {
        paddingTop: 20,
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        width: '100%'
    },
    btmContainer: {
        paddingTop: 20,
        justifyContent: 'space-around',
        height: '100%'
    },
    txtInputContainer: {
        marginBottom: 15,
        alignItems: 'flex-start',
        borderRadius: 50,
        backgroundColor: '#EDEDED',
        paddingLeft:15
    },
    txtInput: {
        width: Dimensions.get('window').width / 3,
        height: 45.59,
        borderRadius: 50,
        backgroundColor: '#EDEDED',
        opacity: 0.8,
        padding: 10,
        fontSize: 16,
    },
    txtInput2: {
        width: Dimensions.get('window').width / 1.5,
        height: 45.59,
        borderRadius: 50,
        backgroundColor: '#EDEDED',
        opacity: 0.8,
        padding: 10,
        fontSize: 16,
    },
    btnSendContainer: {
        alignItems: 'center',
        margin: 20,
        width: Dimensions.get('window').width/1.5
    },
    btnSend: {
        width: 150,
        marginBottom: 10.1,
        shadowColor: '#0F44A0',
        shadowOffset: { width: 2, height: 2 },
        shadowOpacity: .8,
        backgroundColor: '#0F44A0',
        height: 52.94,
        margin: 13,
        borderRadius: 50,
        justifyContent: 'center',
        alignItems: 'center'
    },
    btnSendText: {
        color: 'white',
        fontSize: 18
    },
    boxOutr: {
        backgroundColor: '#ffffff',
        borderRadius: 30,
        width: Dimensions.get('window').width / 1.1,
        justifyContent: 'center',
        borderWidth: 4,
        borderColor: '#0F44A0',
        margin: 15,
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 20,
        paddingRight: 20
    },
    conversionBoxText: {
        fontSize: 18, color: '#0F44A0', fontWeight: '300', textTransform: "uppercase",marginRight:5
    }
})