import { createStackNavigator } from 'react-navigation';

import ActivateAccount from './containers/activateAccountContainer';
import CreateAccount  from './containers/createAccountContainer';
import ResendCode  from './containers/resendCodeContainer';
import CreateAddress from './containers/Wallet/createAddressContainer';
import ProcessComplete from './containers/processCompleteContainer'
import Login from './containers/loginContainer';
import Dashboard from './containers/dashboardContainer';
import BitcoinWallet from './containers/bitcoinWalletContainer';
import EthereumWallet from './containers/ethereumWalletContainer'
import AccountInfo from './containers/accountInfoContainer';
import ResetPass from './containers/resetPassContainer';

import Legal from './components/Common/Legal';
import Website from './components/Common/Website';
import Support from './components/Common/Support';
import JavvyUpdates from './components/Common/JavvyUpdates';

import Scanner from './containers/Wallet/scannerContainer';
import Onboarding from './containers/Onboarding';
import CountryKyc from './containers/countryContainer';
import OnboardingSelfie from './containers/Onboarding/selfie';
import OnboardingSelfieCapture from './containers/Onboarding/selfieCapture';
import FrontKyc from './containers/frontkycInfoContainer';
import BackKyc from './containers/backinfoContainer';
import UtilityKyc from './containers/utilityKycInfoContainer';

//user wallet

import UserWallet from './containers/userWalletContainer';

import Coin from './containers/CoinContainer';

const fade = (props) => {
    const {position, scene} = props

    const index = scene.index

    const translateX = 0
    const translateY = 0

    const opacity = position.interpolate({
        inputRange: [index - 0.7, index, index + 0.7],
        outputRange: [0.3, 1, 0.3]
    })

    return {
        opacity,
        transform: [{translateX}, {translateY}]
    }
}

export default App = createStackNavigator({
    Login: {screen: Login},
    ResetPass: {screen: ResetPass},
    CreateAccount: {screen: CreateAccount},
    ActivateAccount: {screen: ActivateAccount},
    ResendCode: {screen: ResendCode},
    CreateAddress: {screen: CreateAddress},
    ProcessComplete: {screen: ProcessComplete},
    Dashboard: {screen: Dashboard},
  //  BitcoinWallet: {screen: BitcoinWallet},
    EthereumWallet: {screen: EthereumWallet},
    Legal: {screen: Legal},
    Website: {screen: Website},
    Support: {screen: Support},
    JavvyUpdates: {screen: JavvyUpdates},
    AccountInfo: {screen: AccountInfo},
    Scanner: {screen: Scanner},
    Onboarding: {screen: Onboarding},
    OnboardingSelfieCapture: {screen: OnboardingSelfieCapture},
    Coin: { screen: Coin },
    CountryKyc: { screen: CountryKyc },
    FrontKyc: { screen: FrontKyc },
    BackKyc: { screen: BackKyc },
    UtilityKyc: { screen: UtilityKyc },
    UserWallet: { screen: UserWallet}
}, {
    transitionConfig: () => ({
        screenInterpolator: (props) => {
            return fade(props)
        }
    })
})
