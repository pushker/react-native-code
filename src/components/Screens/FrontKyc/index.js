import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Dimensions,
    Alert,
    Image,
    TouchableOpacity,
    ActivityIndicator
} from 'react-native';


import { Header } from '../../common/Header'
import { RNCamera } from 'react-native-camera';
import { token, baseUrlLive } from '../../../api';


export default class FrontKyc extends Component {

    constructor(props) {
        super(props);
        this.state = {
            backgroundColor: '#2756A9',
            path: null,
            imagePath: null,
            isLoading: false
        }
        console.log(token, 'and', baseUrlLive);

    }

    static navigationOptions = {
        header: null
    };

    componentDidMount() {

    }

    takePicture = async function () {
        if (this.camera) {
            this.setState({ isLoading: true });
            const options = { quality: 0.5, base64: true };
            const data = await this.camera.takePictureAsync(options);
            this.setState({
                path: data.base64,
                imagePath: data.uri,
                isLoading: false
            });
        }
    };


    uploadFrontId() {
        const user = {
            frontId: 'data:image/jpg;base64,' + this.state.path,
            jwt: 'JWT ' + this.props.user.loginData.user.jwtToken,
        }

        this.setState({ isLoading: true });
        this.props.navigation.navigate('BackKyc');

        fetch(`${baseUrlLive}user/kyc-wheat?token=${token}`, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: user.jwt
            },
            body: JSON.stringify({
                'front_id_image': user.frontId,
                'state_code': this.props.user.countryStateKycData.state,
                'country_code': this.props.user.countryStateKycData.country
            })
        })
            .then(res => res.json())
            .then((data) => {
                if (data.verified_status === 1) {
                    this.props.saveFrontId(data);
                    Alert.alert(data.message);
                    this.setState({ isLoading: false });
                    this.props.navigation.navigate('BackKyc');
                } else {

                    Alert.alert(data.message);
                    this.props.saveFrontId(data);
                    this.setState({
                        isLoading: false, path: null,
                        imagePath: null
                    });
                   // this.props.navigation.navigate('BackKyc');

                }

            })
            .catch(err => {
                console.log(err)
            })
    }

    renderDocuments() {
        if (this.props.user.kycDocumentsData && this.props.user.kycDocumentsData.data !== '' && this.props.user.kycDocumentsData.data !== null && this.props.user.kycDocumentsData.data !==undefined) {
            return (
                    <Text style={{ fontSize: 16, color: '#fff', marginTop: 10, paddingLeft: 20, paddingRight: 20 }}>{this.props.user.kycDocumentsData.data}</Text>
            )
        }
    }


    renderCamera() {
        return (
            <ScrollView >
                <View>
                    <RNCamera
                        ref={ref => {
                            this.camera = ref;
                        }}
                        style={styles.preview}
                        type={RNCamera.Constants.Type.back}
                        flashMode={RNCamera.Constants.FlashMode.on}
                        permissionDialogTitle={'Permission to use camera'}
                        permissionDialogMessage={'We need your permission to use your camera phone'}
                    >

                    </RNCamera>
                </View>

                    <Text style={{ fontSize: 24, color: '#fff', paddingLeft: 10, paddingBottom: 8 }}>
                        Front of your ID Please?
                        </Text>

                    <Text style={{ fontSize: 16, color: '#fff', paddingLeft: 20, paddingRight: 20 }}>
                       Now we need a picture of your ID to match it to your face.
                       Fraudulent tomfoolery is not acceptable.
                       For this part, only the front of your ID please.
                       It’s best if you place it on a flat surface with adequate
                       light and all four corners visible. Press the capture button.

                        </Text>

                {this.renderDocuments()}

                    <View style={styles.button}>
                        <Text style={styles.textButton} onPress={this.takePicture.bind(this)}>
                            Capture
                        </Text>
                    </View>

            </ScrollView>
        );
    }

    renderImage() {
        return (
            <View>
                <Image
                    source={{ uri: this.state.imagePath }}
                    style={styles.preview}
                />
                <Text style={{ fontSize: 23, color: '#fff', paddingLeft: 10, paddingBottom: 8 }}>
                    Let's Upload a Front of your ID
                </Text>
                <View style={{ width: Dimensions.get('window').width, flexDirection: 'row', justifyContent: 'space-around' }}>
                    <View style={styles.buttonStyle}>
                        <Text style={styles.textButton} onPress={() => this.setState({ imagePath: null })}>
                            Cancel
                        </Text>
                    </View>
                    <View style={styles.buttonStyle}>
                        <Text style={styles.textButton} onPress={this.uploadFrontId.bind(this)}>
                            Upload
                        </Text>
                    </View>
                </View>
            </View>
        );
    }

    render() {

        const { navigate } = this.props.navigation;
        const width = Dimensions.get('window').width;
        const height = Dimensions.get('window').height;
        return (
            <React.Fragment>
                <Header headerText={'Javvy Verification - KYC'} />

                    <View style={{
                        flex: 1,
                        backgroundColor: this.state.backgroundColor
                    }}>
                    <View style={styles.container}>
                        {this.state.isLoading === true ?
                            <View style={{ position: 'absolute', right: 0, left: 0, top: 0, bottom: 0, backgroundColor: '#fff', opacity: .8, height: height, zIndex: 999, justifyContent: 'center', alignItems: 'center' }}><ActivityIndicator size="large" color="#0F44A0" /></View> : null}
                        {this.state.imagePath ? this.renderImage() : this.renderCamera()}
                        </View>
                    </View>

            </React.Fragment>
        )
    }
}


const styles = StyleSheet.create({
    // Slide styles
    slide: {
        flex: 1,   // Take up all screen
        backgroundColor: '#2756A9'
    },
    // Header styles
    header: {
        color: '#FFFFFF',
        fontFamily: 'Avenir',
        fontSize: 30,
        fontWeight: 'bold',
        marginVertical: 15,
    },
    // Text below header
    text: {
        color: '#FFFFFF',
        fontFamily: 'Avenir',
        fontSize: 18,
        marginHorizontal: 40,
        textAlign: 'center',
    },
    textButton: {
        color: '#2756A9',
        fontWeight: '600',
        fontSize: 18
    },
    preview: {
        height: Dimensions.get('window').height / 2,
        justifyContent: 'center',
        alignItems: 'center'
    },
    capture: {
        backgroundColor: 'white',
        borderRadius: 5,
        padding: 10,
        paddingHorizontal: 20,
        margin: 10,
        position: 'absolute',
        top: Dimensions.get('window').width / 1.84,
        alignSelf: 'flex-start',
    },
    buttonStyle: {
        backgroundColor: 'white',
        borderRadius: 5,
        padding: 9,
        paddingHorizontal: 20,
        marginTop: 50,
        alignItems: 'flex-start',
        alignSelf: 'flex-start',
    },
    button: {
        backgroundColor: 'white',
        borderRadius: 5,
        padding: 9,
        paddingHorizontal: 20,
        marginTop: 10,
        marginBottom:15,
        marginLeft: 250,
        alignItems: 'flex-start',
        alignSelf: 'flex-start',
    }
});