import { connect } from 'react-redux';
import  BitcoinWallet from '../components/Screens/Wallet/BitcoinWallet/';

import { createBitcoinTransaction, bitcoinDetail, convertBitcoin, bitcoinBalance } from '../actions/walletAction';

const mapStateToProps = (state) => {
    return {
        user: state.user,
        wallet: state.wallet
    }
}

const mapDispatchToProps = ( dispatch ) => {
    return {
        createBitcoinTransaction: (data) => {
            dispatch(createBitcoinTransaction(data))
        },
        bitcoinDetail: (data) => {
            dispatch(bitcoinDetail(data))
        },
        convertBitcoin: (data) => {
            dispatch(convertBitcoin(data))
        },
        bitcoinBalance: (data) => {
            dispatch(bitcoinBalance(data))
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(BitcoinWallet);