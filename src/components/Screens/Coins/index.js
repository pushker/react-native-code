import React, {Component} from 'react';
import {Fragment} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome'
import {
    ScrollView,
    Platform,
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    TouchableOpacity,
    TouchableWithoutFeedback,
    Dimensions,
    ActivityIndicator,
    StatusBar,
    Alert
} from 'react-native';

import {Header} from '../../common/Header';

import CryptoMenu from '../../common/CryptoMenu';
import AltMenu from '../../common/AltMenu';

import Drawer from 'react-native-drawer';

import SortableGrid from 'react-native-sortable-grid'


export default class BitcoinWallet extends Component {
    constructor(props) {
        super(props);
        this.state = {
            leftModalOpen: false,
            rightModalOpen: false,
            coins: [],
            coins_constant: [],
            favcoins: [],
            isLoading: true
        }
        this.openCryptoMenu = this.openCryptoMenu.bind(this);
        this.openAltMenu = this.openAltMenu.bind(this);
        this.convertBitcoin = this.convertBitcoin.bind(this);
        this.releaseDraggedItem = this.releaseDraggedItem.bind(this);
        this.postfavCoin = this.postfavCoin.bind(this);
        // this.showIcon = this.showIcon.bind(this);
    }


    static navigationOptions = {
        header: null,
        gesturesEnabled: false,
    }

    componentDidMount() {
        // const getCoinsDetail = {
        //     jwt: 'JWT ' + this.props.user.loginData.user.jwtToken
        // };

        // this.props.getCoinsDetail(getCoinsDetail)
        // this.props.getFavCoinsDetail(getCoinsDetail)

        // add showIcon parameter
        let temp_favicon_minus = [...this.props.wallet.favCoinsData.coins]


        for (let i=0; i<temp_favicon_minus.length; i++) {
            temp_favicon_minus[i].iconShow = false
        }

        this.setState({favcoins: temp_favicon_minus}, () => {
            let temp = this.props.wallet.coinsData.tokens
            let with_mob_image = []

            // get objects with mob_image and store in new array i.e. with_mob_image
            for (let i=0; i<temp.length; i++) {
                if (temp[i].mob_image) {
                    with_mob_image.push(temp[i])
                }
            }

            // add parameter for icon show or hide
            for (let i=0; i<with_mob_image.length;i++) {
                with_mob_image[i].iconShow = false

                // example.slice(0, 10)
                with_mob_image[i].name = with_mob_image[i].name.slice(0, 12)
            }


            // Remove favorite icons from all icons
            let with_mob_image_sorted = []
            for (let i=0;i<with_mob_image.length;i++) {
                let found = false;
                for (let j=0; j<this.state.favcoins.length; j++) {
                    if (with_mob_image[i].code == this.state.favcoins[j].code) {
                        found = true
                    }
                }
                if (found) {

                } else {
                    with_mob_image_sorted.push(with_mob_image[i])
                }
            }

            // set state with all mobile image (!SVG)
            this.setState({coins: with_mob_image_sorted}, () => {
                // callback
                this.setState({isLoading: false})
                let coins_temp = [...with_mob_image_sorted]
                this.setState({coins_constant: coins_temp})
            })
        })

    }

    openCryptoMenu() {
        this.setState({leftModalOpen: true});
        this._leftDrawer.open()
    }


    openAltMenu() {
        this.setState({rightModalOpen: true});
        this._rightDrawer.open()
    }


    convertBitcoin(coinAmount) {
        const convertBitcoinObject = {
            amount: coinAmount,
            fromCoin: 'BTC',
            toCoin: 'ETH',
            jwt: 'JWT ' + this.props.user.loginData.user.jwtToken
        }

        this.props.convertBitcoin(convertBitcoinObject)
        setTimeout(() => {
            Alert.alert(this.props.wallet.bitcoinConversionData.message)
        }, 2000);
    }


    sendBitcoin(coinAmount, sendAddress) {
        const sendBitcoinObject = {
            value: coinAmount,
            to: sendAddress,
            account_name: this.props.user.loginData.user.account_name,
            jwt: 'JWT ' + this.props.user.loginData.user.jwtToken
        }

        this.props.createBitcoinTransaction(sendBitcoinObject)
        setTimeout(() => {
            Alert.alert(this.props.wallet.bitcoinTransactionDetails.message)
        }, 2000);
    }

    addIcons = (index) => {

        if (this.state.favcoins.length >= 4) {
            Alert.alert(
                'Error',
                'You Cannot add more coins in your favorite tab',
                [
                  {text: 'Ok', onPress: () => console.log('Cancel Pressed'), style: 'cancel'}
                ],
                { cancelable: false }
              )
            return
        }

        // duplicate entries

        let d_temp = [...this.state.favcoins];
        let d_temp_all = [...this.state.coins];

         for (let i=0; i < d_temp.length; i++) {
             if (d_temp[i].code == d_temp_all[index].code) {
                Alert.alert(
                    'Duplicate Entry',
                    'You Cannot add same coins twice',
                    [
                      {text: 'Ok', onPress: () => console.log('Cancel Pressed'), style: 'cancel'}
                    ],
                    { cancelable: false }
                  )
                return
             }
         }


        // duplicate entries ends


        this.setState({searchText: ''})


        // remove plus icon starts
        let temp_all_coins = [...this.state.coins]

        for (let i=0; i<temp_all_coins.length;i++) {
            temp_all_coins[i].iconShow = false
        }

        this.setState({coins: temp_all_coins})
         // remove plus icon ends


         // without alert

        let temp = [...this.state.favcoins];
        let temp_all = [...this.state.coins];

        temp.push(temp_all[index])

        let final = []

        for (let i=0; i<temp.length; i++) {
            final.push({
                code: temp[i].code,
                coin_order: i + 1
            })
        }

        this.postfavCoin(final)
        this.setState({isLoading: true})

         // without alert ends

        // with alert

        /*
        Alert.alert(
            'Add Confirmation',
            'Add currency to your favorite coin',
            [
              {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
              {text: 'OK', onPress: () => {
                  let temp = [...this.state.favcoins];
                  let temp_all = [...this.state.coins];

                  temp.push(temp_all[index])
                  console.log(temp)

                  let final = []

                  for (let i=0; i<temp.length; i++) {
                      final.push({
                        code: temp[i].code,
                        coin_order: i + 1
                      })
                  }

                  this.postfavCoin(final)
                  this.setState({isLoading: true})

              }},
            ],
            { cancelable: false }
          )
            */

        // with alert ends

    }

    removeIcons = (index) => {


        // remove plus icon starts
        let temp_all_coins = [...this.state.coins]

        for (let i=0; i<temp_all_coins.length;i++) {
            temp_all_coins[i].iconShow = false
        }

        this.setState({coins: temp_all_coins})
         // remove plus icon ends


        this.setState({searchText: ''})

        // without alert

        let temp = [...this.state.favcoins];

        temp.splice(index, 1)

        let final = []

        for (let i=0; i<temp.length; i++) {
            final.push({
            code: temp[i].code,
            coin_order: i + 1
            })
        }

        this.postfavCoin(final)

        this.setState({isLoading: true})

        setTimeout(() => {
            this.setState({favcoins: []})
        }, 1000)

        // without alert ends

        // with alert

        /*
        Alert.alert(
            'Remove Confirmation',
            'Remove currency to your favorite coin',
            [
              {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
              {text: 'OK', onPress: () => {
                  let temp = [...this.state.favcoins];

                  temp.splice(index, 1)

                  let final = []

                  for (let i=0; i<temp.length; i++) {
                      final.push({
                        code: temp[i].code,
                        coin_order: i + 1
                      })
                  }

                  this.postfavCoin(final)
                  this.setState({isLoading: true})

              }},
            ],
            { cancelable: false }
          )

          */
          // with alert ends
    }


    postfavCoin(coin_data){
        // this.setState({isLoading: true})
        const postCoinsDetail = {
            jwt: 'JWT ' + this.props.user.loginData.user.jwtToken,
            coin : coin_data
        };

        this.props.postFavCoins(postCoinsDetail)

        setTimeout(() => {
            const getCoinsDetail = {
                jwt: 'JWT ' + this.props.user.loginData.user.jwtToken
            };
            this.props.getFavCoinsDetail(getCoinsDetail)
            setTimeout(() => {
                // set state for favorite coin images
                this.setState({favcoins: this.props.wallet.favCoinsData.coins}, () => {
                    setTimeout(() => {
                        this.setState({isLoading: false})
                    }, 1000)
                    // this.setState({isLoading: false})

                    // Remove favorite icons from all icons
                    let with_mob_image_sorted = []
                    for (let i=0;i<this.state.coins_constant.length;i++) {
                        let found = false;
                        for (let j=0; j<this.state.favcoins.length; j++) {
                            if (this.state.coins_constant[i].code == this.state.favcoins[j].code) {
                                found = true
                            }
                        }
                        if (found) {

                        } else {
                            with_mob_image_sorted.push(this.state.coins_constant[i])
                        }
                    }

                    this.setState({coins: with_mob_image_sorted})

                })
                }, 3000)
        }, 3000)
    }

    releaseDraggedItem = (event) => {

        let coin_find_index = []
        for (let i=0; i<this.state.favcoins.length;i++) {
            coin_find_index.push(event.itemOrder.findIndex(v => v.key == i))
        }

        // [{"code":"BTC","coin_order":2},{"code":"ETH","coin_order":1},{"code":"JVY","coin_order":3},{"code":"OMG","coin_order":4}]
        let final = []

        for (let j=0; j<this.state.favcoins.length; j++) {
            let temp_child = {
                code : this.state.favcoins[j].code,
                coin_order: coin_find_index[j]
            }
            final.push(temp_child)
        }

        this.postfavCoin(final)
        this.setState({isLoading: true})
    }

    // Search coins from all coins
    searchCoin = (event) => {

        // remove plus icon
        let temp_all_coins = [...this.state.coins]

        let with_mob_image_sorted = []
            for (let i=0;i<temp_all_coins.length;i++) {
                let found = false;
                for (let j=0; j<this.state.favcoins.length; j++) {
                    if (temp_all_coins[i].code == this.state.favcoins[j].code) {
                        found = true
                    }
                }
                if (found) {

                } else {
                    with_mob_image_sorted.push(temp_all_coins[i])
                }
            }

        temp_all_coins = [...with_mob_image_sorted]



        for (let i=0; i<temp_all_coins.length;i++) {
            temp_all_coins[i].iconShow = false
        }

        this.setState({coins: temp_all_coins})

        const searchText = event.nativeEvent.text
        this.setState({searchText: searchText})
        if (searchText == '') {
            let all_coins = [...this.state.coins_constant]
            this.setState({coins: all_coins})
        } else {
            let all_coins = [...this.state.coins_constant]
            let searched_all_coins = []

            for (let i=0; i< all_coins.length;i++) {
                if (all_coins[i].name.toLowerCase().includes(searchText.toLowerCase())) {
                    searched_all_coins.push(all_coins[i])
                }
            }
            this.setState({coins: searched_all_coins})
        }

    }


    // show plus icon
    showIcon = (index) => {
        let temp_all_coins = [...this.state.coins]

        for (let i=0; i<temp_all_coins.length;i++) {
            if (index == i) {
                temp_all_coins[i].iconShow = true
            } else {
                temp_all_coins[i].iconShow = false
            }
        }

        this.setState({coins: temp_all_coins})
    }

    showFavIcon = (index) => {


        let temp_fav_coins = [...this.state.favcoins]

        for (let i=0; i<temp_fav_coins.length;i++) {
            if (index == i) {
                temp_fav_coins[i].iconShow = true
            } else {
                temp_fav_coins[i].iconShow = false
            }
        }

        this.setState({favcoins: temp_fav_coins})
    }
    closeCryptoMenu = (value) => {
        this.setState({ leftModalOpen: value })
        this._leftDrawer.close()
    }



    render() {
        const loadModal = this.state.modalReady === true
        const width = Dimensions.get('window').width
        const height = Dimensions.get('window').height

        const drawerStylesLeft = {
            drawer: {
                backgroundColor: 'white',
                borderTopRightRadius: 30,
                borderBottomRightRadius: 30,
                maxHeight: height - 100,
                borderRightWidth: 1,
                borderBottomWidth: 1,
                borderTopWidth: 1,
                borderColor: 'lightgrey'
            }
        }
        const drawerStylesRight = {
            drawer: {
                backgroundColor: 'white',
                borderTopLeftRadius: 30,
                borderBottomLeftRadius: 30,
                maxHeight: height - 300,
                borderLeftWidth: 1,
                borderBottomWidth: 1,
                borderTopWidth: 1,
                borderColor: 'lightgrey'
            }
        }

        const { goBack } = this.props.navigation;

        return (

            <React.Fragment>

                <StatusBar barStyle="light-content"/>
                { this.state.isLoading === true ?
                <View style={{position: 'absolute', right: 0, left: 0, top: 0, bottom: 0, backgroundColor: '#fff', opacity: .8, height: height, zIndex: 999, justifyContent: 'center', alignItems: 'center'}}><ActivityIndicator size="large" color="#0F44A0" /></View> : null }

                <Header headerText={'Coins'}
                        leftComponent={'chevron-left'}
                        leftOnPress={() => goBack(null)}
                        rightComponent={'more-horizontal'}
                        rightOnPress={this.openAltMenu}/>

                <Drawer content={<CryptoMenu closeModal={this.closeCryptoMenu} navigate={this.props.navigation}/>} tapToClose={true} open={false}
                        type={'overlay'} styles={drawerStylesLeft} side={'left'}
                        openDrawerOffset={(viewport) => viewport.width - 150} ref={(ref) => this._leftDrawer = ref}>
                    <Drawer content={<AltMenu navigate={this.props.navigation}/>} tapToClose={true} open={false}
                            type={'overlay'} styles={drawerStylesRight} side={'right'}
                            openDrawerOffset={(viewport) => viewport.width - 150}
                            ref={(ref) => this._rightDrawer = ref}>



                        <View style={{alignItems: 'center', marginTop: 5, backgroundColor: '#ffffff'}}>

                            <View style={{
                                borderBottomColor: 'grey',
                                borderBottomWidth: 0.5,
                                width: '95%'
                            }}>
                                <Text style={{color: '#000000'}}>Quick Access</Text>
                            </View>

                            <View style={styles.favIconBox}>

                                <SortableGrid onDragRelease={this.releaseDraggedItem}>
                                    {this.state.favcoins.map((coin, index) => {
                                        return (
                                                    <View key={index} style={{justifyContent: 'center', alignItems: 'center', marginTop: 10, marginBottom: 15}}>
                                                        <Image  source={{uri:coin.mob_image}} style={{width: 70, height: 70, marginHorizontal: 20}}/>
                                                        <Text style={{alignItems: 'center', justifyContent: 'center', marginBottom: 10, color: '#000000'}}>{coin.name}</Text>
                                                        <Icon size={20} name="minus-circle" style={{position:'absolute', top: 0, right: 4}} color="#ff6161" onPress={this.removeIcons.bind(this, index)}/>
                                                        {/* <Text onPress={this.removeIcons.bind(this, index)} style={{color: 'red', fontWeight: 'bold'}}>x</Text> */}
                                                        {/* <Icon name="remove" size={15} onPress={this.removeIcons.bind(this, index)} color="#459DFF"/> */}
                                                        {/* <Icon size={20} name="plus" style={{position:'absolute', top: 15, right: 5}}/> */}
                                                    </View>

                                        )
                                    })}
                                    </SortableGrid>

                                {/* <View style={{flex: 1, flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'center', alignItems: 'center', marginTop: 15}}>

                                </View> */}

                            </View>

                            <View style={{
                                borderBottomColor: 'grey',
                                borderBottomWidth: 1,
                                width: '95%'
                            }}>
                                <Text style={{color: '#000000'}}>Search Coins</Text>
                            </View>

                            <TextInput placeholder="Search" style={{width: "95%", borderWidth: 0.5, marginTop: 10, paddingHorizontal: 10, paddingVertical: 5, borderRadius: 3, marginBottom: 20}} onChange={this.searchCoin} value={this.state.searchText}/>


                            <View style={{
                                borderBottomColor: 'grey',
                                borderBottomWidth: 1,
                                width: '95%'
                            }}>
                                <Text style={{color: '#000000'}}>Available Coins</Text>
                            </View>

                            <ScrollView style={styles.allIconBox}>
                                <View style={{flex: 1, flexDirection: 'row', flexWrap: 'wrap', marginVertical: 10}}>
                                    {this.state.coins.map((coin, index) => {
                                        return (
                                                    // <TouchableWithoutFeedback  activeOpacity={5} key={index} >
                                                        <View style={{alignItems: 'center', justifyContent: 'center'}} key={index}>
                                                            <TouchableOpacity onPress={this.showIcon.bind(this, index)}>
                                                                <Image  source={{uri:coin.mob_image}} style={{width: 70, height: 70, marginHorizontal: 10,  position: 'relative'}}  />
                                                            </TouchableOpacity>

                                                            <Text style={{alignItems: 'center', justifyContent: 'center', marginBottom: 10, color: '#000000'}}>{coin.name}</Text>

                                                            {coin.iconShow ? <Icon size={20} name="plus-circle" style={{position:'absolute', top: 5, right: 2}} color="#6ce06c" onPress={this.addIcons.bind(this, index)}/> : null}


                                                        </View>
                                                    // </TouchableWithoutFeedback>
                                        )
                                    })}
                                </View>
                            </ScrollView>
                        </View>

                    </Drawer>
                </Drawer>
            </React.Fragment>
        )
    }
}


const styles = StyleSheet.create({
    favIconBox: {
        borderWidth: 0.5,
        backgroundColor: '#ffffff',
        borderRadius: 3,
        marginTop: 15,
        marginBottom: 15,
        width: '95%',
        minHeight: 110,
        paddingVertical: 5
    },
    allIconBox: {
        borderWidth: 0.5,
        backgroundColor: '#ffffff',
        borderRadius: 3,
        padding: 5,
        marginTop: 15,
        marginBottom: 115,
        width: '95%',
        height: 300,
        paddingLeft: 12
    }
});