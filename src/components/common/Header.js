import React from 'react';
import { Text, View, Image, TouchableOpacity, Dimensions } from 'react-native';

import Icon from 'react-native-vector-icons/Feather';

let window = Dimensions.get('window')


export const Header = ({ headerText, leftButtonText, leftOnPress, rightButtonText, rightOnPress, rightComponent, leftComponent, headerImage, leftImage, rightImage}) => {
    const {
        textStyle,
        viewStyle,
        buttonStyleLeft,
        buttonStyleRight,
        buttonTextStyle,
    } = styles;

    return (
        <View style={viewStyle}>
            <View style={buttonStyleLeft}>
                <TouchableOpacity onPress={leftOnPress}>
                {leftImage}
                    <Text style={buttonTextStyle}>
                        {leftButtonText}
                        <Icon name={leftComponent} color='#fff' size={30}></Icon>
                    </Text>
                </TouchableOpacity>
            </View>
            {headerImage}
            <Text style={textStyle}>{headerText}</Text>
            <View style={buttonStyleRight}>
                <TouchableOpacity onPress={rightOnPress}>
                    {rightImage}
                    <Text style={buttonTextStyle}>
                        {rightButtonText}
                        <Icon name={rightComponent} color='#fff' size={30}></Icon>
                    </Text>
                </TouchableOpacity>
            </View>
        </View>
    );
};

const styles = {
    viewStyle: {
        backgroundColor: '#2756A9',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        height: 65,
        paddingTop: 0,
        position: 'relative'
    },
    textStyle: {
        fontSize: 20,
        fontWeight: '300',
        marginTop: 30,
        color: '#fff'
    },
    buttonStyleLeft: {
        position: 'absolute',
        top: 32,
        left: 15
    },
    buttonStyleRight: {
        position: 'absolute',
        top: 38,
        right: 15,
    },
    buttonTextStyle: {
        color: '#007add',
        fontWeight: 'bold'
    }
};