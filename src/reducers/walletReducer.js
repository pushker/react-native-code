import {
    CREATE_ADDRESS,
    USER_WALLET_DETAIL,
    CREATE_BITCOIN_TRANSACTION,
    BITCOIN_DETAIL,
    CREATE_ETHEREUM_TRANSACTION,
    ETHEREUM_DETAIL,
    SHAPESHIFT_EXCHANGE,
    GET_SHAPESHIFT_RATE,
    ETHEREUM_BALANCE,
    BITCOIN_BALANCE,
    SEND_QRCODE,
    ALL_COINS,
    FAV_COINS,
    SAVE_USER_WALLET_CODE_NAME,
    USER_COIN_DETAIL,
    USER_WALLET_BALANCE,
    CREATE_USERCOIN_TRANSACTION,
    CUSTOM_LOADER,
    CUSTOM_FLAG
} from '../actions/types';


const initialState = {
    createAddressData: {},
    userWalletDetail: {},
    bitcoinTransactionDetails: {},
    bitcoinDetail: {},
    bitcoinBalance: {},
    ethereumTransactionDetails: {},
    ethereumDetail: {},
    ethereumBalance: {},
    qrcodeData: '',
    coinsData: {},
    favCoinsData: {},
    userCommonWalletDeatil: {},
    userCoinWalletData: {},
    userBalance: {},
    userTransactionDetails: {},
    loaderData: '',
    customFlag: true,
    userCoinConversionData: {},
    estimateRateData: {}

}

export default wallet = (state = initialState, action) => {
    switch(action.type) {
        case CREATE_ADDRESS:
            return {
                ...state,
                createAddressData: action.payload
            }
        case USER_WALLET_DETAIL:
            return {
                ...state,
                userWalletDetail: action.payload
            }
        case CREATE_BITCOIN_TRANSACTION:
            return {
                ...state,
                bitcoinTransactionDetails: action.payload
            }
        case BITCOIN_DETAIL:
            return {
                ...state,
                bitcoinDetail: action.payload
            }
        case BITCOIN_BALANCE: {
            return {
                ...state,
                bitcoinBalance: action.payload
            }
        }
        case CREATE_ETHEREUM_TRANSACTION:
            return {
                ...state,
                ethereumTransactionDetails: action.payload
        }
        case ETHEREUM_DETAIL:
            return {
                ...state,
                ethereumDetail: action.payload
            }
        case ETHEREUM_BALANCE: {
            return {
                ...state,
                ethereumBalance: action.payload
            }
        }
        case SEND_QRCODE: {
            return {
                ...state,
                qrcodeData: action.payload
            }
        }
        case ALL_COINS: {
            return {
                ...state,
                coinsData: action.payload
            }
        }
        case FAV_COINS: {
            return {
                ...state,
                favCoinsData: action.payload
            }
        }
        case SAVE_USER_WALLET_CODE_NAME:
            return {
                ...state,
                userCommonWalletDeatil:action.payload
            }
        case USER_COIN_DETAIL:
            return {
                ...state,
                userCoinWalletData:action.payload
            }
        case USER_WALLET_BALANCE:
            return {
                ...state,
                userBalance:action.payload
            }
        case CREATE_USERCOIN_TRANSACTION:
            return {
                ...state,
                userTransactionDetails:action.payload
            }
        case CUSTOM_LOADER:
            return {
                ...state,
                loaderData:action.payload
            }
        case CUSTOM_FLAG:
            return {
                ...state,
                customFlag: action.payload
            }
        case SHAPESHIFT_EXCHANGE: {
            return {
                ...state,
                userCoinConversionData: action.payload
            }
        }
        case GET_SHAPESHIFT_RATE: {
            return {
                ...state,
                estimateRateData:action.payload
            }
        }
        default:
            return state
    }
}