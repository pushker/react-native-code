import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    TextInput,
    TouchableOpacity,
    Dimensions
} from 'react-native';




export default class Transactions extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }

    render() {
        const transactionsList = this.props.transactionsList;
        console.log(transactionsList, 'tranactions');
        return (
            <View style={styles.hmContainer}>
                <View style={styles.hmHeader}>
                    <Text style={{ fontSize: 28, color: '#0F44A0', fontWeight: '300', marginBottom: 10 }}>
                        Transactions
                    </Text>

                </View>
                <ScrollView style={styles.scrollv}>
                    {transactionsList.map((transaction, index) => {
                        if (transactionsList.length !== 0) {
                            return (
                                <View key={index} style={{ marginBottom: 30, paddingBottom: 10, borderBottomWidth: 1, borderBottomColor: 'lightgrey' }}>
                                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginRight: 10 }}>
                                        <View style={{ marginBottom: 15 }}>
                                            <Text style={{ fontSize: 14, opacity: .8, paddingBottom: 5 }}>{transaction.transaction_type}</Text>
                                            <Text>{transaction.value} {transaction.coin_type}</Text>
                                        </View>
                                        <View style={{ marginBottom: 15 }}>
                                            <Text style={{ fontSize: 14, opacity: .8, paddingBottom: 5 }}>On</Text>
                                            <Text>{new Date(transaction.created_at).toLocaleString()}</Text>
                                        </View>
                                    </View>

                                    <View style={{ flexDirection: 'row', justifyContent: 'flex-start' }}>
                                        <View style={{ marginBottom: 15, marginLeft: 1 }}>
                                            {transaction.transaction_type === "Received" ?
                                                <View>
                                                    <Text style={{ fontSize: 14, opacity: .8, paddingBottom: 5 }}>From</Text>
                                                    <Text>{transaction.from}</Text>
                                                </View>
                                                : <View>
                                                    <Text style={{ fontSize: 14, opacity: .8, paddingBottom: 5 }}>To</Text>
                                                    <Text>{transaction.to}</Text>
                                                </View>
                                            }

                                        </View>



                                    </View>
                                </View>
                            )
                        } else {
                            return (
                                <View style={{ alignItems: 'center' }}><Text style={{ fontSize: 14 }}>No Transactions Found</Text></View>

                            )
                        }
                    })
                    }



                    {/* <View style={{alignItems: 'center'}}><Text style={{fontSize: 14}}>No Transactions Found</Text></View> */}


                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    hmContainer: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        paddingBottom: 150,
        height: Dimensions.get('window').height,
        width: Dimensions.get('window').width,
    },
    hmHeader: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    scrollv: {
        padding: 10,
        height: Dimensions.get('window').height,
        width: '100%'
    }
})