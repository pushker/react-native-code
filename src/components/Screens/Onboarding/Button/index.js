import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    TouchableOpacity,
    View
} from 'react-native';

export default class Button extends Component {
    render({ onPress } = this.props) {
        return (
                <View>
                    <TouchableOpacity onPress={onPress}>
                    <View style={styles.button}>
                        <Text style={styles.text}>
                            {this.props.text.toUpperCase()}
                        </Text>
                    </View>
                    </TouchableOpacity>
                </View>
        )
    }
}

const styles = StyleSheet.create({
    // Button container
    button: {
        backgroundColor: 'white',
        borderRadius: 5,
        padding: 10,
        paddingHorizontal: 20,
        margin: 10,
        alignItems: 'flex-start',
        alignSelf: 'flex-start',
    },
    // Button text
    text: {
        color: '#2756A9',
        fontWeight: '600',
        // fontFamily: 'Avenir',
        fontSize: 18
        // backgroundColor: 'white',
        // borderRadius: 5,
        // padding: 10,
        // paddingHorizontal: 20,
        // margin: 10,
        // // alignItems: 'flex-start',
        // alignSelf: 'flex-start',
    },
});