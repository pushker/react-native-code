import { connect } from 'react-redux';
import Onboarding from '../../components/Screens/Onboarding';
import { uploadSelfieImage } from '../../actions/userAction';



const mapStateToProps = ( state ) => {
    return {
        user: state.user,
        wallet: state.wallet
    }
}

const mapDispatchToProps = ( dispatch ) => {
    return {
        uploadSelfieImage: (data) => {
            dispatch(uploadSelfieImage(data))
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Onboarding);