import React, { Component }  from 'react';
import { Text, View, TouchableOpacity, StyleSheet } from 'react-native';


export default class Tabs extends Component {

    constructor(props) {
        super(props);
        this.state = {
            activeTab: this.props.initialTab,
            gestureName: 'none',
            maxLength: this.props.maxLength,
            minLength: 0
        }
    }




    render({ children } = this.props) {
        const config = { velocityThreshold: 0.1, directionOffsetThreshold: 0 };

        return (
            <View style={styles.container}>
                <View style={styles.tabsContainer}>

                    {children.map(({ props: { title } }, index) =>
                    <TouchableOpacity style={styles.tabContainer}
                                    onPress={() => this.setState({ activeTab: index })}
                                    key={index}>
                                    <Text style={[styles.tabText, index === this.state.activeTab ? styles.tabContainerActive : [] ]}>{title}</Text>
                    </TouchableOpacity>

                )}

                </View>
                <View style={styles.contentContainer}>
                    {children[this.state.activeTab]}
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        elevation: 1,
        backgroundColor: '#fff',

    },
    tabsContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 10,
        backgroundColor: '#2756A9',
        borderBottomEndRadius: 33,
        borderBottomStartRadius: 33,
    },
    tabContainer: {
        flex: 1,
        alignItems: 'center',
        paddingVertical: 15,
        paddingRight: 10,
    },
    tabContainerActive: {
        color: '#fff',
        opacity: 1
    },
    tabText: {
        fontSize: 16,
        justifyContent: 'center',
        textAlign: 'center',
        color: '#fff',
        opacity: .5
    }
})