import { token, baseUrlLive } from '../api';

export function FetchPost(url,data,jwtToken) {
    console.log(data);
   return  fetch(`${baseUrlLive}${url}?token=${token}`, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: jwtToken
        },
        body: JSON.stringify(data)
    })
        .then(res => res.json())
       .then((data) => {
           return data;
       })
        .catch(err => {
            console.log(err)
        })
}
