import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    TouchableOpacity,
    Dimensions
} from 'react-native';



export default class ProcessComplete extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: '',
        }

    }


    static navigationOptions = {
        gesturesEnabled: false,
        header: null
    }

    render() {
        const { navigate } = this.props.navigation;

        const width = Dimensions.get('window').width
        const height = Dimensions.get('window').height

        return (
            <View style={styles.container}>
                <View style={styles.lgn_container}>
                    <Image style={width === 320 ? styles320.lgn_icon : styles.lgn_icon} source={require('../../../../img/lg_icon.png')}></Image>
            
                    <Text style={{padding: 10, marginTop: 33}}>Congratulations!</Text>

                    <View style={{alignItems: 'center', justifyContent: 'space-around', width: '100%'}}>

                        <View style={width === 320 ? styles320.btnSignInContainer : styles.btnSignInContainer}>
                            {/* <TouchableOpacity style={width === 320 ? styles320.btnSignIn : styles.btnSignIn} onPress={(create) => this.setState({create: !this.state.create})}> */}
                                {/* <Text style={styles.btnSignInText}>{this.props.wallet.createAddressData}</Text> */}
                                {/* <Icon name='plus-circle' color='#fff' size={25}></Icon> */}
                            {/* </TouchableOpacity> */}

                            

                        </View>
                        <View style={width === 320 ? styles320.btnSignInContainer : styles.btnSignInContainer}>
                            <TouchableOpacity style={width === 320 ? styles320.btnSignIn : styles.btnSignIn} onPress={() => navigate('Login')}>
                                <Text style={styles.btnSignInText}>Login</Text>
                                {/* <Icon name='download' color='#fff' size={25}></Icon> */}
                            </TouchableOpacity>
                        </View>
                    </View>
            
            
                </View>
            </View>
        )
    }

}



const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: '#fff',
        paddingTop: 60
    },
    lgn_icon: {
        height: 136.62,
        width: 127.62,
    },
    lgn_container: {
        width: 296,
        height: 471,
        alignItems: 'center',
    },
    lgn_txtInput: {
        width: 270,
        height: 45.59,
        borderRadius: 50,
        backgroundColor: '#EDEDED',
        opacity: 0.8,
        padding: 10,
        fontSize: 16,
    },
    lgn_txtContainer:{
        width: 325.3,
        height: 102.3,
        marginTop: 47.1,
        justifyContent: 'space-between',
        paddingLeft: 10,
        paddingRight: 10
    },
    fixup:{
        marginTop: 13.1,
        marginBottom: 13.1
    },
    btnSignInContainer: {
        alignItems: 'center', 
        marginTop: 20
    },
    btnSignIn: {
        width: 270, 
        marginBottom: 10.1, 
        shadowColor: '#0F44A0', 
        shadowOffset:{width: 2, height: 2}, 
        shadowOpacity: .8, 
        backgroundColor: '#0F44A0', 
        height:52.94, 
        marginTop: 3, 
        borderRadius: 50, 
        justifyContent: 'center', 
        alignItems:'center',
        flexDirection: 'row'
    },
     btnSignInText: {
        color: 'white', 
        fontSize: 18,
        paddingRight: 5
    },
    signUpContainer: {
        flexDirection: 'row', 
        justifyContent: 'center', 
        alignItems: 'center'
    },
      signUpText: {
        opacity: 0.8, 
        padding: 10
    },
      signUpBtn: {
        color: '#0F44A0', 
        fontWeight: '600'
    }
})