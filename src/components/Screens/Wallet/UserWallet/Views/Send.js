import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    TextInput,
    TouchableOpacity,
    Dimensions,
    ActivityIndicator
} from 'react-native';

import Icon from 'react-native-vector-icons/Feather';


export default class Send extends Component {
    constructor(props) {
        super(props);
        this.state = {
            coinAmount: '',
            sendAddress: this.props.qrcodeData,
            isLoading:false
        }
        this.onChangeToWallet = this.onChangeToWallet.bind(this);
        this.Scanner = this.Scanner.bind(this);
    }

    onChangeToWallet(text) {
        this.setState({
            sendAddress:text
        })
    }
    Scanner() {
        this.setState({
            sendAddress: ''
        })
        this.props.navigate.navigate('Scanner', {
            onGoBack: () => this.refresh(),
        });
    }
    refresh() {
        this.setState({
            sendAddress: this.props.qrcodeData
        })
    }


    render() {
        const UserCoinBalance = this.props.UserCoinBalance;
        const name = this.props.belowTilte
        const icon_image = this.props.customImage;
        const width = Dimensions.get('window').width
        const height = Dimensions.get('window').height
        const { navigate } = this.props.navigate

        return (
            <React.Fragment>
                <View style={styles.hmContainer}>
                    {this.props.customLoader === 'true' ?
                        <View style={{ position: 'absolute', right: 0, left: 0, top: 0, bottom: 0, backgroundColor: '#fff', opacity: .8, height: height, zIndex: 999, justifyContent: 'center', alignItems: 'center' }}><ActivityIndicator size="large" color="#0F44A0" /></View> : null}
                    <View style={styles.hmHeader}>

                        <Text style={{ width: 300, position: 'absolute', left: 20, top: 80, color: '#0F44A0', fontSize: 16 }}>Current Balance: {UserCoinBalance}</Text>
                        <View style={{ flexDirection: 'row', alignItems: 'center', paddingRight: 20 }} >
                            <Image source={{ uri: icon_image }} style={{ height: 30, width: 30, marginRight: 10 }} ></Image>
                            <Text style={{ fontSize: 18, color: '#0F44A0', fontWeight: '300' }}>
                                Send {name}
                            </Text>
                        </View>
                    </View>
                    <View style={styles.btmContainer}>
                        <ScrollView>
                            <View>
                                <Text style={{ marginLeft: 20 }}>Amount to Send:</Text>
                                <View style={styles.txtInputContainer}>
                                    <TextInput keyboardType={'numeric'} placeholder={'0.23'} style={styles.txtInput} onChangeText={(coinAmount) => this.setState({ coinAmount: coinAmount })} underlineColorAndroid={'rgba(0,0,0,0)'} />
                                </View>
                            </View>

                            <View>
                                <Text style={{ marginLeft: 20 }}>Send {name} to:</Text>
                                <View style={[styles.txtInputContainer, { flexDirection: 'row', justifyContent: 'space-around' }]}>
                                    <TextInput style={[styles.txtInput, { width: Dimensions.get('window').width - 60 }]} placeholder='Enter or Scan Address' value={this.state.sendAddress } onChangeText={this.onChangeToWallet} underlineColorAndroid={'rgba(0,0,0,0)'} />
                                    <TouchableOpacity onPress={this.Scanner}><Icon name='camera' size={34} color={'#0F44A0'}></Icon></TouchableOpacity>
                                </View>
                            </View>

                            <View style={styles.btnSendContainer}>
                                <TouchableOpacity style={styles.btnSend} onPress={() => { this.props.sendUsercoin(this.state.coinAmount, this.state.sendAddress) }}>
                                    <Text style={styles.btnSendText}>Send</Text>
                                </TouchableOpacity>
                            </View>
                        </ScrollView>
                    </View>
                </View>
            </React.Fragment>
        )
    }
}



const styles = StyleSheet.create({
    hmContainer: {
        backgroundColor: '#fff',
        height: Dimensions.get('window').height,
        width: Dimensions.get('window').width,
    },
    hmHeader: {
        paddingTop: 20,
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        width: '100%'
    },
    btmContainer: {
        paddingTop: 70,
        justifyContent: 'space-around',
        height: '100%'
    },
    txtInputContainer: {
        marginBottom: 15,
        alignItems: 'center'
    },
    txtInput: {
        width: Dimensions.get('window').width - 20,
        height: 45.59,
        borderRadius: 50,
        backgroundColor: '#EDEDED',
        opacity: 0.8,
        padding: 10,
        fontSize: 16,
    },
    btnSendContainer: {
        alignItems: 'center',
        marginTop: 0
    },
    btnSend: {
        width: 230,
        marginBottom: 10.1,
        shadowColor: '#0F44A0',
        shadowOffset: { width: 2, height: 2 },
        shadowOpacity: .8,
        backgroundColor: '#0F44A0',
        height: 52.94,
        marginTop: 3,
        borderRadius: 50,
        justifyContent: 'center',
        alignItems: 'center'
    },
    btnSendText: {
        color: 'white',
        fontSize: 18
    },
})