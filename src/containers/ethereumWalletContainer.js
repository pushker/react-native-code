import { connect } from 'react-redux';
import  EthereumWallet from '../components/Screens/Wallet/EthereumWallet/';

import { createEthereumTransaction, ethereumDetail, convertEthereum, ethereumBalance } from '../actions/walletAction';

const mapStateToProps = ( state ) => {
    return {
        user: state.user,
        wallet: state.wallet
    }
}

const mapDispatchToProps = ( dispatch ) => {
    return {
        createEthereumTransaction: (data) => {
            dispatch(createEthereumTransaction(data))
        },
        ethereumDetail: (data) => {
            dispatch(ethereumDetail(data))
        },
        convertEthereum: (data) => {
            dispatch(convertEthereum(data))
        },
        ethereumBalance: (data) => {
            dispatch(ethereumBalance(data))
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(EthereumWallet);