import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    Alert,
    ImageBackground,
    TextInput,
    TouchableOpacity,
    Dimensions,
    StatusBar
} from 'react-native';

import {Header} from '../../common/Header'


export default class AccountInfo extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
        this.kycTrigger = this.kycTrigger.bind(this);
    }


    static navigationOptions = {
        header: null
        // title: 'Account Info',
        // gesturesEnabled: false,
        // headerStyle: {backgroundColor: '#0F44A0'},
        // headerTintColor: '#fff',
        // headerTitleStyle: {fontWeight: '400', fontSize: 18}
    }


    componentWillMount() {
        const user = {
            jwt: 'JWT ' + this.props.user.loginData.user.jwtToken,
        }
        this.props.getUserDetail(user)
    }
    kycTrigger() {
        const message = this.props.user.checkKycStatusData.data.status;
        if (message === 'rejected') {
            Alert.alert('Your Profile Status for kyc is '+message);
            setTimeout(() => {
                this.props.navigation.navigate('Onboarding');
            }, 1000);

        } else {
            Alert.alert('Your Profile Status for kyc is '+message);
        }
    }


    render() {
        const {navigate} = this.props.navigation
        return (
            <React.Fragment>
                <StatusBar barStyle='light-content' />

                <Header headerText={'Account Info'}
                        leftComponent={'chevron-left'}
                        leftOnPress={() => navigate('Dashboard')}
                        />

                <View style={styles.mainContainer}>
                    <Image source={require('../../../img/lg_icon.png')}
                            style={styles.img}>
                    </Image >
                    <ScrollView>

                        <View style={[styles.cardContainer, {marginTop: 20}]}>
                            <View>
                                <Text style={styles.cardLabel}>Name</Text>
                            </View>
                            <View>
                                <Text style={styles.cardText}>{this.props.user.accountDetails.name}</Text>
                            </View>
                        </View>

                        <View style={styles.cardContainer}>
                            <View>
                                <Text style={styles.cardLabel}>Email Address</Text>
                                </View>
                            <View>
                                <Text style={styles.cardText}>{this.props.user.accountDetails.email}</Text>
                            </View>
                        </View>

                        <View style={styles.cardContainer}>
                            <View>
                                <Text style={styles.cardLabel}>Account Name</Text>
                            </View>
                            <View>
                                <Text style={styles.cardText}>{this.props.user.accountDetails.account_name}</Text>
                            </View>
                        </View>

                        <View style={styles.cardContainer}>
                            <View>
                                <Text style={styles.cardLabel}>Account Status</Text>
                            </View>
                            <View>{this.props.user.accountDetails.account_activate === 1 ?
                                <Text style={[styles.cardText, {color: 'green'}]}>Active</Text> :
                                <Text style={[styles.cardText, {color: 'red'}]}>Not Active</Text>}
                            </View>
                        </View>
                        <View style={styles.cardContainer}>
                            <View>
                                <Text style={styles.cardLabel}>Verify Account</Text>
                            </View>
                            <TouchableOpacity onPress={this.kycTrigger}>
                                <Text style={styles.cardText}>Click Here</Text>
                            </TouchableOpacity>
                        </View>

                        {this.props.wallet.userWalletDetail.wallets.map((wallet, index) => {
                            return (
                                <View key={index} style={styles.cardContainer}>
                                    <View style={{justifyContent: 'center'}}>
                                        <View>
                                            <Text style={styles.cardLabel}>{wallet.name} Wallet</Text>
                                        </View>
                                        <View style={{marginBottom: 15}}>
                                            <Text style={{fontSize: 16}}>Address:</Text>
                                            <Text>{wallet.address}</Text>
                                        </View>
                                        <View>
                                            <Text style={{fontSize: 16}}>Private Key:</Text>
                                        </View>
                                        <View>
                                            <Text style={{fontSize: 14}}>{wallet.private_key}</Text>
                                        </View>
                                    </View>
                                </View>
                                )
                            })
                        }

                    </ScrollView>
                </View>
            </React.Fragment>
        )
    }
}



const styles = StyleSheet.create({
    mainContainer: {
        justifyContent: 'space-around',
        height: '100%',
        width: '100%',
        paddingLeft: 10,
        paddingTop: 5,
        paddingRight: 1,
        paddingBottom: 50,
        backgroundColor: '#fff'
    },
    img: {
        opacity: .05,
        position: 'absolute',
        alignSelf: 'center',
        marginTop: Dimensions.get('window').height * .2,
        width: 127.62,
        height: 136.62
    },
    cardContainer: {
        alignItems: 'flex-start',
        marginBottom: 30,
        paddingBottom: 10,
        borderBottomWidth: 1,
        borderBottomColor: 'lightgrey'
    },
    cardLabel: {
        fontSize: 14,
        opacity: .8,
        paddingBottom: 5
    },
    cardText: {
        fontSize: 18
    }
})