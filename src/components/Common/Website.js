import React, { Component } from 'react';
import { WebView, StatusBar } from 'react-native';


export default class Website extends Component {

    static navigationOptions = {
        title: 'Javvy Home',
        gesturesEnabled: false,
        headerStyle: {backgroundColor: '#0F44A0'},
        headerTintColor: '#fff',
        headerTitleStyle: {fontWeight: '400', fontSize: 18}
    }

    
    render(){
        return (
            <React.Fragment>
                <StatusBar barStyle='light-content' />
                <WebView
                    source={{uri: 'https://javvy.com'}}   
                />
            </React.Fragment>
        )
    }
}