export function validateEmail(email) {
    var emailRegex = new RegExp("^[a-z0-9_.-]+\.?[a-z0-9_-]+@[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?[.][a-z]{2,61}(?:\.[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?)*$");
    if (email !== '' && email !== undefined && emailRegex.test(email) === true) {
        return 'valid';
    } else if (email !== '' && email !== undefined && emailRegex.test(email) === false) {
        return 'invalid';
    } else {
        return 'empty';
    }
}