import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    KeyboardAvoidingView,
    Animated,
    Image,
    Keyboard,
    TextInput,
    TouchableOpacity,
    Dimensions,
    Switch,
    Alert,
    ActivityIndicator,
    StatusBar
} from 'react-native';

const IMAGE_HEIGHT = 141.39;
const IMAGE_WIDTH = 104.88;


export default class CreateAccount extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            email: '',
            password: '',
            confirmPassword: '',
            toggled: false,
            isLoading: false
        }
        this.handleSubmit = this.handleSubmit.bind(this);
        this.validateNewAccount = this.validateNewAccount.bind(this);
        this.checkpassword = this.checkpassword.bind(this);
        this.focusConfirmPassword = this.focusConfirmPassword.bind(this);

        this.imageHeight = new Animated.Value(IMAGE_HEIGHT);
        this.imageWidth = new Animated.Value(IMAGE_WIDTH);
    }

    componentWillMount() {
        if (Platform.OS === 'ios') {
            this.keyboardWillShowSub = Keyboard.addListener('keyboardWillShow', this.keyboardWillShow);
            this.keyboardWillHideSub = Keyboard.addListener('keyboardWillHide', this.keyboardWillHide);
        } else
            this.keyboardWillShowSub = Keyboard.addListener('keyboardDidShow', this.keyboardWillShow);
        this.keyboardWillHideSub = Keyboard.addListener('keyboardDidHide', this.keyboardWillHide);


    }

    componentWillUnmount() {
        this.keyboardWillShowSub.remove();
        this.keyboardWillHideSub.remove();
    }

    keyboardWillShow = (event) => {
        Animated.timing(this.imageHeight, {
            duration: event.duration,
            toValue: (0),
        }).start();

        Animated.timing(this.imageWidth, {
            duration: event.duration,
            toValue: (0),
        }).start();
    };

    keyboardWillHide = (event) => {
        Animated.timing(this.imageHeight, {
            // duration: event.duration,
            toValue: IMAGE_HEIGHT,
        }).start();

        Animated.timing(this.imageWidth, {
            // duration: event.duration,
            toValue: IMAGE_WIDTH,
        }).start();
    };


    static navigationOptions = {
        gesturesEnabled: false,
        header: null,
    }

    validateNewAccount() {
        const validateNewAccount = "User Registered Successfully and Account Activation Code send to your email."
        console.log(this.props.user.newAccountData);
        const validateErrMessage = "Required Fields: name, email, password";
        const err = this.props.user.newAccountData.message !== validateNewAccount ? this.props.user.newAccountData.message : null
        const clearActivityIndicator = () => this.setState({ isLoading: false })

        if (this.props.user.newAccountData.message === validateNewAccount) {
            this.props.navigation.navigate('ActivateAccount')
            clearActivityIndicator()

        } else if (err === 'Password must have a special symbol,a uppercase character, a lowercase character and a numeric character without any space') {
            Alert.alert(
                'Oh No!',
                'Your password must include a special symbol, an uppercase, lowercase and a numeric character without any space.'
            )
            clearActivityIndicator()

        } else if (err === validateErrMessage) {
            Alert.alert(
                'Oh No!',
                'Please check your input fields.'
            )
            clearActivityIndicator()
        } else {
            Alert.alert(
                '',
                err
            )
            clearActivityIndicator()
        }
    }



    handleSubmit(event) {
        event.preventDefault()
        const firstPassword = this.state.password;

        const user = {
            name: this.state.name,
            email: this.state.email,
            password: this.state.password
        };

        if (this.state.name !== '' && this.state.email !== '' && this.state.password !== '') {
            if (this.state.password === this.state.confirmPassword) {
                this.props.createAccount(user)
                this.setState({ isLoading: true })

                setTimeout(() => {
                    this.validateNewAccount(user)
                }, 3000)
            } else if (this.state.password !== this.state.confirmPassword) {
                Alert.alert(
                    'Oh No!',
                    'Your passwords do not match. Please try again.'
                )
            }
        } else {
            Alert.alert(
                '',
                'Please check your input fields.'
            )
        }

    };


    checkpassword(event) {
        this.setState({ password: event });
    }
    focusConfirmPassword() {
        this.validatePassword(this.state.password);
        this.confirmPassword.focus();
    }
    focusPassword = () => {
        this.validateEmail(this.state.email);
        this.PasswordInput.focus();
    }
    focusEmail = () => {
        this.emailInput.focus()
    }
    toggleButtonChange = () => {
        this.setState({ toggled: !this.state.toggled, name: 'Anonymous' })
        this.focusEmail();
    }

    validateEmail = (email) => {
        var emailRegex = new RegExp("^[a-z0-9_.-]+\.?[a-z0-9_-]+@[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?[.][a-z]{2,61}(?:\.[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?)*$");
        if (email !== '') {
            if (emailRegex.test(email) === true) {
                return true;
            } else {
                Alert.alert(``,
                    `Please enter valid email address.`,
                    [
                        { text: 'OK' }
                    ])
                return false;
            }
        } else {
            Alert.alert(``,
                `Please enter email address.`,
                [
                    { text: 'OK' }
                ])
            return false;
        }
    }
    validatePassword = (password) => {
        // var strongRegex = new RegExp("^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%_*#?&]{8,}$");
        var strongRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])\S{8,}/;
        if (password !== '') {
            if (strongRegex.test(password) === true) {
                return true;
            } else {
                Alert.alert(
                    'Oh No!',
                    'Your password must include a special symbol, an uppercase, lowercase and a numeric character without any space with minimum 8 characters.'
                )
                return false;
            }
        } else {
            Alert.alert(``,
                `Please enter password.`,
                [
                    { text: 'OK' }
                ])
            return false;
        }
    }


    render() {

        const { navigate } = this.props.navigation
        const width = Dimensions.get('window').width
        const height = Dimensions.get('window').height

        return (
            <React.Fragment>
                <StatusBar barStyle={'light-content'} />
                <KeyboardAvoidingView style={styles.container} behavior="padding" enabled={true}>
                    {this.state.isLoading === true ?
                        <View style={{ position: 'absolute', right: 0, left: 0, top: 0, bottom: 0, backgroundColor: '#fff', opacity: .8, height: height, zIndex: 999, justifyContent: 'center', alignItems: 'center' }}><ActivityIndicator size="large" color="#0F44A0" /></View> : null}
                    <View style={styles.lgn_container}>
                        <View style={{ width: '100%', justifyContent: 'center', alignItems: 'center', paddingTop: 40, paddingBottom: 10, backgroundColor: '#0F44A0' }}>
                            <Animated.Image style={[width === 320 ? styles320.lgn_icon : styles.lgn_icon, { height: this.imageHeight, width: this.imageWidth }]} source={require('../../../../img/lg_icon.png')} />
                            <Text style={{ textAlign: 'center', paddingTop: 5, fontSize: 10, color: 'white' }}>v1.0.0</Text>
                        </View>
                        <View style={width === 320 ? styles320.lgn_txtContainer : styles.lgn_txtContainer}>

                            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>

                                {!this.state.toggled ?
                                    <TextInput onChangeText={(name) => this.setState({ name: name })}
                                        keyboardType={'default'}
                                        autoCapitalize={'sentences'}
                                        style={width === 320 ? [styles320.lgn_txtInput, { width: 250 }] : [styles.lgn_txtInput, { width: 250 }]}
                                        placeholder={'Name'}
                                        editable={true}
                                        autoFocus
                                        underlineColorAndroid={'rgba(0,0,0,0)'}
                                        autoCorrect={false}
                                        onSubmitEditing={this.focusEmail}>
                                    </TextInput>
                                    :
                                    <TextInput onChangeText={(name) => this.setState({ name: name })}
                                        keyboardType={'default'}
                                        autoCapitalize={'sentences'}
                                        style={width === 320 ? [styles320.lgn_txtInput, { width: 250 }] : [styles.lgn_txtInput, { width: 250 }]}
                                        value={'Anonymous'}
                                        editable={false}
                                        autoFocus
                                        underlineColorAndroid={'rgba(0,0,0,0)'}
                                        onSubmitEditing={this.focusEmail}>
                                    </TextInput>
                                }
                                <Switch onValueChange={this.toggleButtonChange} value={this.state.toggled} onTintColor={'#0F44A0'} />
                            </View>


                            <TextInput onChangeText={(email) => this.setState({ email: email })}
                                keyboardType={'email-address'}
                                autoCapitalize={'none'}
                                style={[width === 320 ? styles320.lgn_txtInput : styles.lgn_txtInput, styles.fixup]}
                                placeholder={'Email Address'}
                                underlineColorAndroid={'rgba(0,0,0,0)'}
                                autoCorrect={false}
                                ref={(ref) => { this.emailInput = ref; }}
                                onSubmitEditing={this.focusPassword}>
                            </TextInput>

                            <TextInput onSubmitEditing={this.focusConfirmPassword} onChangeText={this.checkpassword}
                                keyboardType={'default'}
                                autoCapitalize={'none'}
                                secureTextEntry={true}
                                style={width === 320 ? styles320.lgn_txtInput : styles.lgn_txtInput}
                                placeholder={'Password'}
                                ref={(ref) => { this.PasswordInput = ref; }}
                                underlineColorAndroid={'rgba(0,0,0,0)'}>
                            </TextInput>

                            <TextInput onSubmitEditing={this.handleSubmit} ref={(ref) => { this.confirmPassword = ref; }} onChangeText={(confirmPassword) => this.setState({ confirmPassword: confirmPassword })}
                                keyboardType={'default'}
                                autoCapitalize={'none'}
                                secureTextEntry={true}
                                style={[width === 320 ? styles320.lgn_txtInput : styles.lgn_txtInput, styles.fixup]}
                                placeholder={'Re-Enter Password'}
                                underlineColorAndroid={'rgba(0,0,0,0)'}>
                            </TextInput>



                            <View style={width === 320 ? styles320.btnSignInContainer : styles.btnSignInContainer}>
                                <TouchableOpacity style={width === 320 ? styles320.btnSignIn : styles.btnSignIn} onPress={this.handleSubmit}>
                                    <Text style={styles.btnSignInText}>Confirm</Text>
                                </TouchableOpacity>
                            </View>


                            <View style={styles.signUpContainer}>
                                <Text style={styles.signUpText}>Already have an account?</Text>
                                <TouchableOpacity onPress={() => navigate('Login')}>
                                    <Text style={styles.signUpBtn}>Sign In</Text>
                                </TouchableOpacity>
                            </View>

                        </View>
                    </View>
                </KeyboardAvoidingView>
            </React.Fragment>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
    lgn_icon: {
        height: 108,
        width: 80,
    },
    lgn_container: {
        width: '100%',
        height: 450,
        alignItems: 'center',
    },
    lgn_txtInput: {
        width: 325.3,
        height: 45.59,
        borderRadius: 50,
        backgroundColor: '#EDEDED',
        opacity: 0.8,
        padding: 10,
        fontSize: 16,
    },
    lgn_txtContainer: {
        width: 325.3,
        height: 102.3,
        marginTop: 27.1,
        justifyContent: 'space-between',
    },
    fixup: {
        marginTop: 13.1,
        marginBottom: 13.1
    },
    rstPassContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 48.1
    },
    rstPassText: {
        opacity: 0.8,
        padding: 5,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center'
    },
    rstPassBtn: {
        color: '#0F44A0',
        fontWeight: '600'
    },
    btnSignInContainer: {
        alignItems: 'center',
        marginTop: 20
    },
    btnSignIn: {
        width: 230,
        marginBottom: 10.1,
        shadowColor: '#0F44A0',
        shadowOffset: { width: 2, height: 2 },
        shadowOpacity: .8,
        backgroundColor: '#0F44A0',
        height: 52.94,
        marginTop: 3,
        borderRadius: 50,
        justifyContent: 'center',
        alignItems: 'center'
    },
    btnSignInText: {
        color: 'white',
        fontSize: 18
    },
    signUpContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    signUpText: {
        opacity: 0.8,
        padding: 10
    },
    signUpBtn: {
        color: '#0F44A0',
        fontWeight: '600'
    }
});


const styles320 = {
    lgn_icon: {
        height: 106.62,
        width: 97.62,
    },
    lgn_txtInput: {
        width: 305.3,
        height: 45.59,
        borderRadius: 50,
        backgroundColor: '#EDEDED',
        opacity: 0.8,
        padding: 10,
        fontSize: 16,
    },
    lgn_txtContainer: {
        width: 305.3,
        height: 102.3,
        justifyContent: 'space-between',
    },
    rstPassContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 0.1
    },
    btnSignInContainer: {
        alignItems: 'center'
    },
    btnSignIn: {
        width: 230,
        marginBottom: 10.1,
        shadowColor: '#0F44A0',
        shadowOffset: { width: 2, height: 2 },
        shadowOpacity: .8,
        backgroundColor: '#0F44A0',
        height: 52.94,
        marginTop: 3,
        borderRadius: 50,
        justifyContent: 'center',
        alignItems: 'center'
    }
}