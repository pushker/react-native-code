import { connect } from 'react-redux';
import AccountInfo from '../components/Screens/AccountInfo';

import { getUserDetail } from '../actions/userAction';


const mapStateToProps = ( state ) => {
    return {
        user: state.user,
        wallet: state.wallet
    }
}

const mapDispatchToProps = ( dispatch ) => {
    return {
        getUserDetail: (data) => {
            dispatch(getUserDetail(data))
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(AccountInfo);