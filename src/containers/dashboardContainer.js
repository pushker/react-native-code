import { connect } from 'react-redux';
import  Dashboard from '../components/Screens/Dashboard';

import { createAccount, transactionsDetail, compareBitcoin, compareEthereum, countryList, checkKycStatus} from '../actions/userAction';
import { userWalletDetail, bitcoinBalance, ethereumBalance, getCoins, getFavCoins } from '../actions/walletAction'

const mapStateToProps = ( state ) => {
    return {
        user: state.user,
        wallet: state.wallet
    }
}

const mapDispatchToProps = ( dispatch ) => {
    return {
        getCoinsDetail: (data) => {
            dispatch(getCoins(data))
        },
        getFavCoins: (data) => {
            dispatch(getFavCoins(data))
        },
        createAccount: (data) => {
            dispatch(createAccount(data))
        },
        userWalletDetail: (data) => {
            dispatch(userWalletDetail(data))
        },
        transactionsDetail: (data) => {
            dispatch(transactionsDetail(data))
        },
        bitcoinBalance: (data) => {
            dispatch(bitcoinBalance(data))
        },
        ethereumBalance: (data) => {
            dispatch(ethereumBalance(data))
        },
        compareBitcoin: (data) => {
            dispatch(compareBitcoin(data))
        },
        compareEthereum: (data) => {
            dispatch(compareEthereum(data))
        },
        countryList: (data) => {
            dispatch(countryList(data))
        },
        checkKycStatus: (data) => {
            dispatch(checkKycStatus(data))
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);