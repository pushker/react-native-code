import { combineReducers } from 'redux';
import userReducer from './userReducer';
import walletReducer from './walletReducer';


export default combineReducers({
    user: userReducer,
    wallet: walletReducer
});