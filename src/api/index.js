// Universal App Token
export const token = 'eyJpc3MiOiJ0b3B0YWwuY29tIiwiZXhwIjoxNDI2NDIwODAwLCJodHRwOi8vdG9wd';

export const baseUrlTest = 'http://35.166.64.139:3002/';
//export const baseUrlLive = 'http://34.226.11.203:3000/';
export const baseUrlLive = 'http://35.166.64.139:3002/';
//export const baseUrlLive = 'https://api.javvy.com/';

// rev
// export const baseUrlLive = 'http://35.166.64.139:3002/'


export const compareBTCUrl = 'https://min-api.cryptocompare.com/data/price?fsym=BTC&tsyms=USD,EUR'
export const compareETHUrl = 'https://min-api.cryptocompare.com/data/price?fsym=ETH&tsyms=USD,EUR'

// http://34.226.11.203:3000/
// LIVE

// http://35.166.64.139:3002/
// TEST