import { Header } from '../../../../common/Header'
import React, {Component} from "react";
import {StyleSheet, Text, View, TouchableOpacity, Button, Dimensions, Image} from "react-native";
import { RNCamera } from 'react-native-camera';

export default class Onboarding extends Component {

    constructor (props) {
        super(props)
        this.state = {
            path: null
        }

    }

    static navigationOptions = {
        header: null
    };

    componentDidMount() {

    }

    takePicture = async function() {
        if (this.camera) {
            const options = { quality: 0.5, base64: true };
            const data = await this.camera.takePictureAsync(options);
            this.setState({path: data.uri});
            console.log(data.uri);
        }
    };

    goCapture = () => {
        console.log('go to camera page')
    }

    renderCamera() {
        const {navigate} = this.props.navigation;
        return (
            <React.Fragment>
                <Header headerText={'Javvy Verification - KYC'} />
                <View style={styles.container}>
                    <RNCamera
                        ref={ref => {
                            this.camera = ref;
                        }}
                        style = {styles.preview}
                        type={RNCamera.Constants.Type.front}
                        flashMode={RNCamera.Constants.FlashMode.on}
                        permissionDialogTitle={'Permission to use camera'}
                        permissionDialogMessage={'We need your permission to use your camera phone'}
                    />
                    <View style={{flex: 0, flexDirection: 'row', justifyContent: 'center',}}>
                        <TouchableOpacity
                            onPress={this.takePicture.bind(this)}
                            style = {styles.capture}
                        >
                            <Text style={{fontSize: 14}}> SNAP </Text>
                        </TouchableOpacity>
                    </View>
                </View>

            </React.Fragment>
        )
    }

    renderImage() {
        return (
            <View>
                <Image
                    source={{ uri: this.state.path }}
                    style={styles.preview}
                />
                <Text
                    style={styles.cancel}
                    onPress={() => this.setState({ path: null })}
                >Cancel
                </Text>
            </View>
        );
    }

    render() {
        return (
            <View style={styles.container}>
                {this.state.path ? this.renderImage() : this.renderCamera()}
            </View>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: 'black'
    },
    capture: {
        flex: 0,
        backgroundColor: '#fff',
        borderRadius: 5,
        padding: 15,
        paddingHorizontal: 20,
        alignSelf: 'center',
        margin: 20
    },
    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
        height: Dimensions.get('window').height,
        width: Dimensions.get('window').width
    },
    cancel: {
        position: 'absolute',
        right: 20,
        top: 20,
        backgroundColor: 'transparent',
        color: '#FFF',
        fontWeight: '600',
        fontSize: 17,
    }
});