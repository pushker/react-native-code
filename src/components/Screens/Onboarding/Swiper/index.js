import React, {Component} from 'react';
import {
    Dimensions,
    Platform,
    ScrollView,
    StyleSheet,
    View
} from 'react-native';

import Button from '../Button';
import { dim } from '../../../../../node_modules/ansi-colors';

const {width, height} = Dimensions.get('window');

export default class OnboardingScreens extends Component {

    // Props for ScrollView
    static defaultProps = {
        // Arrange screens horizontally
        horizontal: true,

        // Scroll exactly to the next screen, instead of continuos scrolling
        pagingEnabled: true,

        // Hide all scroll indicators
        showsHorizontalScrollIndicator: false,
        showsVerticalScrollIndicator: false,

        // Disable bounce when end is reached
        bounces: false,

        // Do not scroll to top when status bar is tapped
        scrollsToTop: false,

        // Remove offscreen child views
        removeClippedSubviews: true,

        // Do not adjust content behind nav-, tab- or toolbars automatically
        automaticallyAdjustContentInsets: false,

        // Active Screen
        index: 0
    };

    state = this.initState(this.props);

    initState(props) {
        // total slides
        const total = props.children ? props.children.length || 1 : 0,
        // current index
        index = total > 1 ? Math.min(props.index, total - 1) : 0,
        // current offset
        offset = width * index;

        const state = {
            total,
            index,
            offset,
            width,
            height
        };

        this.internals = {
            isScrolling: false,
            offset
        };

        return state;
    }

    _onScrollBegin = () => {
        this.internals.isScrolling = true;
    }

    _onScrollEnd = (e) => {
        this.internals.isScrolling = false;

        this._updateIndex(e.nativeEvent.contentOffset
            ? e.nativeEvent.contentOffset.x
            : e.nativeEvent.position * this.state.width
        )
    }

    _onScrollEndDrag = (e) => {
        const { contentOffset: {x: newOffset} } = e.nativeEvent,
        { children } = this.props,
        { index } = this.state,
        { offset } = this.internals;

        // Update internal isScrollingState
        // if swiped right on the last slide
        // or left on the first slide
        if (offset === newOffset &&
            (index === 0 || index === children.length - 1)) {
            this.internals.isScrolling = false;
        }
    }

    _updateIndex = (offset) => {
        const state = this.state,
            diff = offset - this.internals.offset,
            step = state.width;
        let index = state.index;

        if (!diff) {
            return;
        }

        // Ensures index is constantly an integer
        index = parseInt(index + Math.round(diff / step), 10);

        this.internals.offset = offset;

        this.setState({
            index
        });
    }

    _swipe = () => {
        if (this.internals.isScrolling || this.state.total < 2) {
            return;
        }

        const state = this.state,
            diff = this.state.index + 1,
            x = diff * state.width,
            y = 0;

        // Call scrollTo on scrollView component to perform the swipe
        this.scrollView && this.scrollView.scrollTo({
            x,
            y,
            animated: true
        });

        // Update internal scroll state
        this.internals.isScrolling = true;

        // Trigger onScrollEnd manually on android
        if (Platform.OS === 'android') {
            setImmediate(() => {
                this._onScrollEnd({
                    nativeEvent: {
                        position: diff
                    }
                })
            })
        }
    }

    _renderScrollView = (screens) => {
        return(
            <ScrollView ref={component => {this.scrollView = component;} }
                {...this.props}
                contentContainerStyle={[styles.wrapper, this.props.style]}
                onScrollBeginDrag={this._onScrollBegin}
                onMomentumScrollEnd={this._onScrollEnd}
                onScrollEndDrag={this._onScrollEndDrag}
            >
                {screens.map((screen, index) =>
                    <View style={[styles.fullScreen, styles.slide]} key={index}>
                        {screen}
                    </View>
                )}
            </ScrollView>
        )
    }

    _renderPagination = () => {
        if (this.state.total <= 1) {
            return null;
        }

        const ActiveDot = <View style={[styles.dot, styles.activeDot]} />,
            Dot = <View style={styles.dot} />

        let dots = [];

        for (let key = 0; key < this.state.total; key++) {
            dots.push(key === this.state.index
                // Active dot
                ? React.cloneElement(ActiveDot, {key})
                // Other dots
                : React.cloneElement(Dot, {key})
            );
        }

        return (
            <View
                pointerEvents="none"
                style={[styles.pagination, styles.fullScreen]}
            >
                {dots}
            </View>
        );
    }

    _renderButton = () => {
        const navigate = this.props.navigate
        const lastScreen = this.state.index === this.state.total - 1;
        return (
            <View pointerEvents="box-none" style={[styles.buttonWrapper, styles.fullScreen]}>
                {lastScreen
                // Show this button on the last screen
                // TODO: Add a handler that would send a user to your app after onboarding is complete
                ? <Button text="Submit" onPress={() => navigate('AccountInfo')} />
                // Or this one otherwise
                : <Button text="Upload" onPress={() => this._swipe()} />
                }
            </View>
        )
    }

    render = ({ children } = this.props) => {
        console.log(this.props,'checking');
        return (
            <View style={[styles.container, styles.fullScreen]}>
                {/* Render screens */}
                {this._renderScrollView(children)}
                {/* Render pagination */}
                {this._renderPagination()}
                {/* Render Contine or Done button */}
                {this._renderButton()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    // Set width and height to the screen size
    fullScreen: {
        width: width,
        height: height
    },
    // Main container
    container: {
        backgroundColor: 'transparent',
        position: 'relative'
    },
    // Slide
    slide: {
        backgroundColor: 'transparent'
    },
    // Pagination indicators
    pagination: {
        position: 'absolute',
        bottom: 110,
        left: 0,
        right: 0,
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'flex-end',
        backgroundColor: 'transparent'
    },
    // Pagination dot
    dot: {
        backgroundColor: 'rgba(0,0,0,.25)',
        width: 8,
        height: 8,
        borderRadius: 4,
        marginLeft: 3,
        marginRight: 3,
        marginTop: 3,
        marginBottom: 3
    },
    // Active dot
    activeDot: {
        backgroundColor: '#FFFFFF',
    },
    // Button wrapper
    buttonWrapper: {
        backgroundColor: 'transparent',
        flexDirection: 'column',
        position: 'absolute',
        top: Dimensions.get('window').width / 2.85,
        left: 0,
        flex: 1,
        paddingHorizontal: 10,
        paddingVertical: 200,
        alignContent: 'flex-end',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        alignSelf: 'flex-end'
    },
});