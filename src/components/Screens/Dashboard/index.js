import React, {Component} from 'react';
import {
    StyleSheet,
    Dimensions,
    StatusBar,
    View,
    ActivityIndicator
} from 'react-native';

import Tabs from '../../common/Tabs'
import Drawer from 'react-native-drawer'

import { Header } from '../../common/Header';
import CryptoMenu from '../../common/CryptoMenu';
import AltMenu from '../../common/AltMenu';

// Tabs
import Home from './Views/Home';
import Transactions from './Views/Transactions';


export default class Dashboard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            leftModalOpen: false,
            rightModalOpen: false,
            isLoading: true
        }
        this.openCryptoMenu = this.openCryptoMenu.bind(this);
        this.openAltMenu = this.openAltMenu.bind(this);
    }

    static navigationOptions = {
        header: null,
        gesturesEnabled: false,
    }


    componentDidMount() {

        const user = {
            jwt: 'JWT ' + this.props.user.loginData.user.jwtToken,
        }

        // get all coins
        this.props.getCoinsDetail(user)
        this.props.getFavCoins(user)
        this.props.userWalletDetail(user)
        this.props.transactionsDetail(user)
        this.props.bitcoinBalance(user)
        this.props.ethereumBalance(user)
        this.props.countryList(user)
        this.props.checkKycStatus(user)
        this.props.compareBitcoin()
        this.props.compareEthereum()
    }


    openCryptoMenu() {
        setTimeout(() => { this.setState({ leftModalOpen: true }) }, 0);
        this._leftDrawer.open()
    }

    closeCryptoMenu = (value) => {
        setTimeout(() => { this.setState({ leftModalOpen: value }) }, 0);
        this._leftDrawer.close()
    }



    openAltMenu() {
        setTimeout(() => { this.setState({ rightModalOpen: true }) }, 0);
        this._rightDrawer.open()
    }

    render() {
        const loadModal = this.state.modalReady === true
        const width = Dimensions.get('window').width
        const height = Dimensions.get('window').height
        console.log(this.props,'sdgdsg');
        const drawerStylesLeft = {drawer: {backgroundColor: 'white', borderTopRightRadius: 30, borderBottomRightRadius: 30, maxHeight: height - 100, borderRightWidth: 1, borderBottomWidth: 1, borderTopWidth: 1,  borderColor: 'lightgrey'}}
        const drawerStylesRight = { drawer: { backgroundColor: 'white', borderTopLeftRadius: 30, borderBottomLeftRadius: 30, maxHeight: height - 300, borderLeftWidth: 1, borderBottomWidth: 1, borderTopWidth: 1, borderColor: 'lightgrey' } }

        return (
            <React.Fragment>
                {/* {this.state.isLoading === true ?  */}
                    {/* <View style={{position: 'absolute', right: 0, left: 0, top: 0, bottom: 0, backgroundColor: '#fff', opacity: 1, height: height, zIndex: 999, justifyContent: 'center', alignItems: 'center'}}><ActivityIndicator size="large" color="#0F44A0" /></View> : null } */}
                <StatusBar barStyle="light-content" />


                <Header headerText={'Dashboard'}
                        leftComponent={'menu'}
                        leftOnPress={this.openCryptoMenu}
                        rightComponent={'more-horizontal'}
                        rightOnPress={this.openAltMenu} />

                <Drawer tapToClose={true} initialNumToRender={0} content={<CryptoMenu closeModal={this.closeCryptoMenu} navigate={this.props.navigation}  /> } tapToClose={true} open={false} type={'overlay'} styles={drawerStylesLeft} side={'left'} openDrawerOffset={(viewport) => viewport.width - 150} ref={(ref) => this._leftDrawer = ref}>
                    <Drawer tapToClose={true} content={<AltMenu navigate={this.props.navigation} />} tapToClose={true} open={false} type={'overlay'} styles={drawerStylesRight} side={'right'} openDrawerOffset={(viewport) => viewport.width - 150} ref={(ref) => this._rightDrawer = ref}>

                <Tabs initialTab={0} maxLength={0}>

                    {/* Tab 1: Home */}
                    <Home title="Home" name={this.props.user.loginData.user.name}
                                    bitcoinBalance={this.props.wallet.bitcoinBalance.confirm_balance}
                                    ethereumBalance={this.props.wallet.ethereumBalance.confirm_balance}
                                    bitcoinRate={this.props.user.bitcoinRate}
                                    ethereumRate={this.props.user.ethereumRate} />

                    {/* Tab 2: Transactions */}
                    <Transactions title="Transactions" transactionsList={this.props.user.transactionsDetail.transactions}/>

                </Tabs>

                </Drawer>
                </Drawer>

            </React.Fragment>
        )
    }
}

const styles = StyleSheet.create({
    hmContainer: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 20
    },
    hmHeader: {
        justifyContent: 'center',
        alignItems: 'center',
    }
})