import { connect } from 'react-redux';
import CreateAccount from '../components/Screens/Register/Views/CreateAccount';

import { createAccount } from '../actions/userAction';

const mapStateToProps = ( state ) => {
    return {
        user: state.user
    }
}

const mapDispatchToProps = ( dispatch ) => {
    return {
        createAccount: (data) => {
            dispatch(createAccount(data))
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(CreateAccount);