import { connect } from 'react-redux';
import { createAddress, userWalletDetail } from '../../actions/walletAction.js';

import CreateAddress from '../../components/Screens/Register/Views/CreateAddress.js';


const mapStateToProps = ( state ) => {
    return {
        user: state.user,
        wallet: state.wallet
    }
}

const mapDispatchToProps = ( dispatch ) => {
    return {
        createAddress: (wallet) => {
            dispatch(createAddress(wallet))
        },
        userWalletDetail: (user) => {
            dispatch(userWalletDetail(user))
        }
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(CreateAddress);