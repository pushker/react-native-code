import { connect } from 'react-redux';
import BackKyc from '../components/Screens/BackKyc';
import { saveBackId } from '../actions/userAction';


const mapStateToProps = (state) => {
    return {
        user: state.user,
        wallet: state.wallet
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        saveBackId: (data) => {
            dispatch(saveBackId(data))
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(BackKyc);