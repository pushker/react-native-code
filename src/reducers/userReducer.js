import {
    LOGIN,
    FORGOT_PASSWORD,
    CREATE_ACCOUNT,
    ACTIVATE_ACCOUNT,
    RESEND_ACTIVATION_CODE,
    GET_USER_DETAIL,
    TRANSACTIONS_DETAIL,
    COMPARE_BITCOIN,
    COMPARE_ETHEREUM,
    UPDATE_PASSWORD,
    SELFIE_IMAGE,
    COUNTRY_LIST,
    STATE_LIST,
    SAVE_COUNTRY_STATE,
    SAVE_FRONT_ID,
    SAVE_BACK_ID,
    SAVE_FULL_KYC_WITH_UTILITY,
    GET_KYC_DOCUMENTS,
    CHECK_KYC_STATUS
} from '../actions/types';


const initialState = {
    activateAccountData: {},
    newAccountData: {},
    loginData: {},
    activationCode: {},
    accountDetails: {},
    transactionsDetail: {},
    bitcoinRate: {},
    ethereumRate: {},
    resetPassCode: {},
    selfieImageData: {},
    countryListData: {},
    stateListData: {},
    countryStateKycData: {},
    frontIdData: {},
    backIdData: {},
    utilityInfoData: {},
    kycDocumentsData: {},
    checkKycStatusData: {}
}


export default user = (state = initialState, action) => {
    switch (action.type) {
        case ACTIVATE_ACCOUNT:
            return {
                ...state,
                activateAccountData: action.payload
            }
        case CREATE_ACCOUNT:
            return {
                ...state,
                newAccountData: action.payload
            }
        case GET_USER_DETAIL:
            return {
                ...state,
                accountDetails: action.payload
            }
        case LOGIN:
            return {
                ...state,
                loginData: action.payload
            }
        case FORGOT_PASSWORD:
            return {
                ...state,
                resetPassCode: action.payload
            }
        case UPDATE_PASSWORD:
            return {
                ...state,
                updatePassData: action.payload
            }
        case RESEND_ACTIVATION_CODE:
            return {
                ...state,
                activationCode: action.payload,
                activateAccountData: {}
            }
        case TRANSACTIONS_DETAIL:
            return {
                ...state,
                transactionsDetail: action.payload
            }
        case COMPARE_BITCOIN:
            return {
                ...state,
                bitcoinRate: action.payload
            }
        case COMPARE_ETHEREUM:
            return {
                ...state,
                ethereumRate: action.payload
            }
        case SELFIE_IMAGE:
            return {
                ...state,
                selfieImageData:action.payload
            }
        case COUNTRY_LIST:
            return {
                ...state,
                countryListData:action.payload
            }
        case STATE_LIST:
            return {
                ...state,
                stateListData:action.payload
            }
        case SAVE_COUNTRY_STATE:
            return {
                ...state,
                countryStateKycData:action.payload
            }
        case SAVE_FRONT_ID:
            return {
                ...state,
                frontIdData:action.payload
            }
        case SAVE_BACK_ID:
            return {
                ...state,
                backIdData:action.payload
            }
        case SAVE_FULL_KYC_WITH_UTILITY:
            return {
                ...state,
                utilityInfoData:action.payload
            }
        case GET_KYC_DOCUMENTS:
            return {
                ...state,
                kycDocumentsData:action.payload
            }
        case CHECK_KYC_STATUS:
            return {
                ...state,
                checkKycStatusData:action.payload
            }
        default:
            return state
    }
}