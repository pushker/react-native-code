import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    TouchableOpacity,
    Dimensions,
    StatusBar,
    Alert
} from 'react-native';

import  { Header } from '../../../common/Header';

import CryptoMenu from '../../../common/CryptoMenu';
import AltMenu from '../../../common/AltMenu';

import Drawer from 'react-native-drawer';

import Tabs from '../../../common/Tabs';

import Buy from './Views/Buy';
import Convert from './Views/Convert';
import Receive from './Views/Receive';
import Sell from './Views/Sell';
import Send from './Views/Send';



export default class EthereumWallet extends Component {
    constructor(props) {
        super(props);
        this.state = {
            leftModalOpen: false,
            rightModalOpen: false
        }
        this.openCryptoMenu = this.openCryptoMenu.bind(this);
        this.openAltMenu = this.openAltMenu.bind(this);
        this.convertEthereum = this.convertEthereum.bind(this);
    }


    static navigationOptions = {
        header: null,
        gesturesEnabled: false,
    }

    componentDidMount(){
        const getEthereumObject = {
            jwt: 'JWT ' + this.props.user.loginData.user.jwtToken
        }
        this.props.ethereumDetail(getEthereumObject)
        this.props.ethereumBalance(getEthereumObject)
    }


    openCryptoMenu(){
        this.setState({leftModalOpen: true})
        this._leftDrawer.open()
    }


    openAltMenu(){
        this.setState({rightModalOpen: true})
        this._rightDrawer.open()
    }


    convertEthereum(coinAmount){
        const convertEthereumObject = {
            amount: coinAmount,
            fromCoin: 'ETH',
            toCoin: 'BTC',
            jwt: 'JWT ' + this.props.user.loginData.user.jwtToken
        }

        this.props.convertEthereum(convertEthereumObject)
        setTimeout(() => {
            Alert.alert(this.props.wallet.ethereumConversionData.message)
        }, 2000);
    }

    sendEthereum(coinAmount, sendAddress){
        const sendEthereumObject = {
            value: coinAmount,
            to: sendAddress,
            account_name: this.props.user.loginData.user.account_name,
            jwt: 'JWT ' + this.props.user.loginData.user.jwtToken
        }

        this.props.createEthereumTransaction(sendEthereumObject)
        setTimeout(() => {
            Alert.alert(this.props.wallet.ethereumTransactionDetails.message)
        }, 2000);

    }



    render() {

        const loadModal = this.state.modalReady === true
        const width = Dimensions.get('window').width
        const height = Dimensions.get('window').height

        const drawerStylesLeft = {drawer: {backgroundColor: 'white', borderTopRightRadius: 30, borderBottomRightRadius: 30, maxHeight: height - 100, borderRightWidth: 1, borderBottomWidth: 1, borderTopWidth: 1,  borderColor: 'lightgrey'}}
        const drawerStylesRight = {drawer: {backgroundColor: 'white', borderTopLeftRadius: 30, borderBottomLeftRadius: 30, maxHeight: height - 300, borderLeftWidth: 1, borderBottomWidth: 1, borderTopWidth: 1,  borderColor: 'lightgrey'}}

        return (
            <React.Fragment>
                <StatusBar barStyle="light-content" />


                <Header headerText={'Ethereum Home'}
                        leftComponent={'menu'}
                        leftOnPress={this.openCryptoMenu}
                        rightComponent={'more-horizontal'}
                        rightOnPress={this.openAltMenu} />

                <Drawer content={ <CryptoMenu navigate={this.props.navigation} /> } tapToClose={true} open={false} type={'overlay'} styles={drawerStylesLeft} side={'left'} openDrawerOffset={(viewport) => viewport.width - 150} ref={(ref) => this._leftDrawer = ref}>
                <Drawer content={ <AltMenu navigate={this.props.navigation} />  } tapToClose={true}  open={false} type={'overlay'} styles={drawerStylesRight} side={'right'} openDrawerOffset={(viewport) => viewport.width - 150} ref={(ref) => this._rightDrawer = ref}>

                    <Tabs initialTab={4} maxLength={4}>

                        {/* Tab 1: Buy */}
                        <Buy title="Buy" navigate={this.props.navigation} />

                        {/* Tab 2: Sell */}
                        <Sell title="Sell" navigate={this.props.navigation} />

                        {/* Tab 3: Convert */}
                        <Convert title="Convert" convertEthereum={(coinAmount) => this.convertEthereum(coinAmount)} />

                        {/* Tab 4: Send */}
                        <Send title="Send" sendEthereum={(coinAmount, sendAddress) => this.sendEthereum(coinAmount, sendAddress)}
                                ethereumBalance={this.props.wallet.ethereumBalance.confirm_balance}
                                        qrcodeData={this.props.wallet.qrcodeData}
                                        navigate={this.props.navigation}/>

                        {/* Tab 5: Recieve */}
                            <Receive title="Receive" getEthereumDetail={() => this.getEthereumDetail()} ethereumBalance={this.props.wallet.ethereumBalance.confirm_balance}
                                                payload={this.props.wallet.ethereumDetail.address}/>

                    </Tabs>

                </Drawer>
                </Drawer>
            </React.Fragment>
        )
    }
}