import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    TouchableOpacity,
    Dimensions,
    StatusBar,
    Alert,
    ActivityIndicator,
    AsyncStorage
} from 'react-native';

import { Header } from '../../../common/Header';

import CryptoMenu from '../../../common/CryptoMenu';
import AltMenu from '../../../common/AltMenu';

import Drawer from 'react-native-drawer';

import Tabs from '../../../common/Tabs';

import Buy from './Views/Buy'
import Convert from './Views/Convert'
import Receive from './Views/Receive'
import Sell from './Views/Sell'
import Send from './Views/Send'
import { FetchPost } from '../../../../helpers/index';


export default class UserWallet extends Component {
    constructor(props) {
        super(props);
        this.state = {
            leftModalOpen: false,
            rightModalOpen: false,
            wallet_flag: true,
            estimated_val: '',
            check_conversion:false
        }
        this.openCryptoMenu = this.openCryptoMenu.bind(this);
        this.openAltMenu = this.openAltMenu.bind(this);

    }


    static navigationOptions = {
        header: null,
        gesturesEnabled: false,
    }

    componentDidMount() {
        const getBitcoinObjectJwt = {
            jwt: 'JWT ' + this.props.user.loginData.user.jwtToken,
        }
        const getBitcoinObject = {
            jwt: 'JWT ' + this.props.user.loginData.user.jwtToken,
            wallet_code: this.props.wallet.userCommonWalletDeatil.wallet_code
        }
    }


    openCryptoMenu() {
        this.setState({ leftModalOpen: true })
        this._leftDrawer.open()
    }


    openAltMenu() {
        this.setState({ rightModalOpen: true })
        this._rightDrawer.open()
    }

    estimateCoin(to, from) {
        const estimateObject = {
            pair:from+"_"+to
        }
        console.log(estimateObject);
        const jwt = {
            jwtToken: 'JWT ' + this.props.user.loginData.user.jwtToken
        }
        FetchPost('user/shapeshift/rate', estimateObject, jwt.jwtToken)
            .then((resp) => {
                this.props.estimateRateCoin(resp)
                this.setState({
                    estimated_val: ''
                })
            }).catch((err) => {
                this.setState({
                    estimated_val: ''
                })
                Alert.alert('Something went wrong !!')
                console.log(err, 'error');
            })

    }
    custom_rate_calculate(coinAmount, to, from) {
        if (this.props.wallet.estimateRateData !== '' && this.props.wallet.estimateRateData.data !== '' && this.props.wallet.estimateRateData !== undefined && coinAmount !== '' && this.props.wallet.estimateRateData.message !=="Incorrect pair") {
            const rate = this.props.wallet.estimateRateData.data.rate;
            const value = coinAmount * rate;
            this.setState({
                estimated_val:value
            })
        } else if (coinAmount === '') {
            Alert.alert('Please enter amount');
        } else if (this.props.wallet.estimateRateData.message === "Incorrect pair") {
            Alert.alert(this.props.wallet.estimateRateData.message);
            this.setState({
                estimated_val: ''
            })
        } else {
            Alert.alert('Please select currencies');
            this.setState({
                estimated_val: ''
            })
        }

    }
    convertUsercoin(coinAmount, to, from) {
        const convertObject = {
            amount: coinAmount,
            fromCoin: from,
            toCoin: to
        }
        const jwt = {
            jwtToken: 'JWT ' + this.props.user.loginData.user.jwtToken
        }

        FetchPost('user/shapeshift/exchange', convertObject, jwt.jwtToken)
            .then((resp) => {
                console.log(resp);
                this.props.convertUsercoin(resp)
                Alert.alert(this.props.wallet.userCoinConversionData.message)
                this.setState({
                    check_conversion:true
                })
            }).catch((err) => {
                Alert.alert('Something went wrong');
                console.log(err, 'error');
            })

    }
    sendUsercoin(coinAmount, sendAddress) {
        this.props.customLoaderAction('true');
        const sendUsercoinObject = {
            value: coinAmount,
            wallet_code: this.props.wallet.userCommonWalletDeatil.wallet_code,
            to: sendAddress
        }
        const jwt = {
            jwtToken: 'JWT ' + this.props.user.loginData.user.jwtToken
        }

        FetchPost('user/wallet/transaction', sendUsercoinObject, jwt.jwtToken)
            .then((resp) => {
                console.log(resp, 'check');
                this.props.createUserCoinTransaction(resp)
                Alert.alert(this.props.wallet.userTransactionDetails.message)
                this.props.customLoaderAction('');
            }).catch((err) => {
                Alert.alert('Something went wrong');
                console.log(err, 'error');
                this.props.customLoaderAction('');
            })

        // this.props.createUserCoinTransaction(sendUsercoinObject)
        // setTimeout(() => {
        //     Alert.alert(this.props.wallet.userTransactionDetails.message)
        //     this.props.customLoaderAction('');
        // }, 2000);
    }
    capture = (value) => {
        this.props.customFlag(value);
    }
    closeCryptoMenu = (value) => {
        this.setState({ leftModalOpen: value })
        this._leftDrawer.close()
    }

    deleteThem = () => {
        this.props.wallet.estimateRateData = {};
        this.setState({
            estimated_val: ""
        })
    }

    render() {

        const loadModal = this.state.modalReady === true
        const width = Dimensions.get('window').width
        const height = Dimensions.get('window').height
        const { userCommonWalletDeatil, userCoinWalletData, userBalance, loaderData, userWalletDetail, customFlag, estimateRateData, userCoinConversionData } = this.props.wallet;
        //console.log(this.props.wallet.userCoinWalletData);

        const drawerStylesLeft = {
            drawer: {
                backgroundColor: 'white',
                borderTopRightRadius: 30,
                borderBottomRightRadius: 30,
                maxHeight: height - 100,
                borderRightWidth: 1,
                borderBottomWidth: 1,
                borderTopWidth: 1,
                borderColor: 'lightgrey'
            }
        }
        const drawerStylesRight = {
            drawer: {
                backgroundColor: 'white',
                borderTopLeftRadius: 30,
                borderBottomLeftRadius: 30,
                maxHeight: height - 300,
                borderLeftWidth: 1,
                borderBottomWidth: 1,
                borderTopWidth: 1,
                borderColor: 'lightgrey'
            }
        }

        return (
            <React.Fragment>
                <StatusBar barStyle="light-content" />


                <Header headerText={userCommonWalletDeatil.wallet_name + ' Home'}
                    leftComponent={'menu'}
                    leftOnPress={this.openCryptoMenu}
                    rightComponent={'more-horizontal'}
                    rightOnPress={this.openAltMenu} />

                <Drawer content={<CryptoMenu closeModal={this.closeCryptoMenu} navigate={this.props.navigation} />} tapToClose={true} open={false}
                    type={'overlay'} styles={drawerStylesLeft} side={'left'}
                    openDrawerOffset={(viewport) => viewport.width - 150} ref={(ref) => this._leftDrawer = ref}>
                    <Drawer content={<AltMenu navigate={this.props.navigation} />} tapToClose={true} open={false}
                        type={'overlay'} styles={drawerStylesRight} side={'right'}
                        openDrawerOffset={(viewport) => viewport.width - 150}
                        ref={(ref) => this._rightDrawer = ref}>
                        <Tabs initialTab={4} maxLength={4}>
                            {/* Tab 1: Convert */}
                            <Buy title="Buy" navigate={this.props.navigation} />

                            {/* Tab 2: Convert */}
                            <Sell title="Sell" navigate={this.props.navigation} />

                            {/* Tab 3: Convert */}
                            <Convert title="Convert"
                                deleteThem={this.deleteThem}
                                capture={this.capture}
                                wallet_flag={customFlag}
                                wallet_code={userCommonWalletDeatil.wallet_code}
                                belowTilte={userCommonWalletDeatil.wallet_name}
                                customImage={userCommonWalletDeatil.wallet_png}
                                walletList={userWalletDetail}
                                estimateVAlues={estimateRateData}
                                convertUsercoin={(coinAmount, to, from) => this.convertUsercoin(coinAmount, to, from)}
                                estimateCoin={(to, from) => this.estimateCoin(to, from)}
                                custom_rate_calculate={(coinAmount) => this.custom_rate_calculate(coinAmount)}
                                estimatevalue={this.state.estimated_val}
                                check_convert_status={this.state.check_conversion}/>

                            {/* Tab 4: Send */}
                            <Send title="Send"
                                belowTilte={userCommonWalletDeatil.wallet_name}
                                sendUsercoin={(coinAmount, sendAddress) => this.sendUsercoin(coinAmount, sendAddress)}
                                UserCoinBalance={userBalance.confirm_balance}
                                qrcodeData={this.props.wallet.qrcodeData}
                                navigate={this.props.navigation}
                                customImage={userCommonWalletDeatil.wallet_png}
                                customLoader={loaderData} />

                            {/* Tab 5: Recieve */}

                            <Receive title="Receive"
                                belowTilte={userCommonWalletDeatil.wallet_name}
                                UserCoinBalance={userBalance.confirm_balance}
                                payload={userCoinWalletData.address} />
                        </Tabs>

                    </Drawer>
                </Drawer>
            </React.Fragment>
        )
    }
}