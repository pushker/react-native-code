// USER ACTIONS
export const ACTIVATE_ACCOUNT = 'ACTIVATE_ACCOUNT';
export const CHANGE_PASSWORD = 'CHANGE_PASSWORD';
export const COUNTRY_LIST = 'COPUNTRY_LIST';
export const CREATE_ACCOUNT = 'CREATE_ACCOUNT';
export const CURRENCY_LIST = 'CURRENCY_LIST';
export const FORGOT_PASSWORD = 'FORGOT_PASSWORD';
export const GET_USER_DETAIL = 'GET_USER_DETAIL';
export const LOGIN = 'LOGIN';
export const MERGE_ACCOUNT = 'MERGE_ACCOUNT';
export const RESEND_ACTIVATION_CODE = 'RESEND_ACTIVATION_CODE';
export const UPDATE_ACCOUNT = 'UPDATE_ACCOUNT';
export const UPDATE_PASSWORD = 'UPDATE_PASSWORD';
export const TRANSACTIONS_DETAIL = 'TRANSACTIONS_DETAIL';


// JAVVY TOKEN ACTIONS
export const CREATE_TOKEN_TRANSACTIONS = 'CREATE_TOKEN_TRANSACTIONS';
export const GET_TOKEN_BALANCE = 'GET_TOKEN_BALANCE';


// ETHEREUM ACTIONS
export const CREATE_ETHEREUM_TRANSACTION = 'CREATE_ETHEREUM_TRANSACTION';
export const ETHEREUM_BALANCE = 'ETHEREUM_BALANCE';
export const ETHEREUM_DETAIL ='ETHEREUM_DETAIL';
export const ETHEREUM_ESTIMATE_GAS = 'ETHEREUM_ESTIMATE_GAS';
export const ETHEREUM_IMPORT = 'ETHEREUM IMPORT';
export const ETHEREUM_KEY = 'ETHEREUM_KEY';
export const COMPARE_ETHEREUM = 'COMPARE_ETHEREUM';

// Bitcoin ACTIONS
export const CREATE_BITCOIN_TRANSACTION = 'CREATE_BITCOIN_TRANSACTION';
export const BITCOIN_DETAIL = 'BITCOIN_DETAIL';
export const BITCOIN_BALANCE = 'BITCOIN_BALANCE';
export const COMPARE_BITCOIN = 'COMPARE_BITCOIN';


// COIN CONVERSION ACTIONS
export const GET_COIN_LIST = 'GET_COIN_LIST';
export const GET_SHAPESHIFT_RATE = 'GET_SHAPESHIFT_RATE';
export const SHAPESHIFT_EXCHANGE = 'SHAPESHIFT_EXCHANGE';


// BITCOIN & ETHEREUM ACTIONS
export const CREATE_ADDRESS = 'CREATE_ADDRESS';
export const USER_WALLET_DETAIL = 'USER_WALLET_DETAIL';


// Action Errors
export const LOGIN_ERROR = 'LOGIN_ERROR';


//QR CODE
export const SEND_QRCODE = 'SEND_QRCODE';

// COINS
export const ALL_COINS = 'ALL_COINS';
export const FAV_COINS = 'FAV_COINS';

// SELFIE IMAGE UPLOAD
export const SELFIE_IMAGE = 'SELFIE_IMAGE';

//STATE LIST
export const STATE_LIST = 'STATE_LIST';

//KYC STORE ACTIONS
export const SAVE_COUNTRY_STATE = 'SAVE_COUNTRY_STATE';
export const SAVE_FRONT_ID = 'SAVE_FRONT_ID';
export const SAVE_BACK_ID = 'SAVE_BACK_ID';
export const SAVE_FULL_KYC_WITH_UTILITY = 'SAVE_FULL_KYC_WITH_UTILITY';
export const GET_KYC_DOCUMENTS = 'GET_KYC_DOCUMENTS';
export const CHECK_KYC_STATUS = 'CHECK_KYC_STATUS';

//USER WALLET COMMON
export const SAVE_USER_WALLET_CODE_NAME = 'SAVE_USER_WALLET_CODE_NAME';
export const USER_COIN_DETAIL = 'USER_COIN_DETAIL';
export const USER_WALLET_BALANCE = 'USER_WALLET_BALANCE';
export const CREATE_USERCOIN_TRANSACTION = 'CREATE_USERCOIN_TRANSACTION';
export const CUSTOM_LOADER = 'CUSTOM_LOADER';
export const CUSTOM_FLAG = 'CUSTOM_FLAG';
