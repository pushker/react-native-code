import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Dimensions,
    Alert,
    Image,
    TouchableOpacity,
    ActivityIndicator
} from 'react-native';

import { Header } from '../../common/Header'
import { RNCamera, FaceDetector } from 'react-native-camera';
import { token, baseUrlLive } from '../../../api';


export default class Onboarding extends Component {

    constructor(props) {
        super(props);
        this.state = {
            backgroundColor: '#2756A9',
            path: null,
            imagePath: null,
            isLoading: false,
        }
    }

    static navigationOptions = {
        header: null
    };

    componentDidMount() {

    }

    takePicture = async function () {
        if (this.camera) {
            this.setState({ isLoading: true });
            const options = { quality: 0.5, base64: true };
            const data = await this.camera.takePictureAsync(options);
            this.setState({
                path: data.base64,
                imagePath: data.uri,
                isLoading:false
            });
        }
    }

    uploadSelfie() {
        const Selfie = this.state.path;
        const user = {
            jwt: 'JWT ' + this.props.user.loginData.user.jwtToken,
            path: 'data:image/jpg;base64,' + Selfie
        }

        this.setState({ isLoading: true });
       // this.props.navigation.navigate('CountryKyc');
       fetch(`${baseUrlLive}user/kyc-sunflower?token=${token}`, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: user.jwt
            },
            body: JSON.stringify({
                'face_image': user.path
            })
        })
            .then(res => res.json())
            .then((data) => {
                if (data.verified_status === 1) {
                    this.props.uploadSelfieImage(data);
                    Alert.alert(data.message);
                    this.setState({ isLoading: false });
                    this.props.navigation.navigate('CountryKyc');
                } else {

                    Alert.alert(data.message);
                    const userData = {
                        verified_status: data.verified_status,
                        message:data.message
                    }
                    this.props.uploadSelfieImage(userData);
                    this.setState({
                        isLoading: false, path: null,
                        imagePath: null });
                }

            })
            .catch(err => {
                console.log(err)
            })

    }


    renderCamera() {
        return (
            <View>
                <View>
                    <RNCamera
                        ref={ref => {
                            this.camera = ref;
                        }}
                        style={styles.preview}
                        type={RNCamera.Constants.Type.front}
                        flashMode={RNCamera.Constants.FlashMode.on}
                        permissionDialogTitle={'Permission to use camera'}
                        permissionDialogMessage={'We need your permission to use your camera phone'}
                    >

                    </RNCamera>
                </View>
                <ScrollView>
                    <Text style={{ fontSize: 24, color: '#fff', paddingLeft: 10, paddingBottom: 8 }}>
                        Let's take a selfie
                        </Text>

                    <Text style={{ fontSize: 16, color: '#fff', paddingLeft: 20, paddingRight: 20 }}>
                        We’ve all taken at least one selfie in our lives,
                        so this should be pretty easy. We need to see your
                        full face within a well lit room. No hats, hoodies,
                        masks, etc. With a steady hand, hold the front facing
                        camera of your phone a few inches from your face, and
                        press the capture button. Say, “Cheese!”
                        </Text>
                    <View style={styles.button}>
                        <Text style={styles.textButton} onPress={this.takePicture.bind(this)}>
                            Capture
                        </Text>
                    </View>
                </ScrollView>
            </View>
        );
    }

    renderImage() {
        return (
            <View>
                <Image
                    source={{ uri: this.state.imagePath }}
                    style={styles.preview}
                />
                <Text style={{ fontSize: 24, color: '#fff', paddingLeft: 10, paddingBottom: 8 }}>
                    Let's Upload a selfie
                </Text>
                <View style={{ width: Dimensions.get('window').width, flexDirection: 'row', justifyContent: 'space-around' }}>
                    <View style={styles.buttonStyle}>
                        <Text style={styles.textButton} onPress={() => this.setState({ imagePath: null })}>
                            Cancel
                        </Text>
                    </View>
                    <View style={styles.buttonStyle}>
                        <Text style={styles.textButton} onPress={this.uploadSelfie.bind(this)}>
                            Upload
                        </Text>
                    </View>
                </View>
            </View>
        );
    }

    render() {
        const config = {
            velocityThreshold: 0.3,
            directionalOffsetThreshold: 80
        };
        console.log(this.props);
        const { navigate } = this.props.navigation;
        const width = Dimensions.get('window').width;
        const height = Dimensions.get('window').height;
        return (
            <React.Fragment>
                <Header headerText={'Javvy Verification - KYC'} />
                    <View style={{
                        flex: 1,
                        backgroundColor: this.state.backgroundColor
                    }}>
                    <View style={styles.container}>
                        {this.state.isLoading === true ?
                            <View style={{ position: 'absolute', right: 0, left: 0, top: 0, bottom: 0, backgroundColor: '#fff', opacity: .8, height: height, zIndex: 999, justifyContent: 'center', alignItems: 'center' }}><ActivityIndicator size="large" color="#0F44A0" /></View> : null}
                        {this.state.imagePath ? this.renderImage() : this.renderCamera()}
                        </View>
                    </View>

            </React.Fragment>
        )
    }
}



const styles = StyleSheet.create({
    // Slide styles
    slide: {
        flex: 1,   // Take up all screen
        backgroundColor: '#2756A9'
    },
    // Header styles
    header: {
        color: '#FFFFFF',
        fontFamily: 'Avenir',
        fontSize: 30,
        fontWeight: 'bold',
        marginVertical: 15,
    },
    // Text below header
    text: {
        color: '#FFFFFF',
        fontFamily: 'Avenir',
        fontSize: 18,
        marginHorizontal: 40,
        textAlign: 'center',
    },
    textButton: {
        color: '#2756A9',
        fontWeight: '600',
        fontSize: 18
    },
    preview: {
        height: Dimensions.get('window').height / 2,
        justifyContent: 'center',
        alignItems: 'center'
    },
    capture: {
        backgroundColor: 'white',
        borderRadius: 5,
        padding: 10,
        paddingHorizontal: 20,
        margin: 10,
        position: 'absolute',
        top: Dimensions.get('window').width / 1.84,
        alignSelf: 'flex-start',
    },
    buttonStyle: {
        backgroundColor: 'white',
        borderRadius: 5,
        padding: 9,
        paddingHorizontal: 20,
        marginTop: 50,
        alignItems: 'flex-start',
        alignSelf: 'flex-start',
    },
    button: {
        backgroundColor: 'white',
        borderRadius: 5,
        padding: 8,
        paddingHorizontal: 20,
        marginTop:15,
        marginLeft: 260,
        alignItems: 'flex-start',
        alignSelf: 'flex-start',
    }
});