import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    TouchableOpacity,
    Dimensions
} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';



export default class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }


    }


    render() {
        const {name} = this.props;
        const bitcoinBalance = this.props.bitcoinBalance;
        const ethereumBalance = this.props.ethereumBalance;
        const bitcoinRate = this.props.bitcoinRate;
        const ethereumRate = this.props.ethereumRate;
        return (
            <View style={styles.hmContainer}>
                <View style={styles.hmHeader}>
                    <Text style={{fontSize: 28, color: '#0F44A0', fontWeight: '300'}}>
                        Welcome,
                    </Text>
                    <Text style={{fontSize: 28, color: '#0F44A0', fontWeight: '300'}}>
                        {name}
                    </Text>
                </View>

                <View style={{margin: 15}}>
                    <Text style={{fontSize: 18, textAlign: 'center'}}>Current Wallet Balance</Text>
                    <View style={{ width: Dimensions.get('window').width, flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center'}}>
                        <View>
                            <View style={{marginTop: 10, paddingBottom: 5}}><Text style={{fontSize: 14.25, opacity: .8}}>Bitcoin</Text></View>
                            <View><Text style={{fontSize: 16.25, color: '#0F44A0'}}>{bitcoinBalance}</Text></View>
                        </View>

                        <View>
                            <View style={{marginTop: 10, paddingBottom: 5}}><Text style={{fontSize: 14.25, opacity: .8}}>Ethereum</Text></View>
                            <View><Text style={{fontSize: 16.25, color: '#0F44A0'}}>{ethereumBalance}</Text></View>
                        </View>
                    </View>
                </View>

                <View style={{margin: 15}}>
                    <Text style={{fontSize: 18, textAlign: 'center'}}>Coin Conversion Rate</Text>
                    <View style={{ width: Dimensions.get('window').width, flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center'}}>
                        <View>
                            <View style={{marginTop: 10, paddingBottom: 5}}><Text style={{fontSize: 14.25, opacity: .8}}>Bitcoin</Text></View>
                            <View><Text style={{fontSize: 16.25, paddingBottom: 5}}><Icon name="dollar" size={16} ></Icon> {bitcoinRate.USD}</Text></View>
                            <View><Text style={{fontSize: 16.25}}><Icon name="euro" size={16} ></Icon> {bitcoinRate.EUR}</Text></View>
                        </View>

                        <View>
                            <View style={{marginTop: 10, paddingBottom: 5}}><Text style={{fontSize: 14.25, opacity: .8}}>Ethereum</Text></View>
                            <View><Text style={{fontSize: 16.25, paddingBottom: 5}}><Icon name="dollar" size={16} ></Icon> {ethereumRate.USD}</Text></View>
                            <View><Text style={{fontSize: 16.25}}><Icon name="euro" size={16} ></Icon> {ethereumRate.EUR}</Text></View>
                        </View>
                    </View>
                </View>

            </View>
        )
    }
}



const styles = StyleSheet.create({
    hmContainer: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 35
    },
    hmHeader: {
        justifyContent: 'center',
        alignItems: 'center',
    }
})