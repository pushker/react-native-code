import {connect} from 'react-redux';
import Coins from '../components/Screens/Coins';

import {getUserDetail} from '../actions/userAction';
import {getCoins, getFavCoins, postFavCoins} from "../actions/walletAction";

const mapStateToProps = (state) => {
    return {
        user: state.user,
        wallet: state.wallet
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        getCoinsDetail: (data) => {
            dispatch(getCoins(data))
        },
        getFavCoinsDetail: (data) => {
            dispatch(getFavCoins(data))
        },
        postFavCoins: (data) => {
            dispatch(postFavCoins(data))
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Coins);