import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    KeyboardAvoidingView,
    Animated,
    Image,
    Keyboard,
    TextInput,
    TouchableOpacity,
    Dimensions,
    StatusBar,
    Alert,
    ActivityIndicator,
} from 'react-native';

import Icon from 'react-native-vector-icons/Feather';

const IMAGE_HEIGHT = 141.39;
const IMAGE_WIDTH = 104.88;

export default class ResetPass extends Component {
    constructor(props) {
        super(props);
        this.state = {
            codeSent: false,
            email: '',
            password: '',
            code: '',
            isLoading: false
        }
        this.handleSendCode = this.handleSendCode.bind(this)
        this.validateEmail = this.validateEmail.bind(this)
        this.handleReset = this.handleReset.bind(this)
        this.validateUpdate = this.validateUpdate.bind(this)

        this.imageHeight = new Animated.Value(IMAGE_HEIGHT);
        this.imageWidth = new Animated.Value(IMAGE_WIDTH);
    }
    componentWillMount() {
        if (Platform.OS === 'ios') {
            this.keyboardWillShowSub = Keyboard.addListener('keyboardWillShow', this.keyboardWillShow);
            this.keyboardWillHideSub = Keyboard.addListener('keyboardWillHide', this.keyboardWillHide);
        } else
            this.keyboardWillShowSub = Keyboard.addListener('keyboardDidShow', this.keyboardWillShow);
        this.keyboardWillHideSub = Keyboard.addListener('keyboardDidHide', this.keyboardWillHide);


    }

    componentWillUnmount() {
        this.keyboardWillShowSub.remove();
        this.keyboardWillHideSub.remove();
    }

    keyboardWillShow = (event) => {
        Animated.timing(this.imageHeight, {
            duration: event.duration,
            toValue: (0),
        }).start();

        Animated.timing(this.imageWidth, {
            duration: event.duration,
            toValue: (0),
        }).start();
    };

    keyboardWillHide = (event) => {
        Animated.timing(this.imageHeight, {
            // duration: event.duration,
            toValue: IMAGE_HEIGHT,
        }).start();

        Animated.timing(this.imageWidth, {
            // duration: event.duration,
            toValue: IMAGE_WIDTH,
        }).start();
    };



    static navigationOptions = {
        header: null,
        gesturesEnabled: false
    }


    changeView() {
        this.setState({ codeSent: true })
    }


    validateEmail() {
        const clearActivityIndicator = () => this.setState({ isLoading: false })
        const sent = 'Password reset code send to your email .'
        const errMail = "Required fields: email";
        if (this.props.user.resetPassCode.message === sent) {
            this.setState({ email: '' })
            this.changeView()
            Alert.alert(``,
                this.props.user.resetPassCode.message,
                [
                    { text: 'OK', onPress: () => console.log('OK Pressed') }
                ])
            clearActivityIndicator()
        } else if (this.props.user.resetPassCode.message == errMail) {
            Alert.alert(``,
                'Please enter your email.',
                [
                    { text: 'OK' }
                ])
            clearActivityIndicator()
        }
        else {
            Alert.alert(``,
                this.props.user.resetPassCode.message,
                [
                    { text: 'OK' }
                ])
            clearActivityIndicator()
        }

    }

    validateUpdate() {
        const clearActivityIndicator = () => this.setState({ isLoading: false })
        const sucess = 'Password Updated Successfully'
        const errFields = "Required  Fields: email, password, code";
        console.log(this.props.user.updatePassData);
        if (this.props.user.updatePassData.message === sucess) {
            Alert.alert(``,
                this.props.user.updatePassData.message,
                [
                    { text: 'OK' }
            ])
            this.props.navigation.navigate('Login')
            clearActivityIndicator()
        } else if (this.props.user.updatePassData.message === errFields) {
            Alert.alert(``,
                'Please check your input fields.',
                [
                    { text: 'OK' }
                ])
            clearActivityIndicator();
        }
        else {
            Alert.alert(``,
                this.props.user.updatePassData.message,
                [
                    { text: 'OK' }
                ])
            clearActivityIndicator()
        }

    }


    handleSendCode() {
        var emailRegex = new RegExp("^[a-z0-9_.-]+\.?[a-z0-9_-]+@[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?[.][a-z]{2,61}(?:\.[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?)*$");
        const userEmail = { email: this.state.email }
        if (userEmail.email === '' || userEmail.email === undefined) {
            const err = 'Please enter your email.';
            Alert.alert(``,
                err,
                [
                    { text: 'OK', onPress: () => console.log('OK Pressed') }
                ])
        } else {
            if (emailRegex.test(userEmail.email) === false) {
                Alert.alert(``,
                    `Please enter valid email address.`,
                    [
                        { text: 'OK' }
                    ])
            } else {
                this.setState({ isLoading: true })
                this.props.forgotPassword(userEmail)
                setTimeout(() => {
                    this.validateEmail()
                }, 2000)
            }
        }

    }


    handleReset() {
        const updatePasswordObject = {
            email: this.state.email,
            password: this.state.password,
            code: this.state.code
        }
        console.log(updatePasswordObject);
        if (this.state.email !== '' && this.state.password !== '' && this.state.code !== '') {
            this.setState({ isLoading: true })
            this.props.updatePassword(updatePasswordObject)
            setTimeout(() => {
                this.validateUpdate()
            }, 2000)
        } else {
            Alert.alert(``,
                `Please check your input fields.`,
                [
                    { text: 'OK' }
                ])
        }


    }

    updateEmailChange = (e) => {
        this.setState({ email: e })
    }

    focusPassword = () => {
        var emailRegex = new RegExp("^[a-z0-9_.-]+\.?[a-z0-9_-]+@[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?[.][a-z]{2,61}(?:\.[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?)*$");
        if (emailRegex.test(this.state.email) === false) {
            Alert.alert(``,
                `Please enter valid email address.`,
                [
                    { text: 'OK' }
                ])
        } else {
            this.PassInput.focus();
        }

    }
    updatePassword = (event) => {
        this.setState({ password: event })
    }
    focusCode = () => {
        var PasswordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])\S{8,}/;
        if (PasswordRegex.test(this.state.password) === false) {
            console.log(PasswordRegex.test(this.state.password));
            Alert.alert(``,
                `Password Should mininum 8 characters long and 1 uppercase, 1 special character , 1 integer and characters must`,
                [
                    { text: 'OK' }
                ])
        } else {
            this.codeInput.focus();
        }

    }




    render() {
        const { navigate } = this.props.navigation;
        const width = Dimensions.get('window').width;
        const height = Dimensions.get('window').height;
        console.log(width);
        return (
            <React.Fragment>


                <StatusBar barStyle="light-content" />
                <KeyboardAvoidingView style={styles.container} behavior="padding" enabled={true}>

                    {this.state.isLoading === true ?
                        <View style={{ position: 'absolute', right: 0, left: 0, top: 0, bottom: 0, backgroundColor: '#fff', opacity: .8, height: height, zIndex: 999, justifyContent: 'center', alignItems: 'center' }}><ActivityIndicator size="large" color="#0F44A0" /></View> : null}
                    <View style={styles.lgn_container}>

                        <View style={this.state.codeSent === true ? styles.customUpperImageStyle1:styles.customUpperImageStyle2}>




                            <TouchableOpacity style={{ position: 'absolute', top: 20, left: 0 }} onPress={() => this.props.navigation.navigate('Login')}>
                                <Icon name='chevron-left' size={32} color="#fff"></Icon>
                                </TouchableOpacity>
                            {this.state.codeSent === true ? <Animated.Image style={[width === 320 ? styles320.lgn_icon : styles.lgn_icon, { height: this.imageHeight, width: this.imageWidth }]} source={require('../../../img/lg_icon.png')} /> : <Image style={width === 320 ? styles320.lgn_icon : styles.lgn_icon} source={require('../../../img/lg_icon.png')}></Image>}

                                <Text style={{ textAlign: 'center', paddingTop: 5, fontSize: 10, color: 'white' }}>v1.0.0</Text>

                        </View>
                        {this.state.codeSent === false ?
                            <View>

                                <View style={styles.lgn_txtContainer}>
                                    <TextInput onChangeText={(email) => this.setState({ email: email })}
                                        keyboardType={'email-address'}
                                        autoCapitalize={'none'}
                                        style={width === 320 ? styles320.lgn_txtInput : styles.lgn_txtInput}
                                        autoFocus
                                        placeholder={'Email Address'}
                                        underlineColorAndroid={'rgba(0,0,0,0)'}
                                        onSubmitEditing={this.handleSendCode}>
                                    </TextInput>

                                </View>



                                <View style={width === 320 ? styles320.btnSignInContainer : styles.btnSignInContainer}>
                                    <TouchableOpacity style={width === 320 ? styles320.btnSignIn : styles.btnSignIn} onPress={this.handleSendCode} >
                                        <Text style={styles.btnSignInText}>Send</Text>
                                    </TouchableOpacity>
                                </View>



                            </View>
                            :
                            <View>
                                <View style={{ width: '100%', alignItems: 'center', paddingBottom: 10 }}>

                                </View>
                                <View>
                                    <TextInput onChangeText={this.updateEmailChange}
                                        keyboardType={'default'}
                                        autoCapitalize={'none'}
                                        style={width === 320 ? styles320.lgn_txtInput : styles.lgn_txtInput}
                                        autoFocus
                                        placeholder={'Email Address'}
                                        underlineColorAndroid={'rgba(0,0,0,0)'}
                                        onSubmitEditing={
                                            this.focusPassword
                                        }>
                                    </TextInput>
                                    <TextInput onChangeText={this.updatePassword}
                                        secureTextEntry={true}
                                        style={[width === 320 ? styles320.lgn_txtInput : styles.lgn_txtInput, styles.fixup]}
                                        placeholder={'New Password'}
                                        underlineColorAndroid={'rgba(0,0,0,0)'}
                                        ref={(ref) => { this.PassInput = ref; }}
                                        onSubmitEditing={
                                            this.focusCode
                                        }>
                                    </TextInput>
                                    <TextInput onChangeText={(code) => this.setState({ code: code })}
                                        keyboardType={'email-address'}
                                        autoCapitalize={'none'}
                                        style={width === 320 ? styles320.lgn_txtInput : styles.lgn_txtInput}
                                        placeholder={'Code'}
                                        underlineColorAndroid={'rgba(0,0,0,0)'}
                                        ref={(ref) => { this.codeInput = ref; }}
                                        onSubmitEditing={
                                            this.handleReset
                                        }>
                                    </TextInput>
                                </View>



                                <View style={width === 320 ? styles320.btnSignInContainer : styles.btnSignInContainer}>
                                    <TouchableOpacity style={width === 320 ? styles320.btnSignIn : styles.btnSignIn} onPress={this.handleReset} >
                                        <Text style={styles.btnSignInText}>Update</Text>
                                    </TouchableOpacity>
                                </View>

                                <View style={styles.signUpContainer}>
                                    <Text style={styles.signUpText}>Didn't receive a code?</Text>
                                    <TouchableOpacity onPress={() => this.setState({ codeSent: false })}>
                                        <Text style={styles.signUpBtn}>Resend</Text>
                                    </TouchableOpacity>
                                </View>



                            </View>}

                    </View>

                </KeyboardAvoidingView>
            </React.Fragment>

        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
    lgn_icon: {
        height: 141.39,
        width: 104.88,
    },
    lgn_container: {
        width: '100%',
        height: 450,
        alignItems: 'center',
    },
    lgn_txtInput: {
        width: 325.3,
        height: 45.59,
        borderRadius: 50,
        backgroundColor: '#EDEDED',
        opacity: 0.8,
        padding: 10,
        fontSize: 16
    },
    lgn_txtContainer: {
        width: 325.3,
        height: 72.3,
        marginTop: 27.1,
        justifyContent: 'space-between'
    },
    fixup: {
        marginTop: 13.1,
        marginBottom: 13.1
    },
    rstPassContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 48.1
    },
    rstPassText: {
        opacity: 0.8,
        padding: 5,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center'
    },
    rstPassBtn: {
        color: '#0F44A0',
        fontWeight: '600'
    },
    btnSignInContainer: {
        alignItems: 'center',
        marginTop: 0
    },
    btnSignIn: {
        width: 230,
        marginBottom: 10.1,
        shadowColor: '#0F44A0',
        shadowOffset: { width: 2, height: 2 },
        shadowOpacity: .8,
        backgroundColor: '#0F44A0',
        height: 52.94,
        marginTop: 20,
        borderRadius: 50,
        justifyContent: 'center',
        alignItems: 'center'
    },
    btnSignInText: {
        color: 'white',
        fontSize: 18
    },
    signUpContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    signUpText: {
        opacity: 0.8,
        padding: 10
    },
    signUpBtn: {
        color: '#0F44A0',
        fontWeight: '600',
        alignSelf: 'center',
    },
    customUpperImageStyle1: {
        width: '100%', justifyContent: 'center', alignItems: 'center', paddingTop: 40, paddingBottom: 10, backgroundColor: '#0F44A0'
    },
    customUpperImageStyle2: {
        width: '100%', justifyContent: 'center', alignItems: 'center', paddingTop: 60, paddingBottom: 39.6, backgroundColor: '#0F44A0'
    },
});


const styles320 = {
    lgn_icon: {
        height: 106.62,
        width: 97.62,
    },
    lgn_txtInput: {
        width: 305.3,
        height: 45.59,
        borderRadius: 50,
        backgroundColor: '#EDEDED',
        opacity: 0.8,
        padding: 10,
        fontSize: 16,
    },
    lgn_txtContainer: {
        width: 305.3,
        height: 102.3,
        marginTop: 47.1,
        justifyContent: 'space-between',
    },
    rstPassContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 27
    },
    btnSignInContainer: {
        alignItems: 'center'
    },
    btnSignIn: {
        width: 230,
        marginBottom: 10.1,
        shadowColor: '#0F44A0',
        shadowOffset: { width: 2, height: 2 },
        shadowOpacity: .8,
        backgroundColor: '#0F44A0',
        height: 52.94,
        borderRadius: 50,
        justifyContent: 'center',
        alignItems: 'center'
    },
}

