import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    ScrollView,
    TouchableOpacity,
    Image,
    Alert,
    Dimensions
} from 'react-native';

import { connect } from 'react-redux';
import { DrawerActions } from 'react-navigation';


import { getFavCoins, saveUserWalletCodeName, usercoinDetail, userWalletBalance, customFlag } from '../../actions/walletAction'


class CryptoMenu extends Component {

    constructor(props) {
        super(props)

        this.state = {
            favcoins: []
        }
        this.route_dashboard = this.route_dashboard.bind(this);

    }

    componentDidMount() {
        setInterval(() => {
            this.setState({ favcoins: this.props.wallet.favCoinsData.coins })
        }, 2000)

    }

    navigateCoin(name, code, image) {
        this.props.closeModal(false);
        const Wallet = {
            wallet_name: name,
            wallet_code: code,
            wallet_png: image
        }
        const getBitcoinObject = {
            jwt: 'JWT ' + this.props.user.loginData.user.jwtToken,
            wallet_code: code
        }

        this.props.saveUserWalletCodeName(Wallet);
        this.props.usercoinDetail(getBitcoinObject)
        this.props.userWalletBalance(getBitcoinObject);
        this.props.customFlag(true);
        this.props.navigate.navigate('UserWallet');

    }

    route_dashboard() {
        this.props.navigate.navigate('Dashboard');
        this.props.closeModal(false);
    }



    render() {
        const { navigate } = this.props.navigate
        return (
            <ScrollView>
                <View style={{ minWidth: '100%', height: 570, justifyContent: 'space-around', alignItems: 'center' }}>
                    <View style={styles.tabContainer}>
                        <TouchableOpacity style={{ width: 60, alignItems: 'center' }}
                            onPress={this.route_dashboard}>
                            <Image resizeMode={'contain'} source={require('../../img/Javvy_Main.png')}
                                style={{ height: 66.62, width: 60.62, padding: 0 }} />
                        </TouchableOpacity>
                    </View>


                    {this.state.favcoins.map((coin, index) => {
                        return (
                            <View style={styles.tabContainer} key={index}>
                                <TouchableOpacity style={{ width: 60, alignItems: 'center' }}
                                    onPress={() => this.navigateCoin(coin.name, coin.code, coin.mob_image)}>
                                    <Image resizeMode={'contain'} source={{ uri: coin.mob_image }}
                                        style={{ minWidth: 60, minHeight: 60, maxHeight: 60, maxWidth: 60, padding: 0 }} />
                                </TouchableOpacity>
                            </View>
                        )
                    })}

                    <View style={styles.bottomTabContainer}>
                        <TouchableOpacity style={{ width: 60, alignItems: 'center' }}
                            onPress={() => navigate('Coin')}>
                            <Image resizeMode={'contain'} source={require('../../img/add.png')}
                                style={{ minWidth: 60, minHeight: 60, maxHeight: 60, maxWidth: 60, padding: 0 }} />
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>

        )
    }
}

const styles = StyleSheet.create({
    tabContainer: {
        // justifyContent: 'center',
        // alignItems: 'center',
        // width: '100%',
        minHeight: 60,
        maxHeight: 60
        // borderBottomWidth: 1,
        // borderBottomColor: '#2756A9'
    },
    bottomTabContainer: {
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        // width: '100%',
        minHeight: 60,
        maxHeight: 60
        // borderBottomColor: '#2756A9'
    },
    tabText: {
        fontSize: 18,
        color: '#2756A9',
        fontWeight: '300',
        textAlign: 'center'
    }
})



const mapStateToProps = (state) => {
    return {
        wallet: state.wallet,
        user: state.user
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        getFavCoinsDetail: (data) => {
            dispatch(getFavCoins(data))
        },
        saveUserWalletCodeName: (data) => {
            dispatch(saveUserWalletCodeName(data))
        },
        usercoinDetail: (data) => {
            dispatch(usercoinDetail(data))
        },
        userWalletBalance: (data) => {
            dispatch(userWalletBalance(data))
        },
        customFlag: (data) => {
            dispatch(customFlag(data))
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(CryptoMenu);


