import { connect } from 'react-redux';
import ActivateAccount from '../components/Screens/Register/Views/ActivateAccount.js';

import { activateAccount } from '../actions/userAction';

const mapStateToProps = ( state ) => {
    return {
        user: state.user
    }
}

const mapDispatchToProps = ( dispatch ) => {
    return {
        activateAccount: (data) => {
            dispatch(activateAccount(data))
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ActivateAccount);