import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    ScrollView,
    KeyboardAvoidingView,
    Image,
    TextInput,
    StatusBar,
    TouchableOpacity,
    Dimensions,
    Alert
} from 'react-native';

import  { SearchBar }  from 'react-native-elements';


export default class CreateAddress extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: '',
            showETH: true,
            showBTC: true
        }
        this.generateBTC = this.generateBTC.bind(this);
        this.generateETH = this.generateETH.bind(this);
        this.validateWallets = this.validateWallets.bind(this);
    }


    static navigationOptions = {
        gesturesEnabled: false,
        header: null
    }

    componentDidMount(){
        const user = {
            jwt: 'JWT ' + this.props.user.activateAccountData.jwtToken
        }
        this.props.userWalletDetail(user)
    }


    validateWallets(){
        const success = () => Alert.alert(
            'Congratulations!', 
            'Your new Javvy Account has succesfully been created. Please log in to continue.')

        const generateETCObject = {
            coin_type: 'ETH',
            jwt: 'JWT ' + this.props.user.activateAccountData.jwtToken
        }

        const generateBTCObject = {
            coin_type: 'BTC',
            jwt: 'JWT ' + this.props.user.activateAccountData.jwtToken
        }

        if (this.props.wallet.userWalletDetail.wallets.length === 0) {
            this.props.createAddress(generateETCObject)
            this.props.createAddress(generateBTCObject)
            success()
            this.props.navigation.navigate('Login')
            
        } else if (this.props.wallet.userWalletDetail.wallets.length === 1) {
            this.props.createAddress(generateETCObject)
            success()
            this.props.navigation.navigate('Login')

        } else {
            success()
            this.props.navigation.navigate('Login')
        }

    }


    generateETH(event){
        event.preventDefault()

        const generateETCObject = {
            coin_type: 'ETH',
            jwt: 'JWT ' + this.props.user.activateAccountData.jwtToken
        }

        this.props.createAddress(generateETCObject)

        setTimeout(() => {
            Alert.alert(this.props.wallet.createAddressData.message)
        }, 1000)

        setTimeout(() => {
            this.setState({showETH: false})
        }, 2000)
    }
    

    generateBTC(event){
        event.preventDefault()


        const generateBTCObject = {
            coin_type: 'BTC',
            jwt: 'JWT ' + this.props.user.activateAccountData.jwtToken
        }
        this.props.createAddress(generateBTCObject)

        setTimeout(() => {
            Alert.alert(this.props.wallet.createAddressData.message)
        }, 1000)

        setTimeout(() => {
            this.setState({showBTC: false})
        }, 2000)
        
    }



    render() {
        const { navigate } = this.props.navigation;

        const width = Dimensions.get('window').width
        const height = Dimensions.get('window').height

        return (
                <React.Fragment>
                <StatusBar barStyle={'light-content'} />
                <View style={{backgroundColor: '#0F44A0', justifyContent: 'center', alignItems: 'flex-start', width: '100%', paddingTop: 10, backgroundColor: '#2756A9', borderBottomEndRadius: 33, borderBottomStartRadius: 33,}}>
                    <View><Text style={{fontSize: 18, fontWeight: '300', color: '#fff', paddingLeft: 30, marginTop: 35}}>Add/Import Your Wallet</Text></View>
                    <View style={{backgroundColor: '#0F44A0', flexDirection: 'row', justifyContent: 'space-around', alignItems: 'flex-start', width: '100%', paddingTop: 10, backgroundColor: '#2756A9', borderBottomEndRadius: 33, borderBottomStartRadius: 33,}}>
                    <SearchBar round={true} containerStyle={{width: Dimensions.get('window').width / 1.5, backgroundColor: 'transparent', borderBottomColor: 'transparent', borderTopColor: 'transparent', justifyContent: 'flex-start', alignItems: 'flex-start' }} inputStyle={{borderWidth: 1, width: 250, borderColor: '#fff', backgroundColor: 'transparent', color:"white", textDecorationLine: 'none'}} underlineColorAndroid={'transparent'} />
                    <View style={{justifyContent: 'flex-start', alignItems: 'center'}}><TouchableOpacity onPress={() => this.validateWallets()}><Text style={{fontSize: 16, color:'#fff', fontWeight: '300', paddingTop: 10}}>Next</Text></TouchableOpacity></View>
                    </View>
                </View>
                    {/* <Image style={width === 320 ? styles320.lgn_icon : styles.lgn_icon} source={require('../../img/lg_icon.png')}></Image> */}
                <ScrollView>
                    {this.state.showBTC ?
                    <View style={[styles.cardContainer, {backgroundColor: '#ffa53c'}]}>
                        <View style={styles.cardTopHalf}>
                            <Text style={{fontSize: 24, color: '#fff'}}>BITCOIN</Text>
                            <TouchableOpacity style={styles.cardTopHalfRight} onPress={this.generateBTC}>
                                <Text style={{color: '#fff'}}>Create a New Wallet</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.cardMiddle}>
                            <Text style={{fontSize: 18, color: "#fff"}}>or</Text>
                        </View>
                        <View style={styles.cardBottomHalf}>
                        <KeyboardAvoidingView>
                            <TextInput onChangeText={(email) => this.setState({email: email})} 
                                        keyboardType={'default'} 
                                        autoCapitalize={'none'} 
                                        style={width === 320 ? styles320.lgn_txtInput : styles.lgn_txtInput}  
                                        underlineColorAndroid={'rgba(0,0,0,0)'}>
                            </TextInput>
                        </KeyboardAvoidingView>
                        <TouchableOpacity style={styles.btnImport}>
                            <Text style={{opacity: 0.8, fontSize: 16}}>Import</Text>
                        </TouchableOpacity>
                        </View>
                    </View>
                    : null }
                    
                    {this.state.showETH  ? 
                    <View style={[styles.cardContainer, {backgroundColor: '#3e3b91'}]}>
                        <View style={styles.cardTopHalf}>
                            <Text style={{fontSize: 24, color: '#fff'}}>ETHEREUM</Text>
                            <TouchableOpacity style={styles.cardTopHalfRight} onPress={this.generateETH}>
                                <Text style={{color: '#fff'}}>Create a New Wallet</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.cardMiddle}>
                            <Text style={{fontSize: 18, color: "#fff"}}>or</Text>
                        </View>
                        <View style={styles.cardBottomHalf}>
                        <KeyboardAvoidingView>
                            <TextInput onChangeText={(email) => this.setState({email: email})} 
                                        keyboardType={'default'} 
                                        autoCapitalize={'none'} 
                                        style={width === 320 ? styles320.lgn_txtInput : styles.lgn_txtInput}  
                                        underlineColorAndroid={'rgba(0,0,0,0)'}>
                            </TextInput>
                        </KeyboardAvoidingView>
                        <TouchableOpacity style={styles.btnImport}>
                            <Text style={{opacity: 0.8, fontSize: 16}}>Import</Text>
                        </TouchableOpacity>
                        </View>
                    </View>
                    : null }

                    <View style={[styles.cardContainer, {backgroundColor: '#0F44A0'}]}>
                        <View style={styles.cardTopHalf}>
                            <Text style={{fontSize: 24, color: '#fff'}}>JAVVY</Text>
                            <TouchableOpacity style={styles.cardTopHalfRight}>
                                <Text style={{color: '#fff'}}>Create a New Wallet</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.cardMiddle}>
                            <Text style={{fontSize: 18, color: "#fff"}}>or</Text>
                        </View>
                        <View style={styles.cardBottomHalf}>
                        <KeyboardAvoidingView>
                            <TextInput onChangeText={(email) => this.setState({email: email})} 
                                        keyboardType={'default'} 
                                        autoCapitalize={'none'} 
                                        style={width === 320 ? styles320.lgn_txtInput : styles.lgn_txtInput}  
                                        underlineColorAndroid={'rgba(0,0,0,0)'}>
                            </TextInput>
                        </KeyboardAvoidingView>
                        <TouchableOpacity style={styles.btnImport}>
                            <Text style={{opacity: 0.8, fontSize: 16}}>Import</Text>
                        </TouchableOpacity>
                        </View>
                    </View>

                </ScrollView>

                    
            
                </React.Fragment>  
        )
    }

}



const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: '#fff',
        paddingTop: 60
    },
    cardContainer: {
        height: 180,
        width: Dimensions.get('window').width / 1.05,
        alignSelf: 'center',
        margin: 20,
        borderRadius: 20,
    },
    cardTopHalf: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-around',
        paddingTop: 20,
        borderBottomWidth: 1,
        paddingBottom: 20,
        borderBottomColor: '#fff',
    },
    cardMiddle: {
        alignItems: 'center',
        paddingTop: 10
    },
    cardBottomHalf: {
        paddingTop: 10,
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    btnImport: {
        borderTopRightRadius: 50,
        borderBottomRightRadius: 50,
        backgroundColor: 'lightgrey',
        padding: 10,
        width: 80,
        height: 45.59,
        justifyContent: 'center',
        alignItems: 'center',
    },
    cardTopHalfRight: {
        borderWidth: 1,
        width: '50%',
        height: 30,
        borderRadius: 50,
        borderColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center'
    },
    lgn_icon: {
        height: 136.62,
        width: 140.62,
    },
    lgn_container: {
        width: 296,
        height: 471,
        alignItems: 'center',
    },
    lgn_txtInput: {
        width: 240,
        height: 45.59,
        borderTopLeftRadius: 50,
        borderBottomLeftRadius: 50,
        backgroundColor: '#fff',
        paddingLeft: 10,
        fontSize: 16,
    },
    lgn_txtContainer:{
        width: 325.3,
        height: 102.3,
        marginTop: 47.1,
        justifyContent: 'space-between',
        paddingLeft: 10,
        paddingRight: 10
    },
    fixup:{
        marginTop: 13.1,
        marginBottom: 13.1
    },
    btnSignInContainer: {
        alignItems: 'center', 
        marginTop: 20
    },
    btnSignIn: {
        width: 270, 
        marginBottom: 10.1, 
        shadowColor: '#0F44A0', 
        shadowOffset:{width: 2, height: 2}, 
        shadowOpacity: .8, 
        backgroundColor: '#0F44A0', 
        height:52.94, 
        marginTop: 3, 
        borderRadius: 50, 
        justifyContent: 'center', 
        alignItems:'center',
        flexDirection: 'row'
    },
    btnSignInText: {
        color: 'white', 
        fontSize: 18,
        paddingRight: 5
    },
    signUpContainer: {
        flexDirection: 'row', 
        justifyContent: 'center', 
        alignItems: 'center'
    },
    signUpText: {
        opacity: 0.8, 
        padding: 10
    },
    signUpBtn: {
        color: '#0F44A0', 
        fontWeight: '600'
    }
})